\documentclass[epsf,psfig,fancyheadings,12pt]{article}
%\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
%\usepackage[francais]{babel}
\usepackage[utf8]{inputenc}
\usepackage{fancyhdr}
\usepackage{amsmath}
\usepackage{graphics}
\usepackage{subfigure}
\usepackage[final]{epsfig}
\usepackage{mathrsfs}
\usepackage{auto-pst-pdf}
\usepackage{pstricks,pst-node,pst-text,pst-3d}
\usepackage{amssymb}
\usepackage{multirow}
\usepackage{rotating}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{nicefrac}
\usepackage{xfrac}
\usepackage{animate}
\usepackage[toc,page]{appendix}
\makeatletter
% double spacing lines 
\renewcommand{\baselinestretch}{1}
\newcommand{\LyX}{L\kern-.1667em\lower.25em\hbox{Y}\kern-.125emX\spacefactor1000}
%pour faire reference toujours avec la meme norme
%lyx !
\newcommand{\eq}[1]{Eq.~\ref{eq:#1}}
\newcommand{\fig}[1]{Fig.~\ref{fig:#1}}
\newcommand{\tab}[1]{Tab.~\ref{tab:#1}}
\newcommand{\para}[1]{Sec.~\ref{para:#1}}
\newcommand{\ap}[1]{Appendix~\ref{ap:#1}}
\renewcommand{\appendixtocname}{Annexes} 
\renewcommand{\appendixpagename}{Annexes}
\newcommand{\ul}{\underline}
%
\makeatother

%\fancyhf{}%
% Utiliser \fancyhead[L] pour mettre du texte dans la partie de gauche
% du header
%\fancyhead[L]{}%
%\fancyhead[R]{\thepage}%
%\renewcommand{\headrulewidth}{0pt} % ça c'est pour faire une ligne horizontale
%\headsep=25pt % ça c'est pour laisser de la place entre le header et le texte
%\pagestyle{fancy}%

% margins
\setlength{\topmargin}{-0.75in}
\setlength{\textheight}{9in}
\setlength{\oddsidemargin}{-.50in}
\setlength{\textwidth}{7.5in}



\begin{document}


\title{Modèle de lentille parfaite}

\maketitle
\begin{center}
\author{\bf|Méso|Star>}\\
 (\url{https://www.meso-star.com}) \\
\end{center}
\vspace{1cm}

%\tableofcontents

\newpage

%============================================================================================================

Ce document décrit un modèle de lentille parfaite, pour mise en oeuvre dans HTRDR.

Un système photographique a pour but de créer une image d'une scène donnée sur un plan, dit plan focal, où
est placé un capteur (grille de pixels). Le fonctionnement est radicalement différent d'une caméra sténopé, où on considère que
chaque pixel ne capte que le rayonnement incident dans un angle solide propre au pixel (la somme de ces
angles solides individuels constituant l'angle solide d'ouverture total de la caméra). Dans une caméra
munie d'une ou de plusieurs lentilles, chaque pixel capte le rayonnement incident sur tout un hémisphère.
Etant donné qu'une partie de cet hémisphère est masqué (le capteur est placé dans une cavité, ouverte
sur le rayonnement externe uniquement via le système de lentilles), chaque pixel reçoit en fait le
rayonnement émergent de la lentille. L'idée derrière l'utilisation d'une lentille est que tout le rayonnement
incident dans une direction donnée est focalisé en une unique position sur le capteur. Ainsi, chaque pixel
ne capte en réalité que le rayonnement incident dans un angle solide très petit, d'autant plus petit que
les dimensions du pixel sont réduites.

\begin{figure}[!h]
\centering
\includegraphics[width=0.80\textwidth,angle=0]{./figures/spherical_lens.png}
\caption[]{Lentille, définie comme la zone d'intersection de deux boules: l'une de centre $O_{1}$ et de rayon $R_{1}$,
  l'autre de centre $O_{2}$ et de rayon $R_{2}$}
\label{fig:spherical_lens}
\end{figure}

Etant donné que le but du système optique d'un appareil photo ou d'une caméra est de réaliser la convergence
des trajectoires optiques incidentes, on ne traitera ici que le cas des lentilles convergentes. La figure \ref{fig:spherical_lens}
présente par exemple le cas d'une lentille définie comme la zone d'intersection de deux boules. A noter que
dans ce cas là, la position du point focal image dépend légèrement de la distance entre les rayons incidents
(parallèles entre eux) et le centre optique de la lentille: on parle d'astigmatisme. D'autres défauts
affectent les systèmes optiques physiques: aberration chromatique, aberration de sphéricité, production
d'artefacts par réfractions internes multiples, etc.

Le modèle de lentille le plus simple qui est décrit ci-après, est également celui qui est dépourvu de
la majorité de ces défauts. La forme exacte de la lentille n'est pas décrite; la lentille est entièrement
déterminée par deux paramètres que sont: son diamètre $D$ et sa distance focale $f$. Etant donné que la
géométrie de la lentille n'est pas détaillée, on la représente comme un objet 2D, placé en une position
donnée sur l'axe optique. On utilisera ici la convention $C$ pour le centre de la lentille. La distance
focale $f$ est la distance entre $C$ et le point focal image $F^{\prime}$ (ou $C$ et le point focal objet $F$)
où convergent toutes les trajectoires optiques parallèles entre elles, sans astigmatisme ni aberrations
chromatiques (l'indice de réfraction du matériau de la lentille n'est même pas spécifié, on sait juste
qu'il est indépendant de la fréquence dans ce modèle).

\begin{figure}[!h]
\centering
\includegraphics[width=0.80\textwidth,angle=0]{./figures/perfect_lens.png}
\caption[]{Modèle de lentille parfaite, entièrement caractérisée par son diamètre $D$ et sa distance focale $f$. Le point $C$
  est le centre optique (position de la lentille sur l'axe optique principal), le foyer image $F^{\prime}$ et le foyer
image $F$ sont situés à une distance $f$ du point $C$.}
\label{fig:perfect_lens}
\end{figure}

La figure \ref{fig:perfect_lens} illustre les règles qui régissent les constructions géométriques en
présence de ce modèle de lentille parfaite:
\begin{itemize}
\item les trajectoires incidentes passant par le centre optique $C$ ne sont pas déviées (trajectoire bleue)
\item les trajectoires incidentes parallèles à l'axe optique du côté objet convergent toutes au
  foyer image $F^{\prime}$ (trajectoires rouges)
\item identiquement, les trajectoires parallèles à l'axe optique du côté image convergent toutes
  au foyer objet $F$, symétrique de $F^{\prime}$ par-rapport à $C$ (trajectoires oranges)
\end{itemize}

A partir de là, on peut imaginer reproduire le comportement du système optique répondant au
modèle suivant:

\begin{itemize}
\item On dispose d'une lentille de diamètre $D$ et de distance focale $f$
\item L'origine du repère $O$ est située au centre du capteur, l'axe optique est l'axe (O$\vec{x}$), les
  axes $\vec{y}$ et $\vec{z}$ sont définis de façon arbitraire
\item Soit $d$ la distance entre le capteur et le foyer image $F^{\prime}$; on verra plus tard comment
  la valeur de ce paramètre est déterminée
\end{itemize}

L'initialisation d'un trajet optique depuis le capteur, vers la scène, peut être réalisée
de la façon suivante:

\begin{figure}[!h]
\centering
\includegraphics[width=0.80\textwidth,angle=0]{./figures/model.png}
\caption[]{Construction géométrique permettant d'initialiser une trajectoire optique
  ($\vec{x_{1}}$,$\vec{u_{1}}$) de façon à produire une image sur un capteur situé à
une distance $d$ du foyer image $F^{\prime}$ de la lentille placée en $C$}
\label{fig:model}
\end{figure}

\begin{itemize}
\item Echantillonnage d'une position de départ $\vec{x_{0}}$ sur le capteur, ou plutôt sur un pixel
  donné puisqu'il s'agit {\it in-fine} de réaliser un calcul Monte-Carlo pour chaque pixel
\item Echantillonnage uniforme d'une position $\vec{x_{1}}$ sur le disque de diamètre $D$ centré
  en $C$ (donc à la distance $d+f$ de $O$) et perpendiculaire à l'axe (O$\vec{x}$); ce qui
  correspond à échantillonner un rayon $r=\frac{D}{2}\sqrt{r_{1}}$ et un angle $\theta=2\pi r_{2}$ avec
  $r_{1}$ et $r_{2}$ des nombres aléatoires uniformes sur $[0,1]$, pour obtenir:
  \begin{equation}
    \begin{cases}
      x_{1}(1)=d+f \\
      x_{1}(2)=r cos(\theta) \\
      x_{1}(3)=r sin(\theta)
    \end{cases}
  \end{equation}
\item La direction $\vec{u_{0}}$ est définie par les positions $\vec{x_{0}}$ et $\vec{x_{1}}$
\item La direction $\vec{u_{1}}$ est définie par les positions $\vec{x_{1}}$ et $\vec{x_{2}}$
\item La position $\vec{x_{2}}$ est elle-même définie de la façon suivante: on sait que tous les
  rayons incidents, du côté image, dans la direction $\vec{u_{0}}$, vont converger en une position
  unique $\vec{x_{2}}$ sur le plan focal objet. Or, la ligne parallèle à [$\vec{x_{0}}$,$\vec{x_{1}}$]
  et passant par $C$ (ligne bleue pointillée sur la figure \ref{fig:model}) n'est pas déviée, par
  définition. Elle intersecte donc le plan focal objet en $\vec{x_{2}}$. Il faut donc être en
  mesure de déterminer l'intersection entre le rayon ($C$,$\vec{u_{0}}$) et le plan $x=d+2f$.
\end{itemize}

Il nous reste à fixer la distance $d$ entre le capteur et le foyer image $F^{\prime}$; chaque valeur
de $d$ correspond à une mise au point particulière. On sait en effet que $F^{\prime}$ est le lieu où
convergent tous les rayons incidents (du côté objet) parallèles à l'axe optique. Ces trajectoires
correspondent à une mise au point ``à l'infini'': un objet situé à l'infini va en effet être relié
à la lentille par des trajectoires optiques parallèles à l'axe optique. On peut donc placer le
capteur sur le plan focal image ($d$=0), mais seuls les objets situés à l'infini seront bien
focalisés. Le reste de la scène sera hors focus.

\begin{figure}[!h]
\centering
\includegraphics[width=0.80\textwidth,angle=0]{./figures/focal.png}
\caption[]{Construction géométrique permettant de calculer la distance $d$ permettant de focaliser
sur un objet situé à une distance $L$ de la lentille}
\label{fig:focus}
\end{figure}

Pour un objet situé à une distance $L$ de la lentille (voir figure \ref{fig:focus}), on souhaite
maintenant calculer la distance $d$ entre le capteur et le point $F^{\prime}$ permettant d'obtenir
une image nette de l'objet (au détriment de tout le reste de la scène qui sera hors focus).
Obtenir une image nette signifie que toutes les trajectoires émises par un point de l'objet
vont finalement être focalisées en une seule position sur le capteur. Prenons par exemple le sommet
de l'objet, de hauteur $h$. On peut tracer une première trajectoire optique émise par ce point,
et passant par $C$: on sait que ce rayon n'est pas dévié. Une autre trajectoire optique émise
par le sommet de l'objet est le rayon parallèle à l'axe optique: on sait qu'il va être dirigé
vers le point $F^{\prime}$ après traversée de la lentille. L'intersection de ces deux trajectoires
(après passage de la lentille) définit la position du plan image permettant d'obtenir une
image nette de l'objet. C'est là que doit être placé le capteur pour une mise au point à
distance $L$.

Par construction géométrique, on a:

\begin{equation}
  \begin{cases}
    \frac{h}{L}=\frac{h^{\prime}}{f+d} \\
    \frac{h}{f}=\frac{h^{\prime}}{d}
  \end{cases}
\end{equation}

On en déduit rapidement:

\begin{equation}
  d=\frac{f}{\frac{L}{f}-1}
\end{equation}
    
Il s'agit de la distance $d$ permettant d'assurer une mise au point pour des objets situés
à une distance $L$ de la lentille. Ce paramètre doit également être laissé à l'appréciation de
l'utilisateur (en plus de $f$ et $D$) pour la réalisation de l'image.

\begin{figure}[!h]
\centering
\includegraphics[width=0.80\textwidth,angle=0]{./figures/multifocus.png}
\caption[]{Calcul de la position $\vec{x_{0}^{\prime}}$ pour une distance $d^{\prime}$, à partir
d'une trajectoire produite à partir d'une position initiale $\vec{x_{0}}$ pour une distance $d$.}
\label{fig:multifocus}
\end{figure}

Ou pas. Je m'explique: si on effectue le calcul de l'image pour une distance $d$ initiale
(voir figure \ref{fig:multifocus} où $d$=0), et qu'on conserve la valeur du poids pour chaque
trajectoire optique (ainsi que la position initiale $\vec{x_{0}}$ et la direction initiale
$\vec{u_{0}}$), il est toujours possible de calculer, en post-traitement, la position $\vec{x_{0}^{\prime}}$
correspondante à une autre distance $d^{\prime}$ de positionnement du capteur. Et à partir
de $\vec{x_{0}^{\prime}}$, il est facile d'identifier le pixel du capteur situé en $d^{\prime}$
auquel doit être affecté le poids de la réalisation en question. Autrement dit, il est possible, en
conservant l'information sur chaque réalisation MC, de post-traiter l'image pour \emph{n'importe
  quelle distance de mise au point $L$} ! Il suffit simplement que $d^{\prime}>d$ de façon à
assurer que tous les pixels du capteur situé en $d^{\prime}$ recoivent une information à
partir de l'image de référence calculée pour $d$ (à taille de capteur identique). D'où la
possibilité évidente de calculer l'image de référence pour $d$=0.


%============================================================================================================
\bibliographystyle{unsrt}
\bibliography{biblio}

\end{document}
