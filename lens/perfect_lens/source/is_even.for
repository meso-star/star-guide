c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      logical function is_even(num)
      implicit none
      include 'max.inc'
c
c     Purpose: to determine whether of not a given integer value is even
c
c     Input:
c       + num: integer value to test
c
c     Output:
c       + is_even: true if "num" is even; false otherwise
c
c     I/O
      integer num

      if (dble(num)/2.0D+0.eq.dble(num/2)) then
         is_even=.true.
      else
         is_even=.false.
      endif

      return
      end
