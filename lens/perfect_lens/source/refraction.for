c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine refraction(dim,u1,normal,n1,n2,refracted_part,u2)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the propagation direction after interface refraction
c     
c     Input:
c       + dim: dimension of space
c       + u1: propagation direction before hitting the interface
c       + normal: normal to the interface at the intersection position
c       + n1: refraction index of the popagation medium before hitting the interface
c       + n2: refraction index of the propagation medium after hitting the interface
c     
c     Output:
c       + refracted_part: true if ray "u2" exists; false if total refraction occurs and there is no refracted part into medium 2
c       + u2: propagation direction after hitting the interface
c     
c     I/O
      integer dim
      double precision u1(1:Ndim_mx)
      double precision normal(1:Ndim_mx)
      double precision n1
      double precision n2
      logical refracted_part
      double precision u2(1:Ndim_mx)
c     temp
      double precision minus_u1(1:Ndim_mx)
      double precision minus_normal(1:Ndim_mx)
      double precision p1,p2
      double precision theta1,theta2
      double precision theta1_lim
      double precision k(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subrotuine refraction'

      call scalar_vector(dim,-1.0D+0,u1,minus_u1)
      call scalar_product(dim,minus_u1,normal,p1)
      theta1=dacos(p1)

      refracted_part=.true.
      if (n1.gt.n2) then
c     theta1_lim: incoming angle above which total refraction occurs
         theta1_lim=dasin(n2/n1)
         if (theta1.gt.theta1_lim) then
            refracted_part=.false.
c     Debug
            write(*,*) 'n1=',n1
            write(*,*) 'n2=',n2
            write(*,*) 'theta1_lim=',theta1_lim
            write(*,*) 'theta1=',theta1
c     Debug
         endif
      endif
      if (refracted_part) then
         theta2=dasin(n1*dsin(theta1)/n2)
c     k: rotation axis that transforms 'normal' into '-n1'
         call vector_product_normalized(dim,normal,minus_u1,k)
c     rotation of '-normal' around axe 'k' by angle 'theta2' -> u2
         call scalar_vector(dim,-1.0D+0,normal,minus_normal)
         call rotation(dim,minus_normal,theta2,k,u2)
      endif 
      
      return
      end
