c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      program main
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     Variables
      integer dim
      character*(Nchar_mx) data_file
      character*(Nchar_mx) image_file
      integer Npixels(1:2)
      double precision sensor_size(1:2)
      double precision dOC
      double precision lens_diameter
      double precision f
      double precision target_size(1:2)
      integer Ncells(1:2)
      double precision dCT
      integer Nevent
c     temp
      integer iseed(4)
      integer ipix,jpix,event,j
      double precision delta
      double precision sum,sum2,contrib,weight
      double precision mean,variance,std_dev
      double precision pixel_size(1:2),pixel_dz
      double precision cell_size(1:2)
      double precision r
      double precision k0,k1
      double precision x0(1:Ndim_mx)
      double precision radius,phi
      double precision x1(1:Ndim_mx)
      double precision xC(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
      double precision x0x1(1:Ndim_mx)
      double precision x1x2(1:Ndim_mx)
      double precision u0(1:Ndim_mx)
      double precision u1(1:Ndim_mx)
      logical intersection_found
      double precision xint(1:Ndim_mx)
      integer cell_index(1:2)
      double precision green,blue,red,yellow
c     functions
      logical is_even
c     progress display
      integer len,ndone,ntot
      double precision fdone,fdone0
      integer ifdone,pifdone
      character*(Nchar_mx) str,source_str
      character*(Nchar_mx) fmt,fmt0
      integer err
c     label
      character*(Nchar_mx) label
      label='program main'

      dim=3
c     ----------------- print status
      len=6
      call num2str(len,str,err)
      if (err.eq.0) then
         fmt0='(a,i'//trim(str)//',a)'
         fmt='(i'//trim(str)//',a)'
      endif
c     ----------------- print status

      data_file='./data.in'
      image_file='./results/image.dat'
      call read_data(data_file,Npixels,sensor_size,dOC,lens_diameter,f,target_size,Ncells,dCT,Nevent)
      do j=1,2
         pixel_size(j)=sensor_size(j)/Npixels(j)
         cell_size(j)=target_size(j)/Ncells(j)
      enddo                     ! j
      xC(1)=dOC
      xC(2)=0.0D+0
      xC(3)=0.0D+0
      green=0.125
      blue=0.375
      red=0.625
      yellow=0.875

c     Initialization for the random number generator
      iseed(1)=0
      iseed(2)=1
      iseed(3)=2
      iseed(4)=3
      
c     Computation of the distance between the lens and the focal plane
      delta=f/(dCT/f-1.0D+0)
      write(*,*) 'Focal plane to lens distance:',delta+f,' m'

c     ----------------- print status
      write(*,*) 'MC computation...'
      write(*,trim(fmt0),advance='no')
     &     'Done:   ',floor(fdone0),' %'
      ndone=0
      ntot=Nevent*Npixels(1)*Npixels(2)
      pifdone=0
c     ----------------- print status
c     Loop over each pixel
      open(11,file=trim(image_file))
      do ipix=1,Npixels(1)
         do jpix=1,Npixels(2)
            sum=0.0D+0
            sum2=0.0D+0
            do event=1,Nevent
               weight=0.0D+0
c     sample pixel area: x0
               x0(1)=0.0D+0
               call dlaruv(iseed,1,r)
               x0(2)=(ipix-1)*pixel_size(1)+pixel_size(1)*r-sensor_size(1)/2.0D+0
               call dlaruv(iseed,1,r)
               x0(3)=(jpix-1)*pixel_size(2)+pixel_size(2)*r-sensor_size(2)/2.0D+0
c     sample lens area: x1
               x1(1)=dOC
               call dlaruv(iseed,1,r)
               radius=lens_diameter/2.0D+0*dsqrt(r)
               call dlaruv(iseed,1,r)
               phi=2.0D+0*pi*r
               x1(2)=radius*dcos(phi)
               x1(3)=radius*dsin(phi)
c     emission direction: u0
               call substract_vectors(dim,x1,x0,x0x1)
               call normalize_vector(dim,x0x1,u0)
c     The ray is (x0,u0)
c     Intersection between the (C,u0) line and the Object Focal Plane
               if (u0(1).le.0.0D+0) then
                  call error(label)
                  write(*,*) 'u0(1)=',u0(1)
                  write(*,*) 'x0=',x0
                  write(*,*) 'x1=',x1
                  write(*,*) 'should be > 0'
                  stop
               else
                  k0=f/u0(1)
               endif
               call direct_flight(dim,xC,u0,k0,x2)
c     Propagation direction after the lens: u1
               call substract_vectors(dim,x2,x1,x1x2)
               call normalize_vector(dim,x1x2,u1)
c     Now the ray is (x1,u1)
c     Intersection with the x=dOC+dCT plane
               if (u1(1).le.0.0D+0) then
                  call error(label)
                  write(*,*) 'u1(1)=',u1(1)
                  write(*,*) 'should be > 0'
                  stop
               else
                  k1=dCT/u1(1)
               endif
               call direct_flight(dim,x1,u1,k1,x3)
               if ((x3(2).ge.-target_size(1)/2.0D+0).and.
     &              (x3(2).le.target_size(1)/2.0D+0).and.
     &              (x3(3).ge.-target_size(2)/2.0D+0).and.
     &              (x3(3).le.target_size(2)/2.0D+0)) then
                  intersection_found=.true.
                  call duplicate_vector(dim,x3,xint)
               else
                  intersection_found=.false.
               endif
               if (intersection_found) then
                  do j=1,2
                     cell_index(j)=int((xint(j+1)+target_size(j)/2.0D+0)/cell_size(j))+1
                     if ((cell_index(j).lt.1).or.(cell_index(j).gt.Ncells(j))) then
                        call error(label)
                        write(*,*) 'cell_index(',j,')=',cell_index(j)
                        write(*,*) 'should be in the [1-',Ncells(j),'] range'
                        stop
                     endif
                  enddo         ! j
c     First cell is red, the next one is yellow, and so on
                  if ((is_even(cell_index(1)).and.(.not.is_even(cell_index(2)))).or.(is_even(cell_index(2)).and.(.not.is_even(cell_index(1))))) then
                     weight=yellow
                  else
                     weight=red
                  endif
               else
                  if (u1(3).le.0.0D+0) then
                     weight=green
                  else
                     weight=blue
                  endif
               endif            ! intersection_found
               
c     ----------------- print status
               ndone=ndone+1
               fdone=dble(ndone)/dble(ntot)*1.0D+2
               ifdone=floor(fdone)
               if (ifdone.gt.pifdone) then
                  do j=1,len+2
                     write(*,"(a)",advance='no') "\b"
                  enddo         ! j
                  write(*,trim(fmt),advance='no')
     &                 floor(fdone),' %'
                  pifdone=ifdone
               endif
c     ----------------- print status
               contrib=weight
               sum=sum+contrib
               sum2=sum2+contrib**2.0D+0
            enddo               ! event
            call statistics(Nevent,sum,sum2,mean,variance,std_dev)
            write(11,*) ipix,jpix,mean,std_dev
         enddo                  ! jpix
         write(11,*)
      enddo                     ! ipix
      write(*,*)
      write(*,*) '...done'
      close(11)

      end
