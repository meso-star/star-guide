c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine num2str(num,str,err_code)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to convert an integer to a character string
c
c     Input:
c       + num: integer
c     
c     Output:
c       + str: character string
c       + err_code: false means conversion is OK; true means "num" was not recognized.
c
c     I/O
      integer num
      character*(Nchar_mx) str
      logical err_code
c     temp
      integer absnum,sign
      character*(Nchar_mx) f
c     label
      character*(Nchar_mx) label
      label='subroutine num2str'

      if (num.lt.0) then
         sign=-1
      else
         sign=1
      endif
      absnum=abs(num)
      
      err_code=.false.
      if ((absnum.ge.0).and.(absnum.lt.10)) then
         write(str,101) absnum
      else if ((absnum.ge.10).and.(absnum.lt.100)) then
         write(str,102) absnum
      else if ((absnum.ge.100).and.(absnum.lt.1000)) then
         write(str,103) absnum
      else if ((absnum.ge.1000).and.(absnum.lt.10000)) then
         write(str,104) absnum
      else if ((absnum.ge.10000).and.(absnum.lt.100000)) then
         write(str,105) absnum
      else if ((absnum.ge.100000).and.(absnum.lt.1000000)) then
         write(str,106) absnum
      else if ((absnum.ge.1000000).and.(absnum.lt.10000000)) then
         write(str,107) absnum
      else if ((absnum.ge.10000000).and.(absnum.lt.100000000)) then
         write(str,108) absnum
      else if ((absnum.ge.100000000).and.(absnum.lt.1000000000)) then
         write(str,109) absnum
      else
         err_code=.true.
         goto 666
      endif

      if (sign.eq.-1) then
         str="-"//trim(str)
      endif
      
 666  continue
      return
      end



      subroutine num2str1(num,str1)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to convert an integer to a character string of size 1
c
c     Input:
c       + num: integer
c     
c     Output:
c       + str2: character string
c

c     I/O
      integer num
      character*1 str1
c     temp
      character*1 kch1
c     label
      character*(Nchar_mx) label
      label='subroutine num2str1'

      if ((num.ge.0).and.(num.lt.10)) then
         write(kch1,11) num
         str1=trim(kch1)
      else
         call error(label)
         write(*,*) 'num=',num,' >= 10'
         stop
      endif

      return
      end



      subroutine num2str2(num,str2)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to convert an integer to a character string of size 2
c
c     Input:
c       + num: integer
c     
c     Output:
c       + str2: character string
c

c     I/O
      integer num
      character*2 str2
c     temp
      character*1 zeroch,kch1
      character*2 kch2
c     label
      character*(Nchar_mx) label
      label='subroutine num2str2'

      write(zeroch,11) 0
      if ((num.ge.0).and.(num.lt.10)) then
         write(kch1,11) num
         str2=trim(zeroch)//trim(kch1)
      else if ((num.ge.10).and.(num.lt.100)) then
         write(kch2,12) num
         str2=trim(kch2)
      else
         call error(label)
         write(*,*) 'num=',num,' >= 100'
         stop
      endif

      return
      end



      subroutine num2str3(num,str3)
      implicit none
      include 'max.inc'
c
c     Purpose: to convert an integer to a character string of size 3
c
c     Input:
c       + num: integer
c     
c     Output:
c       + str3: character string
c

c     I/O
      integer num
      character*3 str3
c     temp
      character*(Nchar_mx) str_tmp
      logical err_code
c     label
      character*(Nchar_mx) label
      label='subroutine num2str3'

      call num2str(num,str_tmp,err_code)
      if (err_code) then
         call error(label)
         write(*,*) 'Problem converting to character string'
         write(*,*) 'num=',num
         stop
      endif

      if (num.lt.10) then
         str3='00'//trim(str_tmp)
      else if (num.lt.100) then
         str3='0'//trim(str_tmp)
      else if (num.lt.1000) then
         str3=trim(str_tmp)
      else
         call error(label)
         write(*,*) 'unable to convert:'
         write(*,*) 'num=',num,' > 1000'
         write(*,*) 'to a string of 3 characters'
         stop
      endif

      return
      end



      subroutine num2str4(num,str4)
      implicit none
      include 'max.inc'
c
c     Purpose: to convert an integer to a character string of size 4
c
c     Input:
c       + num: integer
c     
c     Output:
c       + str4: character string
c

c     I/O
      integer num
      character*4 str4
c     temp
      character*(Nchar_mx) str_tmp
      logical err_code
c     label
      character*(Nchar_mx) label
      label='subroutine num2str4'

      call num2str(num,str_tmp,err_code)
      if (err_code) then
         call error(label)
         write(*,*) 'Problem converting to character string'
         write(*,*) 'num=',num
         stop
      endif

      if (num.lt.10) then
         str4='000'//trim(str_tmp)
      else if (num.lt.100) then
         str4='00'//trim(str_tmp)
      else if (num.lt.1000) then
         str4='0'//trim(str_tmp)
      else if (num.lt.10000) then
         str4=trim(str_tmp)
      else
         call error(label)
         write(*,*) 'unable to convert:'
         write(*,*) 'num=',num,' > 1000'
         write(*,*) 'to a string of 3 characters'
         stop
      endif

      return
      end
