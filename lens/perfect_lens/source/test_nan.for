c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine test_nan(value,is_nan)
      implicit none
      include 'max.inc'
c     
c     Purpose: to test a given value against NaN
c     
c     Input:
c       + value: double-precision data
c     
c     Output:
c       + is_nan: T if "value" in NaN
c     
c     I/O
      double precision value
      logical is_nan
c     label
      character*(Nchar_mx) label
      label='subroutine test_nan'
c
      if (value.ne.value) then
         is_nan=.true.
      else
         is_nan=.false.
      endif
c
      return
      end
