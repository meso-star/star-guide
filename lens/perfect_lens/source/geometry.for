c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)

      subroutine focal_distance(n,R1,R2,dO1O2,f)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the focal distance for a given lens geometry
c     
c     Input:
c       + n: refraction index of the lens material
c       + R1: radius of the first sphere (on the right) [m]
c       + R2: radius of the second sphere (on the left) [m]
c       + dO1O2: distance between sphere centers [m]
c       + 
c     
c     Output:
c       + focal distance [m}
c     
c     I/O
      double precision n
      double precision R1
      double precision R2
      double precision dO1O2
      double precision f
c     temp
      double precision vergence
c     label
      character*(Nchar_mx) label
      label='subroutine focal_distance'

      vergence=(n-1.0D+0)*(1.0D+0/R1+1.0D+0/R2)
c     &     -dO1O2*(n-1.0D+0)/(n*R1*R2))
      f=1.0D+0/vergence
      
      return
      end


      
      subroutine direct_flight(dim,x1,u,length,x2)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute x2=x1+length*u
c
c     Input:
c       + dim: space dimension
c       + x1: starting position
c       + u: flight direction
c       + length: flight length
c     
c     Output:
c       + x2=x1+length*u
c
c     I/O
      integer dim
      double precision x1(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision length
      double precision x2(1:Ndim_mx)
c     temp
      double precision flight(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine direct_flight'

      call scalar_vector(dim,length,u,flight)
      call add_vectors(dim,x1,flight,x2)

      return
      end

      
      
      subroutine triangle_area(dim,p1,p2,p3,area)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the area of a triangle
c     
c     Input:
c       + dim: dimension of space 
c       + p1: coordinates of the 1st point
c       + p2: coordinates of the 2nd point
c       + p3: coordinates of the 3rd point
c     
c     Output:
c       + area: area of the triangle
c     
c     I/O
      integer dim
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision p3(1:Ndim_mx)
      double precision area
c     temp
      double precision v12(1:Ndim_mx)
      double precision v13(1:Ndim_mx)
      double precision w(1:Ndim_mx)
      double precision length
c     label
      character*(Nchar_mx) label
      label='subroutine triangle_area'

      call substract_vectors(dim,p2,p1,v12)
      call substract_vectors(dim,p3,p1,v13)
      call vector_product(dim,v12,v13,w)
      call vector_length(dim,w,length)
      area=length/2.0D+0

      return
      end



      subroutine tetrahedron_volume(dim,p1,p2,p3,p4,volume)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the volume of a tetrahedron from the
c     coordinates of its 4 nodes.
c     Note: the volume will be positive when (p1, p2, p3) define
c     a triangular base of the tetrahedron, provided a order such
c     that vector n=(p1p2)x(p1p3) is directed in the same direction
c     as vector (p1p4).
c     
c     Input:
c       + dim: dimension of space 
c       + p1: coordinates of the 1st point
c       + p2: coordinates of the 2nd point
c       + p3: coordinates of the 3rd point
c       + p4: coordinates of the 4th point
c     
c     Output:
c       + area: area of the triangle
c     
c     I/O
      integer dim
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision p3(1:Ndim_mx)
      double precision p4(1:Ndim_mx)
      double precision volume
c     temp
      double precision u12(1:Ndim_mx)
      double precision u13(1:Ndim_mx)
      double precision u14(1:Ndim_mx)
      double precision n(1:Ndim_mx)
      double precision product
c     label
      character*(Nchar_mx) label
      label='subroutine tetrahedron_volume'

      call substract_vectors(dim,p2,p1,u12)
      call substract_vectors(dim,p3,p1,u13)
      call substract_vectors(dim,p4,p1,u14)
      call vector_product(dim,u12,u13,n)
      call vector_vector(dim,n,u14,product)
      volume=product/6.0D+0
      
      return
      end


      
      subroutine distance_to_vector(dim,x1,x2,P,dmin,A,
     &     A_is_at_end,end_side)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the distance between a given vector and a position
c     
c     Input:
c       + dim: dimension of space
c       + x1: first position that defines the vector
c       + x2: second position that defines the vector
c       + P: position
c     
c     Output:
c       + dmin: smallest distance between P and vector
c       + A: closest position to P that belongs to the vector
c       + A_is_at_end: true if A is at one end of the [x1,x2] segment
c       + end_side: index (1 or 2) or the end side if A_is_at_end=T
c     
c     I/O
      integer dim
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision P(1:Ndim_mx)
      double precision dmin
      double precision A(1:Ndim_mx)
      logical A_is_at_end
      integer end_side
c     temp
      integer j
      double precision u(1:Ndim_mx)
      double precision v(1:Ndim_mx)
      double precision nu(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision AP(1:Ndim_mx)
      double precision norm_u,norm_v,L1,d,sp
c     label
      character*(Nchar_mx) label
      label='subroutine distance_to_vector'

      call substract_vectors(dim,x2,x1,u)
      call normalize_vector(dim,u,nu)
      call substract_vectors(dim,P,x1,v)
      call vector_length(dim,u,norm_u)
      call vector_length(dim,v,norm_v)
      call vector_vector(dim,u,v,sp)
      if (norm_u.le.0.0D+0) then
         call error(label)
         write(*,*) 'norm_u=',norm_u
         write(*,*) 'u=',u
         stop
      else
         L1=sp/norm_u
      endif
      if (L1.le.0.0D+0) then
         call copy_vector(dim,x1,A)
         do j=1,dim
            tmp(j)=0.0D+0
         enddo                  ! j
         A_is_at_end=.true.
         end_side=1
      else if (L1.ge.norm_u) then
         call copy_vector(dim,x2,A)
         call copy_vector(dim,u,tmp)
         A_is_at_end=.true.
         end_side=2
      else
         call scalar_vector(dim,L1,nu,tmp)
         call add_vectors(dim,x1,tmp,A)
         A_is_at_end=.false.
      endif
      call substract_vectors(dim,v,tmp,AP)
      call vector_length(dim,AP,dmin)

      return
      end



      subroutine line_sphere_forward_intersect(debug,dim,
     &     origin,u,center,radius,Pint,d2int)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the intersection position between a line
c     and a sphere in the case when the origin of the line is inside
c     the sphere: there must be only one intersection in the
c     forward direction.
c
c     Input:
c       + dim: dimension of the vectors
c       + origin: cartesian coordinates of the origin of the line
c       + u: cartesian coordinates of the direction vector
c       + center: cartesian coordinates of the sphere center
c       + radius: radius of the sphere
c
c     Output:
c       + Pint: cartesian coordinates of the intersection position
c       + d2int: distance to intersection position
c
c     I/O
      logical debug
      integer dim
      double precision origin(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision center(1:Ndim_mx)
      double precision radius
      double precision Pint(1:Ndim_mx)
      double precision d2int
c     temp
      integer n_int
      double precision P0(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision k0,k1,k2
      double precision dOC
      double precision epsilon
c     label
      character*(Nchar_mx) label
      label='subroutine line_sphere_forward_intersect'

      epsilon=1.0D-8*radius
      
      call distance(dim,origin,center,dOC)
      if (dOC-radius.gt.epsilon) then
         call error(label)
         write(*,*) 'Distance origin-center=',dOC
         write(*,*) 'should be < radius=',radius
         stop
      endif
      
      call line_sphere_intersect(debug,dim,
     &     origin,u,center,radius,n_int,
     &     P0,k0,P1,k1,P2,k2)
      if (n_int.le.0) then
         call error(label)
         write(*,*) 'No intersection found'
         stop
      else if (n_int.eq.1) then
         call copy_vector(dim,P0,Pint)
      else if (n_int.eq.2) then
         if ((k1.gt.epsilon).and.(k2.gt.epsilon)) then
            call error(label)
            write(*,*) 'k1=',k1
            write(*,*) 'k2=',k2
            write(*,*) 'should be both < epsilon=',epsilon
            stop
         else
            if (k1.gt.epsilon) then
               call copy_vector(dim,P1,Pint)
               d2int=k1
            else if (k2.gt.epsilon) then
               call copy_vector(dim,P2,Pint)
               d2int=k2
            else
               call error(label)
               write(*,*) 'No suitable intersection found:'
               write(*,*) 'k1=',k1
               write(*,*) 'k2=',k2
               stop
            endif
         endif
      endif

      return
      end


      
      subroutine line_sphere_intersect(debug,dim,
     &     origin,u,center,radius,n_int,
     &     P0,k0,P1,k1,P2,k2)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the intersection point(s) between a line
c     and a sphere in the general case (not centered at the origin)
c     Update 2011-07-23:
c     only valid solutions (intersections met in the direction of propagation)
c     are given.
c
c     Input:
c       + dim: dimension of the vectors
c       + origin: cartesian coordinates of the origin of the line
c       + u: cartesian coordinates of the direction vector
c       + center: cartesian coordinates of the sphere center
c       + radius: radius of the sphere
c
c     Output:
c       + n_int: number of intersections
c       + P0: cartesian coordinates of the intersection point when n_int=1
c       + k0: index of k for the solution when n_int=1
c       + P1,P2: cartesian coordinates of the intersections point when n_int=2, closest position first
c       + k1,k2: indexes of k for the solutions when n_int=2, closest position first
c
c     I/O
      logical debug
      integer dim
      double precision origin(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision center(1:Ndim_mx)
      double precision radius
      integer n_int
      double precision P0(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision k0,k1,k2
c     temp
      double precision a,b,c,d
      integer nsol
      double precision un(1:Ndim_mx)
      double precision flight0(1:Ndim_mx)
      double precision flight1(1:Ndim_mx)
      double precision flight2(1:Ndim_mx)
      double precision epsilon_r
c     label
      character*(Nchar_mx) label
      label='subroutine line_sphere_intersect'

      epsilon_r=1.0D-5*radius

      call normalize_vector(dim,u,un)
      
      a=un(1)**2.0D+0+un(2)**2.0D+0+un(3)**2.0D+0
      b=2.0D+0*(un(1)*(origin(1)-center(1))
     &     +un(2)*(origin(2)-center(2))
     &     +un(3)*(origin(3)-center(3)))
      c=(origin(1)-center(1))**2.0D+0
     &     +(origin(2)-center(2))**2.0D+0
     &     +(origin(3)-center(3))**2.0D+0
     &     -radius**2.0D+0

c     find mathematical solutions
      call eq2deg(a,b,c,nsol,k0,k1,k2)

c     Debug
      if (debug) then
         write(*,*) 'a=',a
         write(*,*) 'b=',b
         write(*,*) 'c=',c
         write(*,*) 'nsol=',nsol
         write(*,*) 'k1=',k1
         write(*,*) 'k2=',k2
      endif
c     Debug

      n_int=nsol
      if (n_int.eq.1) then
         call scalar_vector(dim,k0,un,flight0)
         call add_vectors(dim,origin,flight0,P0)
         call distance(dim,center,P0,d)
         if (d.gt.epsilon_r) then
            n_int=0
            goto 666
         endif
      else if (n_int.eq.2) then
         call scalar_vector(dim,k1,un,flight1)
         call scalar_vector(dim,k2,un,flight2)
         call add_vectors(dim,origin,flight1,P1)
         call add_vectors(dim,origin,flight2,P2)
c     Debug
         if (debug) then
            write(*,*) 'P1=',P1
            write(*,*) 'P2=',P2
         endif
c     Debug
         call distance(dim,center,P1,d)
         if (dabs(d-radius).gt.epsilon_r) then
            n_int=0
            goto 666
         endif
         call distance(dim,center,P2,d)
         if (dabs(d-radius).gt.epsilon_r) then
            n_int=0
            goto 666
         endif
      endif

 666  continue
      return
      end


      
      subroutine eq2deg(a,b,c,nsol,x0,x1,x2)
      implicit none
      include 'max.inc'
c
c     Purpose: to solve a 2nd degree equation ax²+bx+c=0
c
c     Input:
c       + a,b,c: coefficients of the 2nd degree equation ax²+bx+c=0
c
c     Output:
c       + nsol: number of solution (0,1 or 2)
c       + x0: solution when nsol=1
c       + x1,x2: solutions when nsol=2 (x1<x2
c
c     I/O
      double precision a,b,c
      integer nsol
      double precision x0,x1,x2
c     temp
      double precision t1,t2
      double precision delta
c     label
      character*(Nchar_mx) label
      label='subroutine eq2deg'

      delta=b**2.0D+0-4.0D+0*a*c
      if (delta.lt.0.0D+0) then
         nsol=0
      else if (delta.eq.0.0D+0) then
         nsol=1
         x0=-b/(2.0D+0*a)
      else if (delta.gt.0.0D+0) then
         nsol=2
         t1=(-b-dsqrt(delta))/(2.0D+0*a)
         t2=(-b+dsqrt(delta))/(2.0D+0*a)
         if (t1.lt.t2) then
            x1=t1
            x2=t2
         else
            x1=t2
            x2=t1
         endif
      else
         call error(label)
         write(*,*) 'delta=',delta
         stop
      endif

      return
      end


      
      subroutine output_normal_on_sphere(dim,
     &     center,radius,P,normal)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the output normal to the sphere
c     for a given position
c
c     Input:
c       + dim: dimension of the vectors
c       + center: cartesian coordinates of the sphere center
c       + radius: radius of the sphere
c       + P: position on the sphere
c
c     Output:
c       + normal: output normal to the sphere @ P
c
c     I/O
      integer dim
      double precision center(1:Ndim_mx)
      double precision radius
      double precision P(1:Ndim_mx)
      double precision normal(1:Ndim_mx)
c     temp
      double precision CP(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine output_normal_on_sphere'

      call substract_vectors(dim,P,center,CP)
      call normalize_vector(dim,CP,normal)

      return
      end


      
      subroutine input_normal_on_sphere(dim,
     &     center,radius,P,normal)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the input normal to the sphere
c     for a given position
c
c     Input:
c       + dim: dimension of the vectors
c       + center: cartesian coordinates of the sphere center
c       + radius: radius of the sphere
c       + P: position on the sphere
c
c     Output:
c       + normal: output normal to the sphere @ P
c
c     I/O
      integer dim
      double precision center(1:Ndim_mx)
      double precision radius
      double precision P(1:Ndim_mx)
      double precision normal(1:Ndim_mx)
c     temp
      double precision PC(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine input_normal_on_sphere'

      call substract_vectors(dim,center,P,PC)
      call normalize_vector(dim,PC,normal)

      return
      end



      subroutine cartesian2spherical(x_cartesian,x_spherical)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to convert (x,y,z) cartesian coordinates
c     into (r,theta,phi) spherical coordinates
c
c     Input:
c       + x_cartesian: cartesian coordinates of the position: x (m), y (m), z (m)
c
c     Output:
c       + x_spherical: spherical coordinates of the position: r (m), theta (rad) in [-pi/2,pi/2], phi (rad) in [0:2*pi]
c
c     I/O
      double precision x_cartesian(1:Ndim_mx)
      double precision x_spherical(1:Ndim_mx)
c     temp
      double precision cosp,sinp,cost,sint
      double precision a,t1,t2
c     label
      character*(Nchar_mx) label
      label='subroutine cartesian2spherical'

      a=dsqrt(x_cartesian(1)**2.0D+0
     &     +x_cartesian(2)**2.0D+0)
      x_spherical(1)=dsqrt(x_cartesian(1)**2.0D+0
     &     +x_cartesian(2)**2.0D+0
     &     +x_cartesian(3)**2.0D+0)
      if (a.eq.0.0D+0) then
         cosp=1.0D+0
         sinp=0.0D+0
      else
         cosp=x_cartesian(1)/a
         sinp=x_cartesian(2)/a
      endif
      cost=a/x_spherical(1)
      sint=x_cartesian(3)/x_spherical(1)
      
      t1=dacos(dabs(cosp))
      if (x_cartesian(1).ge.0.0D+0) then
         if (sinp.ge.0.0D+0) then
            x_spherical(3)=t1
         else
            x_spherical(3)=2.0D+0*pi-t1
         endif
      else
         if (sinp.ge.0.0D+0) then
            x_spherical(3)=pi-t1
         else
            x_spherical(3)=pi+t1
         endif
      endif

      t2=dacos(cost)
      if (sint.ge.0.0D+0) then
         x_spherical(2)=t2
      else
         x_spherical(2)=-t2
      endif
      
      return
      end
