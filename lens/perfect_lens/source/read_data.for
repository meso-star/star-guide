c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine read_data(data_file,Npixels,sensor_size,dOC,lens_diameter,f,target_size,Ncells,dCT,Nevent)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read the input data file
c     
c     Input:
c       + data_file: file to read
c     
c     Output:
c       + Npixels: number of pixels along each direction
c       + sensor_size: total sensor size along each direction [m]
c       + dOC: distance between the sensor and the lesn [m]
c       + lens_diameter: lens diameter [m]
c       + f: lens focal distance [m]
c       + target_size: size of the target along each direction [m]
c       + Ncells: number of cells along each direction [m]
c       + dCT: distance between lens and target [m]
c       + Nevent: number of statistical realizations per pixel
c
c     I/O
      character*(Nchar_mx) data_file
      integer Npixels(1:2)
      double precision sensor_size(1:2)
      double precision dOC
      double precision lens_diameter
      double precision f
      double precision target_size(1:2)
      integer Ncells(1:2)
      double precision dCT
      integer Nevent
c     temp
      integer ios,i,j
c     label
      character*(Nchar_mx) label
      label='subrotuine read_data'

      open(11,file=trim(data_file),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(data_file)
         stop
      else
         do i=1,7
            read(11,*)
         enddo                  ! i
         read(11,*) Npixels(1)
         read(11,*) Npixels(2)
         read(11,*)
         read(11,*) sensor_size(1)
         read(11,*) sensor_size(2)
         read(11,*)
         read(11,*) dOC
         do i=1,6
            read(11,*)
         enddo                  ! i
         read(11,*) lens_diameter
         read(11,*)
         read(11,*) f
         do i=1,6
            read(11,*)
         enddo                  ! i
         read(11,*) target_size(1)
         read(11,*) target_size(2)
         read(11,*)
         read(11,*) Ncells(1)
         read(11,*) Ncells(2)
         read(11,*)
         read(11,*) dCT
         do i=1,6
            read(11,*)
         enddo                  ! i
         read(11,*) Nevent
      endif
      close(11)

      do j=1,2
         if (Npixels(j).le.0) then
            call error(label)
            write(*,*) 'Npixels(',j,')=',Npixels(j)
            write(*,*) 'should be > 0'
            stop
         endif
         if (sensor_size(j).le.0.0D+0) then
            call error(label)
            write(*,*) 'sensor_size(',j,')=',sensor_size(j)
            write(*,*) 'should be > 0'
            stop
         endif
         if (target_size(j).le.0.0D+0) then
            call error(label)
            write(*,*) 'target_size(',j,')=',target_size(j)
            write(*,*) 'should be > 0'
            stop
         endif
         if (Ncells(j).le.0) then
            call error(label)
            write(*,*) 'Ncells(',j,')=',Ncells(j)
            write(*,*) 'should be > 0'
            stop
         endif
      enddo                     ! j
      if (dOC.lt.0.0D+0) then
         call error(label)
         write(*,*) 'dOC=',dOC
         write(*,*) 'should be > 0'
         stop
      endif
      if (dCT.le.0.0D+0) then
         call error(label)
         write(*,*) 'dCT=',dCT
         write(*,*) 'should be > 0'
         stop
      endif
      if (lens_diameter.le.0.0D+0) then
         call error(label)
         write(*,*) 'lens_diameter=',lens_diameter
         write(*,*) 'should be > 0'
         stop
      endif
      if (f.le.0.0D+0) then
         call error(label)
         write(*,*) 'f=',f
         write(*,*) 'should be > 0'
         stop
      endif
      if (Nevent.le.0) then
         call error(label)
         write(*,*) 'Nevent=',Nevent
         write(*,*) 'should be > 0'
         stop
      endif

      return
      end
