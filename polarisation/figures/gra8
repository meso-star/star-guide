set term pngcairo enhanced color font "helvetica,20" linewidth 1 size 1280,720
set output "fig8.png"

R=1
ra=0.3
pl=1.20
theta=pi/4.0
phi=pi/5.0

set parametric
set urange [0:pi/2.0]
set xrange [-1:1]
set yrange [-1:1]
set nokey
unset border
unset xtics
unset ytics
unset ztics

#set size square

set label 'O' at 0.0,-0.1,0.0 textcolor 'black'
set arrow from 0,0,0 to R,0,0 head filled size screen 0.01,20 lc 'black' lw 2
set label 'X' at pl*R,0,0 textcolor 'black'
set arrow from 0,0,0 to 0,R,0 head filled size screen 0.01,20 lc 'black' lw 2
set label 'Y' at 0,pl*R,0 textcolor 'black'
set arrow from 0,0,0 to 0,0,R head filled size screen 0.01,20 lc 'black' lw 2
set label 'Z' at 0,0,pl*R textcolor 'black'

set view 50,110

f1x(u)=R*cos(u)
f1y(u)=0.0
f1z(u)=R*sin(u)

f2x(u)=0.0
f2y(u)=R*cos(u)
f2z(u)=R*sin(u)

f3x(u)=R*cos(u)
f3y(u)=R*sin(u)
f3z(u)=0.0

f4x(u)=R*cos(u)*cos(phi)
f4y(u)=R*cos(u)*sin(phi)
f4z(u)=R*sin(u)

g1x(u)=ra*cos(u*theta*2/pi)*cos(phi)
g1y(u)=ra*cos(u*theta*2/pi)*sin(phi)
g1z(u)=ra*sin(u*theta*2/pi)

g2x(u)=ra*cos(u*phi*2/pi)
g2y(u)=ra*sin(u*phi*2/pi)
g2z(u)=0.0

set arrow from 0,0,0 to R*cos(theta)*cos(phi),R*cos(theta)*sin(phi),R*sin(theta) head filled size screen 0.01,20 lc 'red' lw 2
set label '~u{.8{/Symbol \256}} ' at pl*R*cos(theta)*cos(phi),pl*R*cos(theta)*sin(phi),pl*R*sin(theta) textcolor 'red'

set arrow from 0,0,0 to R*cos(phi),R*sin(phi),0.0 nohead size screen 0.008,10 dashtype 2 lc 'red' lw 1

set label '{/Symbol q}' at pl*ra*cos(theta*2.0/3.0)*cos(phi),pl*ra*cos(theta*2.0/3.0)*sin(phi),pl*ra*sin(theta*2.0/3.0) textcolor 'red'
set label '{/Symbol f}' at pl*ra*cos(phi/2.0),pl*ra*sin(phi/2.0),0.0 textcolor 'red'

splot f1x(u),f1y(u),f1z(u) lw 2 lc 'blue' \
     ,f2x(u),f2y(u),f2z(u) lw 2 lc 'blue' \
     ,f3x(u),f3y(u),f3z(u) lw 2 lc 'blue' \
     ,f4x(u),f4y(u),f4z(u) lw 2 lc 'red' \
     ,g1x(u),g1y(u),g1z(u) lw 1 lc 'red' \
     ,g2x(u),g2y(u),g2z(u) lw 1 lc 'red'