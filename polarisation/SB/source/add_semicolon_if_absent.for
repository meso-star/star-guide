c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine add_semicolon_if_absent(st)
      implicit none
      include 'max.inc'
c
c     Purpose: to add a semicolon character ':' at the end of a character string
c     when there is none.
c
c     I/O:
c       + st: character string
c
c     I/O
      character*(Nchar_mx) st
c     temp
      character*1 sstring
      integer n
      character*(Nchar_mx) label
      label='subroutine add_semicolon_if_absent'

      sstring=':'

      n=len_trim(st)
      if (st(n:n).ne.sstring(1:1)) then
         st=trim(st)//sstring(1:1)
      endif

      return
      end
