c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine Zmatrix(dim,theta_in,phi_in,theta,phi,Z)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the total rotation matrix Z for rotating Stokes vectors
c     
c     Input:
c       + dim: dimension of space
c       + theta_in: latitude of incoming direction [rad] (-pi/2,pi/2)
c       + phi_in: longitude of incoming direction [rad] (0,2*pi)
c       + theta: zenithal scattering angle, defined in the local referential
c                of the incoming direction based on the meridian plane [rad] (0,pi)
c       + phi: azimuthal scattering angle, defined in the local referential
c                of the incoming direction based on the meridian plane [rad] (0,2*pi)
c     
c     Output:
c       + Z: total scattering matrix
c     
c     I/O
      integer dim
      double precision theta_in
      double precision phi_in
      double precision theta
      double precision phi
      double precision Z(1:4,1:4)
c     temp
      double precision r_in,r_out
      double precision theta_out,phi_out
      double precision x1(1:Ndim_mx)
      double precision y1(1:Ndim_mx)
      double precision z1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision y2(1:Ndim_mx)
      double precision z2(1:Ndim_mx)
      double precision n1(1:Ndim_mx)
      double precision n2(1:Ndim_mx)
      double precision n(1:Ndim_mx)
      double precision nprime(1:Ndim_mx)
      double precision alpha1,alpha2
      double precision axe(1:Ndim_mx)
      double precision M(1:Ndim_mx,1:Ndim_mx)
      double precision prod,p1,p2
      logical identical
      double precision R1(1:4,1:4)
      double precision R2(1:4,1:4)
      double precision P(1:4,1:4)
      double precision PR1(1:4,1:4)
c     parameters
      double precision epsilon_p
      double precision epsilon_alpha
      double precision epsilon_r
      parameter(epsilon_p=1.0D-8)
      parameter(epsilon_alpha=1.0D-8)
      parameter(epsilon_r=1.0D-8)
c     label
      character*(Nchar_mx) label
      label='subroutine Zmatrix'

c     z1: incoming direction
      r_in=1.0D+0
      call spher2cart(label,r_in,theta_in,phi_in,z1(1),z1(2),z1(3))
c     x1: x-axis of the local referential for the incoming direction
      x1(1)=r_in*dsin(theta_in)*dcos(phi_in)
      x1(2)=r_in*dsin(theta_in)*dsin(phi_in)
      x1(3)=-r_in*dcos(theta_in)
c     y1: y-axis of the local referential for the incoming direction
      call vectorial_product(dim,z1,x1,y1)
c     n1: normal to the meridian plane for the incoming direction
      call copy_vector(dim,y1,n1)
c     n: normal to the scattering plane
      call rotation_matrix(dim,phi,z1,M)
      call matrix_vector(dim,M,y1,n)
c     z2: scattered direction
      call rotation_matrix(dim,theta,n,M)
      call matrix_vector(dim,M,z1,z2)
c     check
      call scalar_product(dim,z1,z2,prod) ! z1.z2 should be equal to cos(theta)
      if (dabs(dcos(theta)-prod).gt.epsilon_p) then
         call error(label)
         write(*,*) 'cos(theta)=',dcos(theta)
         write(*,*) 'should be equal to z1.z2=',prod
         stop
      endif
c     check

c     theta_out,phi_out: latitude and longitude of scattered direction
      call cart2spher(z2(1),z2(2),z2(3),r_out,theta_out,phi_out)
c     check
      if (dabs(r_out-1.0D+0).gt.epsilon_r) then
         call error(label)
         write(*,*) 'r_out=',r_out
         write(*,*) 'should be equal to 1'
         stop
      endif
c     check
      
c     x2: x-axis of the local referential for the scattered direction
      x2(1)=r_out*dsin(theta_out)*dcos(phi_out)
      x2(2)=r_out*dsin(theta_out)*dsin(phi_out)
      x2(3)=-r_out*dcos(theta_out)
c     y2: y-axis of the local referential for the scattered direction
      call vectorial_product(dim,z2,x2,y2)
c     n2: normal to the meridian plane for the scattered direction
      call copy_vector(dim,y2,n2)

c     alpha1 is the rotation angle between the meridian plane for the incoming direction
c     and the scattering plane; it should be equal to phi
      call scalar_product(dim,n1,n,p1)
      alpha1=dacos(p1)
c     check
      if (dabs(alpha1-phi).gt.epsilon_alpha) then
         call error(label)
         write(*,*) 'alpha1=',alpha1
         write(*,*) 'should be equal to phi=',phi
         stop
      endif
c     check
      
c     alpha2 is the rotation angle between the meridian plane for the scattered direction
c     and the scattering plane
      call scalar_product(dim,n2,n,p2)
      alpha2=dacos(p2)

c     consistency check: y2 rotated by alpha2 around z2 should be equal to n
      call rotation_matrix(dim,alpha2,z2,M)
      call matrix_vector(dim,M,y2,nprime)
      call identical_vectors(dim,n,nprime,identical)
      if (.not.identical) then
         call error(label)
         write(*,*) 'nprime=',nprime
         write(*,*) 'should be equal to n=',n
         stop
      endif

c     production of the scattering matrix
      call Rmatrix(alpha1,R1)
      call Rmatrix(-alpha2,R2)
      call Rayleigh_Pmatrix(theta,P)
      call multiply_matrix(4,P,R1,PR1)
      call multiply_matrix(4,R2,PR1,Z)
      
      return
      end


      
      subroutine Rmatrix(alpha,R)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the rotation matrix for a rotation
c     of the [I,Q,U,V] Stokes vector around the propagation direction
c     
c     Input:
c       + alpha: rotation angle
c     
c     Output:
c       + R: rotation matrix
c     
c     I/O
      double precision alpha
      double precision R(1:4,1:4)
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine Rmatrix'
c
      R(1,1)=1.0D+0
      R(1,2)=0.0D+0
      R(1,3)=0.0D+0
      R(1,4)=0.0D+0
c     
      R(2,1)=0.0D+0
      R(2,2)=dcos(2.0D+0*alpha)
      R(2,3)=dsin(2.0D+0*alpha)
      R(2,4)=0.0D+0
c     
      R(3,1)=0.0D+0
      R(3,2)=-dsin(2.0D+0*alpha)
      R(3,3)=dcos(2.0D+0*alpha)
      R(3,4)=0.0D+0
c     
      R(4,1)=0.0D+0
      R(4,2)=0.0D+0
      R(4,3)=0.0D+0
      R(4,4)=1.0D+0
c
      return
      end



      
      subroutine Rayleigh_Pmatrix(theta,P)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the particulate scattering matrix for a scattering angle
c     for isotropic Rayleigh scattering
c     
c     Input:
c       + theta: scattering angle
c     
c     Output:
c       + P: particulate scattering matrix
c     
c     I/O
      double precision theta
      double precision P(1:4,1:4)
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine Rayleigh_Pmatrix'
c
      P(1,1)=0.75D+0*(1.0D+0+dcos(theta)**2.0D+0)
      P(1,2)=-0.75D+0*dsin(theta)**2.0D+0
      P(1,3)=0.0D+0
      P(1,4)=0.0D+0
c
      P(2,1)=-0.75D+0*dsin(theta)**2.0D+0
      P(2,2)=0.75D+0*(1.0D+0+dcos(theta)**2.0D+0)
      P(2,3)=0.0D+0
      P(2,4)=0.0D+0
c
      P(3,1)=0.0D+0
      P(3,2)=0.0D+0
      P(3,3)=1.50D+0*dcos(theta)
      P(3,4)=0.0D+0
c
      P(4,1)=0.0D+0
      P(4,2)=0.0D+0
      P(4,3)=0.0D+0
      P(4,4)=1.50D+0*dcos(theta)
c      
      return
      end
