c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      program main
      implicit none
      include 'max.inc'
      include 'param.inc'
c     
c     Purpose: test program for polarized radiative transfer
c     
c     Variables
      integer dim
      double precision theta_in,phi_in
      double precision theta,phi
      double precision Z(1:4,1:4)
      integer i,j
c     label
      character*(Nchar_mx) label
      label='program main'

      dim=3
      theta_in=pi/5.0D+0
      phi_in=pi/6.0D+0
      theta=pi/6.0D+0
      phi=pi/3.0D+0

      call Zmatrix(dim,theta_in,phi_in,theta,phi,Z)

c     Debug
      write(*,*) 'Z='
      do i=1,4
         write(*,*) (Z(i,j),j=1,4)
      enddo                     ! i
c     Debug
      
      end
