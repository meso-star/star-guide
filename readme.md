This repository is the resting place for all Méso-Star internal documentation that can be publicly accessed.

Prerequisites: **pdflatex** and **bibtex** in order to compile LaTeX sources; **gitfat** in order to download binary files (see instructions below); a **fortran compiler** in order to generate executables from fortran source files, and **gnuplot** in order to generate image files from the output of these programs.

Binary files (i.e. image files) are stored on gitlab using git-lfs. In order to get these files, you will have to use the "git-lfs" command. After a successful cloning of the present repository, the following commands should download binary files in your local directory:

>git-lfs fetch

>git-lfs checkout



# RT_algorithms

Extensive description of Monte-Carlo algorithms for radiative transfer. The document starts with a introduction to the Monte-Carlo method, with many examples and the corresponding code. Then, starting from the radiative transfer equation, we provide a step-by-step mathematical formal solution for the intensity field, in homogeneous absorbing, emitting and scattering semi-transparent participating media, both for radiation that is emitted by the medium and for external radiation. The Monte-Carlo algorithms that are based on these integral formulations are then provided and described. Variants may be discussed. Most of these algorithms have been implemented in the HTRDR software. Language: english.

The main pdf file is not provided; however, it may be compiled from source files (tex and images) using the "f1" script, provided that the "pdflatex" and "bibtex" commands are available.


# Polarization

A beginner's guide about the polarization phenomenon: the mathematical formalism is first provided, using reference textbooks. Then the possibility to simulate polarization in our reverse Monte-Carlo codes is discussed. Language: french.

The main pdf file is not provided; however, it may be compiled from source files (tex and images) using the "f1" script, provided that the "pdflatex" and "bibtex" commands are available.


# spectrum2color

A guide about XYZ colorimetry. Base concepts of colorimetry are provided, and the way a color may be computed from a visible spectral signal is detailled. The companion code "spectrum2colour" provides an implementation of the method. Language: english.

The main pdf file is not provided; however, it may be compiled from source files (tex and images) using the "f1" script, provided that the "pdflatex" and "bibtex" commands are available.


# Thermique statistique

A guide about the resolution of thermal processes problems using statistical algorithms. A short but complete description of the technique, preceded by a introduction to the Monte-Carlo method, with detailled examples, and a appendix describing sampling procedures that are involved in the various Monte-Carlo algorithms. Language: french.

## License

The contents of this repository (LaTeX files, images and source code) are released under the CeCILL v2.1 license. You are welcome to redistribute it under certain conditions; refer to the license for details.
