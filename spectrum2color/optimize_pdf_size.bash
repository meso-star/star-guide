#!/bin/bash
gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH \
    -sOutputFile=colorimetry.pdf spectrum2color.pdf
exit 0
