c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine extract_albedo_signal(Nmaterial,Nlambda,
     &     material_name,sdata,material,
     &     albedo)
      implicit none
      include 'max.inc'
c     
c     Purpose: extract the albedo data for a given material
c     
c     Input:      
c       + Nmaterial: number of materials
c       + Nlambda: number of wavelength points
c       + material_name: array of material identification
c       + sdata: array of spectral data for each material, each wavelength
c       + material: name of the material data has to be extracted for
c     
c     Output:
c       + albedo: albedo signal for the required material
c     
c     I/O
      integer Nmaterial
      integer Nlambda
      character*(Nchar_mx) material_name(1:Nmaterial_mx)
      double precision sdata(1:Nlambda_mat_mx,1:Nmaterial_mx)
      character*(Nchar_mx) material
      double precision albedo(1:Nlambda_mat_mx)
c     temp
      integer i,mat_index
      logical mat_found
c     label
      character*(Nchar_mx) label
      label='subroutine extract_albedo_signal'

      mat_found=.false.
      do i=1,Nmaterial
         if (trim(material_name(i)).eq.trim(material)) then
            mat_found=.true.
            mat_index=i
            goto 111
         endif
      enddo                     ! i
 111  continue
      if (mat_found) then
         do i=1,Nlambda
            albedo(i)=sdata(i,mat_index)/1.0D+2 ! 0-100 -> 0-1
         enddo                  ! i
      else
         call error(label)
         write(*,*) 'Material not found: ',trim(material)
         stop
      endif

      return
      end
