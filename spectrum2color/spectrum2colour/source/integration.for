c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine integration_discrete(Np,x,f,S)
      implicit none
      include 'max.inc'
c     
c     Purpose: to perform the integration of a function defined a discrete positions
c     
c     Input:
c       + Np: number of points that define x/f
c       + x: Np values of x
c       + f: corresponding Np values of the function to integrate
c     
c     Output:
c       + S: integral of f over x
c     
c     I/O
      integer Np
      double precision x(1:Np_mx)
      double precision f(1:Np_mx)
      double precision S
c     temp
      integer i
      double precision sum
c     label
      character*(Nchar_mx) label
      label='subroutine integration'

      sum=0.0D+0
      do i=1,Np
         sum=sum+f(i)
      enddo                     ! i
      S=sum*5.0D-3
      
      return
      end


      
      subroutine integration_intervals(Np,x,f,S)
      implicit none
      include 'max.inc'
c     
c     Purpose: to perform a integration of a function defined by intervals
c     
c     Input:
c       + Np: number of points that define x
c       + x: Np values of x
c       + f: corresponding values of the function for the (Np-1) intervals
c     
c     Output:
c       + S: integral of f over x
c     
c     I/O
      integer Np
      double precision x(1:Np_mx)
      double precision f(1:Np_mx)
      double precision S
c     temp
      integer i
      double precision sum
c     label
      character*(Nchar_mx) label
      label='subroutine integration'

      sum=0.0D+0
      do i=1,Np-1
         sum=sum+f(i)*(x(i+1)-x(i))
      enddo                     ! i
      S=sum/2.0D+0
      
      return
      end
