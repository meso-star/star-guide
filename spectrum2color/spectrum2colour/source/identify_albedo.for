c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine identify_albedo(Nlambda,lambda,dt,wvl,albedo)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the albedo for a given wavelength
c     
c     Input:
c       + Nlambda: number of wavelength points
c       + lambda: array of wavelengths
c       + dt: albedo signal
c       + wvl: wavelength the albedo has to be identified for
c     
c     Output:
c       + albedo: value of the albedo for wvl
c     
c     I/O
      integer Nlambda
      double precision lambda(1:Nlambda_mat_mx)
      double precision dt(1:Nlambda_mat_mx)
      double precision wvl
      double precision albedo
c     temp
      logical int_found
      integer int,i
      double precision dt1,dt2
c     label
      character*(Nchar_mx) label
      label='subroutine identify_albedo'

      if (wvl.lt.lambda(1)) then
         albedo=dt(1)
      else if (wvl.gt.lambda(Nlambda)) then
         albedo=dt(Nlambda)
      else
         int_found=.false.
         do i=1,Nlambda-1
            if ((wvl.ge.lambda(i)).and.(wvl.le.lambda(i+1))) then
               int_found=.true.
               int=i
               goto 111
            endif
         enddo                  ! i
 111     continue
         if (int_found) then
            dt1=dt(int)
            dt2=dt(int+1)
            albedo=dt1+(dt2-dt1)
     &           *(wvl-lambda(int))
     &           /(lambda(int+1)-lambda(int))
         else
            call error(label)
            write(*,*) 'wvl=',wvl
            write(*,*) 'could not be identified in lambda range:'
            write(*,*) 'lambda(1)=',lambda(1)
            write(*,*) 'lambda(',Nlambda,')=',lambda(Nlambda)
            stop
         endif
      endif                     ! wvl

      return
      end

      

      subroutine average_albedo_over_interval(Nlambda,lambda,albedo,
     &     lambda_min,lambda_max,average_albedo)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute to average of the albedo
c     over a given spectral interval
c     
c     Input:
c       + Nlambda: number of wavelength points
c       + lambda: array of wavelengths [micrometers]
c       + albedo: albedo signal
c       + lambda_min: lower wavelength of the integration interval [micrometers]
c       + lambda_max: higher wavelength of the integration interval [micrometers]
c     
c     Output:
c       + average_albedo: average albedo over the [lambda_min,lambda_max] interval [W/m^2/micrometer]
c
c     I/O
      integer Nlambda
      double precision lambda(1:Nlambda_mat_mx)
      double precision albedo(1:Nlambda_mat_mx)
      double precision lambda_min
      double precision lambda_max
      double precision average_albedo
c     temp
      logical lambda_min_found
      logical lambda_max_found
      integer i,imin,imax
      double precision lambda1,lambda2,F1,F2
      double precision S
c     label
      character*(Nchar_mx) label
      label='subroutine average_albedo_over_interval'
c
      lambda_min_found=.false.
      do i=1,Nlambda-1
         if ((lambda_min.ge.lambda(i)).and.
     &        (lambda_min.le.lambda(i+1))) then
            lambda_min_found=.true.
            imin=i
            goto 111
         endif
      enddo                     ! i
 111  continue
      if (.not.lambda_min_found) then
         call error(label)
         write(*,*) 'lambda_min=',lambda_min
         write(*,*) 'could not be found in the wavelength array'
         stop
      endif
c     
      lambda_max_found=.false.
      do i=imin,Nlambda-1
         if ((lambda_max.ge.lambda(i)).and.
     &        (lambda_max.le.lambda(i+1))) then
            lambda_max_found=.true.
            imax=i
            goto 112
         endif
      enddo                     ! i
 112  continue
      if (.not.lambda_max_found) then
         call error(label)
         write(*,*) 'lambda_max=',lambda_max
         write(*,*) 'could not be found in the wavelength array'
         stop
      endif

      S=0.0D+0
      do i=imin,imax
c     
         if (i.eq.imin) then
            if (lambda(imin).eq.lambda_min) then
               lambda1=lambda(imin)
               F1=albedo(imin)
            else
               lambda1=lambda_min
               call linear_interpolation(
     &              lambda(imin),lambda(imin+1),lambda1,
     &              albedo(imin),albedo(imin+1),F1)
            endif
         else
            lambda1=lambda(i)
            F1=albedo(i)
         endif                  ! i=imin
c     
         if (i.eq.imax) then
            if (lambda(imax).eq.lambda_max) then
               lambda2=lambda(imax)
               F2=albedo(imax)
            else
               lambda2=lambda_max
               call linear_interpolation(
     &              lambda(imax),lambda(imax+1),lambda2,
     &              albedo(imax),albedo(imax+1),F2)
            endif
         else
            lambda2=lambda(i+1)
            F2=albedo(i+1)
         endif                  ! i=imax
c     
         S=S+(F1+F2)*(lambda2-lambda1) ! [micrometers]
c     
      enddo                     ! i
      S=S/2.0D+0                ! [micrometers]
      average_albedo=S/(lambda_max-lambda_min) ! [1]

      return
      end
