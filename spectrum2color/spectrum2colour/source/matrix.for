c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine invert_matrix(n,a,inv_a)
      implicit none
      include 'max.inc'
c     
c     Purpose: to inverse a matrix
c     
c     Input:
c       + n: dimension of the system (must be greater than 1 !)
c       + a: (n*n) square matrix
c     
c     Output:
c       + inv_a: (n*n) square matrix, invere of matrix a
c     
c     I/O
      integer n
      double precision a(1:Ndim_mx,1:Ndim_mx)
      double precision inv_a(1:Ndim_mx,1:Ndim_mx)
c     temp
      integer i,j
      double precision det_a
      double precision c(1:Ndim_mx,1:Ndim_mx)
      double precision tc(1:Ndim_mx,1:Ndim_mx)
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine invert_matrix'

c     if your compiler supports a recursive compilation option (gfortran uses the "-frecursive" option)
c     then you can use subroutine "determinant"
      call determinant(n,a,det_a,determinant)
      if (det_a.eq.0.0D+0) then
         call error(label)
         write(*,*) 'could not invert matrix a:'
         do i=1,n
            write(*,*) (a(i,j),j=1,n)
         enddo                  ! i
         write(*,*) 'because determinant(a)=',det_a
         stop
      else
c     compute the matrix of cofactors
         do i=1,n
            do j=1,n
               call cofactor(n,a,i,j,c(i,j))
            enddo               ! j
         enddo                  ! i
         call transpose_matrix(n,c,tc)
         call matrix_scalar(n,tc,1.0D+0/det_a,inv_a)
      endif

      return
      end



      subroutine cofactor(n,a,i,j,cof)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the cofactor(i,j) of matrix a
c     
c     Input:
c       + n: dimension of the system (must be greater than 1 !)
c       + a: (n*n) square matrix
c       + (i,j): indexes of the required cofactor
c     
c     Output:
c       + cof: cofactor(i,j) of matrix a
c     
c     I/O
      integer n
      double precision a(1:Ndim_mx,1:Ndim_mx)
      integer i,j
      double precision cof
c     temp
      integer dim_m
      double precision m(1:Ndim_mx,1:Ndim_mx)
      double precision det_m
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine cofactor'

      call extract_matrix(n,a,i,j,dim_m,m)
      call determinant(dim_m,m,det_m,determinant)
      cof=(-1.0D+0)**(i+j)*det_m

      return
      end
      


      subroutine extract_matrix(n,a,i,j,dim_m,m)
      implicit none
      include 'max.inc'
c     
c     Purpose: to remove line 'i' and column 'j' from a given matrix
c     
c     Input:
c       + n: dimension of the system (must be greater than 1 !)
c       + a: (n*n) square matrix
c       + i: index of the line to remove
c       + j: index of the column to remove
c     
c     Output:
c       + dim_m: dimension of matrix m (dim_m x dim_m)
c       + m: resulting matrix
c     
c     I/O
      integer n
      double precision a(1:Ndim_mx,1:Ndim_mx)
      integer i,j
      integer dim_m
      double precision m(1:Ndim_mx,1:Ndim_mx)
c     temp
      integer mi,mj
      integer ai,aj
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine extract_matrix'

      dim_m=n-1
c      
      do mi=1,dim_m
c     
         if (mi.lt.i) then
            ai=mi
         else
            ai=mi+1
         endif
c     
         do mj=1,dim_m
c     
            if (mj.lt.j) then
               aj=mj
            else
               aj=mj+1
            endif
c     
            m(mi,mj)=a(ai,aj)
c     
         enddo                  ! mj
c     
      enddo                     ! mi
c      
      return
      end



      subroutine transpose_matrix(n,a,ta)
      implicit none
      include 'max.inc'
c     
c     Purpose: to translate a matrix
c     
c     Input:
c       + n: dimension of the system (must be greater than 1 !)
c       + a: (n*n) square matrix
c     
c     Output:
c       + ta: transposed matrix
c     
c     I/O
      integer n
      double precision a(1:Ndim_mx,1:Ndim_mx)
      double precision ta(1:Ndim_mx,1:Ndim_mx)
c     temp
      integer i,j
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine transpose_matrix'

      do i=1,n
         do j=1,n
            ta(i,j)=a(j,i)
         enddo                  ! j
      enddo                     ! i

      return
      end

      

      subroutine linear_system(debug,n,a,y,check,max_err,valid,x)
      implicit none
      include 'max.inc'
c     
c     Purpose: to solve a Cramer linear system A*X=Y
c
c     Inputs:
c       + debug: true if debug information should be printed
c       + n: dimension of the system (must be greater than 1 !)
c       + a: (n*n) square matrix
c       + y: (n) vector of second-terms
c       + check: true if ax-y=0 check is required
c       + max_err: error threshold to detect previous test failed
c
c     Outputs:
c       + valid: .true. if a valid solution was found; .false. otherwise
c       + x: (n) vector of solutions if a valid solution was found
c

c     inputs
      logical debug
      integer n
      double precision a(1:Ndim_mx,1:Ndim_mx)
      double precision y(1:Ndim_mx)
      logical check
      double precision max_err
c     outputs
      logical valid
      double precision x(1:Ndim_mx)
c     temp
      integer nswitch,Nswitch_max
      external determinant
      double precision a_tmp(1:Ndim_mx,1:Ndim_mx)
      double precision y_tmp(1:Ndim_mx)
      double precision p(1:Ndim_mx)
      double precision p1(1:Ndim_mx)
      double precision v(1:Ndim_mx)
      double precision det,eps
      double precision a11,b,c,sum
      double precision y_ref
      integer i,j,k,l,lif,li,iy
      double precision err
c     parameters
      parameter(Nswitch_max=100)
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine linear_system'

      if ((n.lt.1).or.(n.gt.Ndim_mx)) then
         call error(label)
         write(*,*) 'Bad input parameter:'
         write(*,*) 'n=',n
         if (n.gt.Ndim_mx) then
            write(*,*) 'while Ndim_mx=',Ndim_mx
         endif
         stop
      endif
      valid=.true.
c     Debug
      if (debug) then
         write(*,*) 'n=',n
      endif
c     Debug

c     ------------------------------------------------------
c     case of dimension 1 system (1 equation only)
      if (n.eq.1) then
         if (a(1,1).eq.0.0D+0) then
            call error(label)
            write(*,*) 'a*x=y with a=',a(1,1)
            write(*,*) 'no solution !'
            valid=.false.
            goto 666
         else
            x(1)=y(1)/a(1,1)
            goto 666
         endif
      endif

c     ------------------------------------------------------
c     in the case all solutions y(i) are identical
      iy=1
      y_ref=y(1)
      do i=2,n
         if (y(i).ne.y_ref) then
            iy=0
            goto 123
         endif
      enddo ! i
 123  continue
      if (iy.eq.1) then
         do i=1,n-1
            x(i)=0.0D+0
         enddo
         x(n)=y_ref
         goto 666
      endif

c     ------------------------------------------------------
c     all other cases...

c     copy "a" onto "a_tmp" and "y" onto "y_tmp"(that will be modified)
      call copy_matrix(n,a,a_tmp)
      call duplicate_vector(n,y,y_tmp)

c     computing determinant
c     --------------------------------
c     modif 2009/04/29
c     
c     if your compiler supports a recursive compilation option (gfortran uses the "-frecursive" option)
c     then you can use subroutine "determinant"
c     Otherwise, you MUST use subroutine "det3"
c
c     call det3(n,a_tmp,det)
c     Debug
c      write(*,*) 'ok0'
c     Debug
      call determinant(n,a_tmp,det,determinant)
c     Debug
      if (debug) then
         write(*,*) 'det=',det
      endif
c     Debug
c     modif 2009/04/29
c     --------------------------------
      if (dabs(det).lt.1.0D-10) then
         call error(label)
         write(*,*) 'determinant=',det
         write(*,*) 'matrix='
         do i=1,n
            write(*,*) (a(i,j),j=1,n),y(i)
         enddo ! i
         stop
      endif
      
      eps=1.0D-13
c     solving system using the Gauss method
      do i=2,n
c     Debug
         if (debug) then
            write(*,*) 'i=',i,' /',n
         endif
c     Debug
         nswitch=0
 222     continue
         a11=a_tmp(i-1,i-1)
         if (dabs(a11).lt.eps) then
            lif=0
            do l=i,n
               if (a_tmp(i-1,l).ne.0.0D+0) then
                  li=l
                  lif=1
c     Debug
                  if (debug) then
                     write(*,*) 'goto 111'
                  endif
c     Debug
                  goto 111
               endif
            enddo ! l
 111        continue
            if (lif.eq.0) then
               call error(label)
               write(*,*) 'going down'
               write(*,*) 'a(',i-1,',',i-1,')=',a11
               write(*,*) 'and no switching line found'
               valid=.false.
c     Debug
               if (debug) then
                  write(*,*) 'goto 666'
               endif
c     Debug
               goto 666
            else
c     switch lines 'i-1' and 'li'
c     Debug
               if (debug) then
                  write(*,*) 'switching lines:',i-1,li
                  write(*,*) (a_tmp(i-1,j),j=1,n),y_tmp(i-1)
                  write(*,*) (a_tmp(li,j),j=1,n),y_tmp(li)
               endif
c     Debug
               nswitch=nswitch+1
               if (nswitch.ge.Nswitch_max) then
                  call error(label)
                  write(*,*) 'number of successive switches exceeded'
                  write(*,*) 'check the value of eps'
                  stop
               endif
               call switch_lines(n,a_tmp,y_tmp,i-1,li)
c     Debug
               if (debug) then
                  write(*,*) 'goto 222'
               endif
c     Debug
               goto 222
            endif
         endif
         do j=i,n
            b=a_tmp(j,i-1)
            c=b/a11
            do k=i,n
               a_tmp(j,k)=a_tmp(j,k)-a_tmp(i-1,k)*c
            enddo ! k
            y_tmp(j)=y_tmp(j)-y_tmp(i-1)*c
            a_tmp(j,i-1)=0.0D+0
         enddo ! j
      enddo ! i

c     Debug
      if (debug) then
         write(*,*)
         write(*,*) 'a_tmp='
         do i=1,n
            write(*,*) (a_tmp(i,j),j=1,n),y_tmp(i)
         enddo
      endif
c     Debug

      do i=n,1,-1
         sum=0.0D+0
         if (i+1.le.n) then
            do j=n,i+1,-1
               sum=sum+a_tmp(i,j)*x(j)
            enddo ! j
         endif
         if (a_tmp(i,i).eq.0.0D+0) then
            call error(label)
            write(*,*) 'going up'
            write(*,*) 'a_tmp(',i,',',i,')=',a_tmp(i,i)
            valid=.false.
            goto 666
         else
            x(i)=(y_tmp(i)-sum)/a_tmp(i,i)
c     Debug
            if (debug) then
               write(*,*) 'x(',i,')=',x(i)
            endif
c     Debug
         endif
      enddo                     ! i

      if (check) then
         call matrix_vector(n,a,x,p)
         call scalar_vector(n,-1.0D+0,p,p1)
         call add_vectors(n,y,p1,v)
         call vector_length(n,v,err)
         if (dabs(err).gt.max_err) then
            if (debug) then
               call error(label)
               write(*,*) 'a.x-y='
               write(*,*) (v(i),i=1,n)
               write(*,*) 'norm=',err
            endif
            valid=.false.
         endif
      endif ! check

 666  continue
      return
      end



      subroutine switch_lines(n,a,y,i,j)
      implicit none
      include 'max.inc'
c
c     Purpose: to switch two lines on linear system parameters
c
c     Inputs:
c       + n: dimension of the system
c       + a: matrix "A" in "A.X=Y"
c       + y: vector "Y" in "A.X=Y"
c       + i: index of the first line
c       + li: index of the second line
c
c     Outputs:
c       + a: (updated) matrix "A" in "A.X=Y"
c       + y: (updated) vector "Y" in "A.X=Y"
c     

c     inputs
      integer n
      double precision a(1:Ndim_mx,1:Ndim_mx)
      double precision y(1:Ndim_mx)
      integer i,j
c     temp
      double precision line_tmp(1:Ndim_mx)
      double precision y_tmp
      integer k
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine switch_lines'

      if ((n.lt.1).or.(n.gt.Ndim_mx)) then
         call error(label)
         write(*,*) 'Bad input parameter:'
         write(*,*) 'n=',n
         if (n.gt.Ndim_mx) then
            write(*,*) 'while Ndim_mx=',Ndim_mx
         endif
         stop
      endif

      if ((i.lt.1).or.(i.gt.n)) then
         call error(label)
         write(*,*) 'Bad input parameter:'
         write(*,*) 'i=',i
         stop
      endif

      if ((j.lt.1).or.(j.gt.n)) then
         call error(label)
         write(*,*) 'Bad input parameter:'
         write(*,*) 'j=',j
         stop
      endif

      do k=1,n
         line_tmp(k)=a(j,k)
         a(j,k)=a(i,k)
      enddo
      y_tmp=y(j)
      y(j)=y(i)
      do k=1,n
         a(i,k)=line_tmp(k)
      enddo
      y(i)=y_tmp

      return
      end




      subroutine determinant(dim,mat,det,dummy_arg)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the determinant of a square matrix (of any size)
c     WARNING: this routine must not be used when you don't know whether or not
c     your compiler supports recursivity. gfortran will compile and use this
c     routine using the "-frecursive" option.
c
c     Inputs:
c       + dim: dimension of the matrix (>1)
c       + mat: dim*dim matrix
c     
c     Output:
c       + det: determinant
c       + dummy_arg: use "determinant" in the main call
c                  i.e. "call determinant(dim,mat,det,determinant)"
c                  This is a dummy argument, used for recursivity
c
c     input
      integer dim
      double precision mat(1:Ndim_mx,1:Ndim_mx)
c     output
      double precision det
c     temp
      external dummy_arg
      integer i,j
      integer row
      integer dim_tmp
      double precision sign
      double precision mat_tmp(1:Ndim_mx,1:Ndim_mx),det_tmp
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine determinant'

c     Debug
c      write(*,*) 'mat='
c      do i=1,dim
c         write(*,*) (mat(i,j),j=1,dim)
c      enddo
c      write(*,*)
c     Debug

      if (dim.eq.2) then
         call det2(dim,mat,det)
      else if (dim.eq.3) then
         call det3(dim,mat,det)
      else
         dim_tmp=dim-1
         det=0.0D+0
         do row=1,dim
            sign=(-1.0D+0)**(row+1)
            if (row.gt.1) then
               do i=1,row-1
                  do j=2,dim
                     mat_tmp(j-1,i)=mat(j,i)
                  enddo         ! j
               enddo            ! i
            endif
            if (row.lt.dim) then
               do i=row+1,dim
                  do j=2,dim
                     mat_tmp(j-1,i-1)=mat(j,i)
                  enddo         ! j
               enddo            ! i
            endif
c     Debug
c     write(*,*) 'row=',row,' sign=',sign
c     do i=1,dim_tmp
c     write(*,*) (mat_tmp(i,j),j=1,dim_tmp)
c     enddo
c     write(*,*)
c     Debug
            call dummy_arg(dim_tmp,mat_tmp,det_tmp,dummy_arg)
            det=det+sign*mat(1,row)*det_tmp
         enddo                  ! row
      endif ! dim=3

      return
      end



      
      subroutine det2(dim,mat,det)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the determinant of a square 2*2 matrix
c
c     Inputs:
c       + dim: dimension of the matrix (MUST BE 2)
c       + mat: 2*2 matrix
c     
c     Output:
c       + det: determinant
c

c     input
      integer dim
      double precision mat(1:Ndim_mx,1:Ndim_mx)
c     output
      double precision det
c     temp
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine det2'


      if (dim.ne.2) then
         call error(label)
         write(*,*) 'Bad input argument:'
         write(*,*) 'dim=',dim
         stop
      endif

      det=mat(1,1)*mat(2,2)-mat(1,2)*mat(2,1)

      return
      end


      
      subroutine det3(dim,mat,det)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the determinant of a square 3*3 matrix
c
c     Inputs:
c       + dim: dimension of the matrix (MUST BE 3)
c       + mat: 3*3 matrix
c     
c     Output:
c       + det: determinant
c

c     input
      integer dim
      double precision mat(1:Ndim_mx,1:Ndim_mx)
c     output
      double precision det
c     temp
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine det3'


      if (dim.ne.3) then
         call error(label)
         write(*,*) 'Bad input argument:'
         write(*,*) 'dim=',dim
         stop
      endif

      det=mat(1,1)*(mat(2,2)*mat(3,3)-mat(2,3)*mat(3,2))
     &     -mat(1,2)*(mat(2,1)*mat(3,3)-mat(2,3)*mat(3,1))
     &     +mat(1,3)*(mat(2,1)*mat(3,2)-mat(2,2)*mat(3,1))

      return
      end



      subroutine normalize_vector(dim,u,nu)
      implicit none
      include 'max.inc'
c
c     Purpose: to normalize a vector
c
c     Input:
c       + dim: space dimension
c       + u: vector to normalize
c
c     Output:
c       + nu: normalized vector u: nu=u/|u|
c
c     I/O
      integer dim
      double precision u(1:Ndim_mx)
      double precision nu(1:Ndim_mx)
c     temp
      double precision length
      integer i
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine normalize_vector'

      call vector_length(dim,u,length)
      if (length.le.0.0D+0) then
         call error(label)
         write(*,*) 'length=',length
         write(*,*) 'u=',(u(i),i=1,dim)
         stop
      else
         do i=1,dim
            nu(i)=u(i)/length
         enddo
      endif

      return
      end



      subroutine add_vectors(dim,u1,u2,v)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the sum of two vectors
c
c     Input:
c       + dim: space dimension
c       + u1: first vector
c       + u2: second vector
c     
c     Output:
c       + v=u1+u2
c
c     I/O
      integer dim
      double precision u1(1:Ndim_mx)
      double precision u2(1:Ndim_mx)
      double precision v(1:Ndim_mx)
c     temp
      integer i
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine add_vectors'

      do i=1,dim
         v(i)=u1(i)+u2(i)
      enddo

      return
      end



      subroutine substract_vectors(dim,u1,u2,v)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the difference of two vectors: v=u1-u2
c
c     Input:
c       + dim: space dimension
c       + u1: first vector
c       + u2: second vector
c     
c     Output:
c       + v=u1-u2
c
c     I/O
      integer dim
      double precision u1(1:Ndim_mx)
      double precision u2(1:Ndim_mx)
      double precision v(1:Ndim_mx)
c     temp
      integer i
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine substract_vectors'

      do i=1,dim
         v(i)=u1(i)-u2(i)
      enddo

      return
      end



      subroutine vector_length(dim,u,length)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the lenght of a given vector
c
c     Imput:
c       + dim: space dimension
c       + u: vector
c
c     Output:
c       + length: lenght of vector u
c
c     I/O
      integer dim
      double precision u(1:Ndim_mx)
      double precision length
c     temp
      integer i
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine vector_length'
      
      do i=1,dim
         x1(i)=0.0D+0
      enddo ! i
      call add_vectors(dim,x1,u,x2)
      call distance_2points(dim,x1,x2,length)

      return
      end
      


      subroutine scalar_vector(dim,scalar,u,v)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the product of a scalar and a vector
c
c     Input:
c       + dim: space dimension
c       + scalar: scalar value
c       + u: vector
c     
c     Output:
c       + v=scalar*u
c
c     I/O
      integer dim
      double precision scalar
      double precision u(1:Ndim_mx)
      double precision v(1:Ndim_mx)
c     temp
      integer i
      double precision nu(1:Ndim_mx)
      double precision nv(1:Ndim_mx)
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine scalar_vector'

      do i=1,dim
         v(i)=u(i)*scalar
      enddo

      return
      end



      subroutine matrix_scalar(n,M1,scalar,M2)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the product of a scalar and a matrix
c
c     I/O
      integer n
      double precision scalar
      double precision M1(1:Ndim_mx,1:Ndim_mx)
      double precision M2(1:Ndim_mx,1:Ndim_mx)
c     temp
      integer i,j
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine scalar_matrix'

      do i=1,n
         do j=1,n
            M2(i,j)=M1(i,j)*scalar
         enddo                  ! j
      enddo                     ! i

      return
      end



      subroutine copy_matrix(n,mat1,mat2)
      implicit none
      include 'max.inc'
c     I/O
      integer n
      double precision mat1(1:Ndim_mx,1:Ndim_mx)
      double precision mat2(1:Ndim_mx,1:Ndim_mx)
c     temp
      integer i,j
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine copy_matrix'

      do i=1,n
         do j=1,n
            mat2(i,j)=mat1(i,j)
         enddo ! j
      enddo ! i

      return
      end






      subroutine identity_matrix(dim,Id)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute a (dim x dim) square identity matrix
c
c     Input:
c       + dim: dimension of the matrix
c
c     Output:
c       + Id: (dim x dim) identity matrix
c
c     I/O
      integer dim
      double precision Id(1:Ndim_mx,1:Ndim_mx)
c     temp
      integer i,j
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine identity_matrix'

      do i=1,dim
         do j=1,dim
            if (i.ne.j) then
               Id(i,j)=0.0D+0
            else
               Id(i,j)=1.0D+0
            endif
         enddo ! j
      enddo ! i

      return
      end




      subroutine add_matrix(dim,M1,M2,M)
      implicit none
      include 'max.inc'
c
c     Purpose: to add two square matrices
c
c     Inputs:
c       + dim: size of the matrices
c       + M1: matrix number 1
c       + M2: matrix number 2
c     
c     Outputs:
c       + M=M1+M2 (also a square matrix of size dim)
c
c     I/O
      integer dim
      double precision M1(1:Ndim_mx,1:Ndim_mx)
      double precision M2(1:Ndim_mx,1:Ndim_mx)
      double precision M(1:Ndim_mx,1:Ndim_mx)
c     temp
      integer i,j
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine add_matrix'

      do i=1,dim
         do j=1,dim
            M(i,j)=M1(i,j)+M2(i,j)
         enddo ! j
      enddo ! i

      return
      end



      subroutine matrix_vector(dim,M1,vector,M)
      implicit none
      include 'max.inc'
c
c     Purpose: to multiply a square matrix by a vector
c
c     Inputs:
c       + dim: size of matrix M1 and vector
c       + M1: square matrix
c       + vector: vector of size dim
c     
c     Outputs:
c       + M=M1*vector (also a vector of size dim)
c
c     I/O
      integer dim
      double precision M1(1:Ndim_mx,1:Ndim_mx)
      double precision vector(1:Ndim_mx)
      double precision M(1:Ndim_mx)
c     temp
      integer i,k
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine matrix_vector'

      do i=1,dim
         M(i)=0.0D+0
         do k=1,dim
            M(i)=M(i)+M1(i,k)*vector(k)
         enddo ! k
      enddo ! i

      return
      end



      subroutine matrix_matrix(dim,M1,M2,M)
      implicit none
      include 'max.inc'
c
c     Purpose: to multiply a square matrix by a square matrix
c
c     Inputs:
c       + dim: size of matrices M1 and M2
c       + M1: square matrix number 1
c       + M2: square matrix number 2
c     
c     Outputs:
c       + M=M1*M2
c
c     I/O
      integer dim
      double precision M1(1:Ndim_mx,1:Ndim_mx)
      double precision M2(1:Ndim_mx,1:Ndim_mx)
      double precision M(1:Ndim_mx,1:Ndim_mx)
c     temp
      integer i,j,k
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine matrix_matrix'

      do i=1,dim
         do j=1,dim
            M(i,j)=0.0D+0
            do k=1,dim
               M(i,j)=M(i,j)+M1(i,k)*M2(k,j)
            enddo               ! k
         enddo                  ! j
      enddo                     ! i

      return
      end



      subroutine scalar_product(dim,u1,u2,product)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the scalar product between two vectors
c
c     Input:
c       + dim: space dimension
c       + u1: first vector
c       + u2: second vector
c
c     Output:
c       + product: |u1.u2|
c
c     I/O
      integer dim
      double precision u1(1:Ndim_mx)
      double precision u2(1:Ndim_mx)
      double precision product
c     temp
      integer i
      double precision nu1(1:Ndim_mx)
      double precision nu2(1:Ndim_mx)
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine scalar_product'

      call normalize_vector(dim,u1,nu1)
      call normalize_vector(dim,u2,nu2)

      product=0.0D+0
      do i=1,dim
         product=product+nu1(i)*nu2(i)
      enddo ! i

      return
      end



      subroutine identical_vectors(dim,u,v,identical)
      implicit none
      include 'max.inc'
c
c     Purpose: to check whether or not two given vectors are identical
c
c     Input:
c       + dim: space dimension
c       + u: first vector
c       + v: second vector
c     
c     Output:
c       + identical: true if u and v are the same
c
c     I/O
      integer dim
      double precision u(1:Ndim_mx)
      double precision v(1:Ndim_mx)
      logical identical
c     temp
      double precision diff(1:Ndim_mx)
      logical is_null
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine identical_vectors'

      call substract_vectors(dim,u,v,diff)
      call is_null_vector(dim,diff,is_null)
      if (is_null) then
         identical=.true.
      else
         identical=.false.
      endif

      return
      end




      subroutine is_null_vector(dim,u,is_null)
      implicit none
      include 'max.inc'
c
c     Purpose: to check whether or not a given vector is null
c
c     Input:
c       + dim: space dimension
c       + u: vector
c     
c     Output:
c       + is_null: true if vector u is null
c
c     I/O
      integer dim
      double precision u(1:Ndim_mx)
      logical is_null
c     temp
      double precision length
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine is_null_vector'

      call vector_length(dim,u,length)
      if (dabs(length).lt.1.0D-8) then
         is_null=.true.
      else
         is_null=.false.
      endif

      return
      end


      
      subroutine duplicate_vector(dim,x1,x2)
      implicit none
      include 'max.inc'
c
c     Purpose: to copy a given position
c
c     Input:
c       + dim: space dimension
c       + x1: position to copy
c
c     Output:
c       + x2: copied position
c
c     I/O
      integer dim
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
c     temp
      integer i
c     label
      integer stlren
      character*(Nchar_mx) label
      label='subroutine duplicate_vector'

      do i=1,dim
         x2(i)=x1(i)
      enddo ! i

      return
      end


      
      subroutine distance_2points(dim,x1,x2,d)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the distance between two positions defined
c     by their cartesian coordinates
c
c     Input:
c       + dim: space dimension
c       + x1: first position
c       + x2: second position
c
c     Output:
c       + d: distance between the two positions
c
c     inputs
      integer dim
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
c     outputs
      double precision d
c     temp
      integer i
      double precision d2
c     label
      integer stlren
      character*(Nchar_mx) label
      label='subroutine distance_2points'

      d2=0.0D+0
      do i=1,dim
         d2=d2+(x2(i)-x1(i))**2.0D+0
      enddo
      d=dsqrt(d2)

      return
      end
