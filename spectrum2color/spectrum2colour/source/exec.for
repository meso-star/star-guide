c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine exec(command)
      implicit none
      include 'max.inc'
c
c     Purpose: to execute the provided system command
c     
c     Input:
c       + command: character string (maximum 100 characters) that
c         contains the system command to execute
c
      character*(Nchar_mx) command
c     label
      character*(Nchar_mx) label
      label='subroutine exec'

      call system(trim(command))

      return
      end
