c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine Yxy2XYZ(color_Yxy,color_XYZ)
      implicit none
      include 'max.inc'
c     
c     Purpose: to convert a color from Yxy to XYZ colorspace
c     
c     I/O
      double precision color_Yxy(1:Ndim_mx)
      double precision color_XYZ(1:Ndim_mx)
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine Yxy2XYZ'

      color_XYZ(1)=color_Yxy(1)
     &     *color_Yxy(2)/color_Yxy(3)
      color_XYZ(2)=color_Yxy(1)
      color_XYZ(3)=color_Yxy(1)
     &     *(1.0D+0-color_Yxy(2)-color_Yxy(3))
     &     /color_Yxy(3)

      return
      end
