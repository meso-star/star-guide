c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine read_material_spectral_data(datafile,
     &     Nmaterial,material_name,Nlambda,lambda,sdata)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to read a .csv solid material spectral data database
c     
c     Input:
c       + datafile: name of the .csv data file to read
c     
c     Output:
c       + Nmaterial: number of materials
c       + material_name: array of material identification
c       + Nlambda: number of wavelength points
c       + lambda: array of wavelengths [micrometers]
c       + sdata: array of spectral data for each material, each wavelength
c     
c     I/O
      character*(Nchar_mx) datafile
      integer Nmaterial
      character*(Nchar_mx) material_name(1:Nmaterial_mx)
      integer Nlambda
      double precision lambda(1:Nlambda_mat_mx)
      double precision sdata(1:Nlambda_mat_mx,1:Nmaterial_mx)
c     temp
      integer imat,ilambda,ios,i0,i,nl
      character*(Nchar_mx) line,st
      logical keep_looking,run
c     label
      character*(Nchar_mx) label
      label='subroutine read_material_spectral_data'

      open(11,file=trim(datafile),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(datafile)
         stop
      else
         write(*,*) 'Reading: ',trim(datafile)
         call get_nlines(datafile,nl)
         Nlambda=nl-1
         if (Nlambda.gt.Nlambda_mat_mx) then
            call error(label)
            write(*,*) 'Nlambda=',Nlambda
            write(*,*) '> Nlambda_mat_mx=',Nlambda_mat_mx
            stop
         endif                  ! Nlambda > Nlambda_mat_mx
c     -----------------------------------------------------------------
c     read the first line
c     -----------------------------------------------------------------
         read(11,10) line
         i0=0
         Nmaterial=0
         keep_looking=.true.
         do while (keep_looking)
            i=1
            run=.true.
            do while (run)
               if (i0+i.gt.len_trim(line)) then
                  run=.false.
                  keep_looking=.false.
                  goto 111
               endif
               if (line(i0+i:i0+i).eq.',') then ! character index i0+i is a comma ','
                  st=line(i0+1:i0+i-1)
                  if (trim(st).ne.'"wavelength"') then
                     Nmaterial=Nmaterial+1
                     if (Nmaterial.gt.Nmaterial_mx) then
                        call error(label)
                        write(*,*) 'Nmaterial_mx was reached'
                        stop
                     else
                        if ((st(1:1).eq.'"').and.
     &                       (st(len_trim(st):len_trim(st)).eq.'"'))
     &                       then
                           material_name(Nmaterial)=st(2:len_trim(st)-1)
                        else
                           material_name(Nmaterial)=trim(st)
                        endif
                     endif      ! Nmaterial.gt.Nmaterial_mx
                  endif         ! trim(st).ne.'"wavelength"'
                  run=.false.
               endif            ! character is a comma
               if (run) then
                  i=i+1
               endif
            enddo               !  while (run)
 111        continue
            if (keep_looking) then
               i0=i0+i
            endif               ! keep_looking
         enddo                  !  while (keep_looking)
c     -----------------------------------------------------------------
c     read all other lines
c     -----------------------------------------------------------------
         do ilambda=1,Nlambda
            read(11,*) lambda(ilambda), ! [nm]
     &           (sdata(ilambda,imat),imat=1,Nmaterial)
c     conversions
            lambda(ilambda)=lambda(ilambda)*1.0D-3 ! [nm] -> [micrometer]
         enddo                  ! ilambda
c     -----------------------------------------------------------------
      endif
      close(11)

      return
      end
