c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine num2str2(num,str2)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     I/O
      integer num
      character*2 str2
c     temp
      character*1 zeroch,kch1
c     label
      character*(Nchar_mx) label
      label='subroutine num2str2'
      
      write(zeroch,11) 0
      if ((num.ge.0).and.(num.lt.10)) then
         write(kch1,11) num
         str2=trim(zeroch)//trim(kch1)
      else if ((num.ge.10).and.(num.lt.100)) then
         write(str2,12) num
      else
         call error(label)
         write(*,*) 'num=',num,' >= 100'
         stop
      endif
      
      return
      end



      subroutine num2str3(num,str3)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to convert a integer into a 3-characters string
c     
c     Input:
c       + num: integer to convert
c     
c     Output:
c       + str3: character string of length 3
c     
c     I/O
      integer num
      character*3 str3
c     temp
      character*1 zeroch,kch1
      character*2 kch2
c     label
      character*(Nchar_mx) label
      label='subroutine num2str3'
      
      write(zeroch,11) 0
      if ((num.ge.0).and.(num.lt.10)) then
         write(kch1,11) num
         str3=trim(zeroch)//trim(zeroch)//trim(kch1)
      else if ((num.ge.10).and.(num.lt.100)) then
         write(kch2,12) num
         str3=trim(zeroch)//trim(kch2)
      else if ((num.ge.100).and.(num.lt.1000)) then
         write(str3,13) num
      else
         call error(label)
         write(*,*) 'num=',num,' >= 1000'
         stop
      endif
      
      return
      end



      subroutine num2str4(num,str4)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     I/O
      integer num
      character*4 str4
c     temp
      character*1 zeroch,kch1
      character*2 kch2
      character*3 kch3
c     label
      character*(Nchar_mx) label
      label='subroutine num2str4'
      
      write(zeroch,11) 0
      if ((num.ge.0).and.(num.lt.10)) then
         write(kch1,11) num
         str4=trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(kch1)
      else if ((num.ge.10).and.(num.lt.100)) then
         write(kch2,12) num
         str4=trim(zeroch)
     &        //trim(zeroch)
     &        //trim(kch2)
      else if ((num.ge.100).and.(num.lt.1000)) then
         write(kch3,13) num
         str4=trim(zeroch)
     &        //trim(kch3)
      else if ((num.ge.1000).and.(num.lt.10000)) then
         write(str4,14) num
      else
         call error(label)
         write(*,*) 'num=',num,' >= 10000'
         stop
      endif
      
      return
      end



      subroutine num2str10(num,str10)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     I/O
      integer*8 num
      character*10 str10
c     temp
      character*1 zeroch,kch1
      character*2 kch2
      character*3 kch3
      character*4 kch4
      character*5 kch5
      character*6 kch6
      character*7 kch7
      character*8 kch8
      character*9 kch9
c     label
      character*(Nchar_mx) label
      label='subroutine num2str10'
      
      write(zeroch,11) 0
      if ((num.ge.0).and.(num.lt.10)) then
         write(kch1,11) num
         str10=trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(kch1)
      else if ((num.ge.10).and.(num.lt.100)) then
         write(kch2,12) num
         str10=trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(kch2)
      else if ((num.ge.100).and.(num.lt.1000)) then
         write(kch3,13) num
         str10=trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(kch3)
      else if ((num.ge.1000).and.(num.lt.10000)) then
         write(kch4,14) num
         str10=trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(kch4)
      else if ((num.ge.10000).and.(num.lt.100000)) then
         write(kch5,15) num
         str10=trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(kch5)
      else if ((num.ge.100000).and.(num.lt.1000000)) then
         write(kch6,16) num
         str10=trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(kch6)
      else if ((num.ge.1000000).and.(num.lt.10000000)) then
         write(kch7,17) num
         str10=trim(zeroch)
     &        //trim(zeroch)
     &        //trim(zeroch)
     &        //trim(kch7)
      else if ((num.ge.10000000).and.(num.lt.100000000)) then
         write(kch8,18) num
         str10=trim(zeroch)
     &        //trim(zeroch)
     &        //trim(kch8)
      else if ((num.ge.100000000).and.(num.lt.1000000000)) then
         write(kch9,19) num
         str10=trim(zeroch)
     &        //trim(kch9)
      else
         write(str10,110) num
      endif
      
      return
      end
