c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine read_HR_Sun_spectrum(Nlambda,lambda,F,int_F)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read the high-resolution Sun emission spectrum
c     from "kurudz_full.dat"
c     
c     Input:
c     
c     Output:
c       + Nlambda: number of wavelength points
c       + lambda: array of wavelength values [micrometers]
c       + F: array of corresponding solar flux density [W/m^2/micrometer]
c       + int_F: spectral integral of F [W/m^2]
c     
c     I/O
      integer*8 Nlambda
      double precision lambda(1:Nlambda_sun_mx)
      double precision F(1:Nlambda_sun_mx)
      double precision int_F
c     temp
      character*(Nchar_mx) filename
      integer ios,nl
      integer*8 i
      double precision S
c     label
      character*(Nchar_mx) label
      label='subroutine read_HR_Sun_spectrum'
c
      filename='./data/kurudz_full.dat'
c      
      open(11,file=trim(filename),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(filename)
         stop
      else
         write(*,*) 'Reading: ',trim(filename)
         call get_nlines(filename,nl)
         Nlambda=nl-11
         if (Nlambda.gt.Nlambda_sun_mx) then
            call error(label)
            write(*,*) 'Number of points is: ',Nlambda
            write(*,*) '> Nlambda_sun_mx=',Nlambda_sun_mx
            stop
         endif
         do i=1,11
            read(11,*)
         enddo                  ! i
         do i=1,Nlambda
            read(11,*) lambda(i),F(i) ! [nm] / [mW/m^2/nm]
c     conversions
            lambda(i)=lambda(i)*1.0D-3 ! [nm] -> [micrometer]
         enddo                  ! i
c     perform the spectral integral
         S=0.0D+0
         do i=1,Nlambda-1
            S=S+(F(i)+F(i+1))*(lambda(i+1)-lambda(i))
         enddo                  ! i
         int_F=S/2.0D+0         ! [W/m^2]
      endif
      close(11)

      return
      end
      


      subroutine average_flux_over_interval(Nlambda,lambda,F,
     &     lambda_min,lambda_max,average_F)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute to average of the high-resolution Sun emission spectrum
c     over a given spectral interval
c     
c     Input:
c       + Nlambda: number of wavelength points
c       + lambda: array of wavelength values [micrometers]
c       + F: array of corresponding solar flux density [W/m^2/micrometer]
c       + lambda_min: lower wavelength of the integration interval [micrometers]
c       + lambda_max: higher wavelength of the integration interval [micrometers]
c     
c     Output:
c       + average_F: average solar flux over the [lambda_min,lambda_max] interval [W/m^2/micrometer]
c
c     I/O
      integer*8 Nlambda
      double precision lambda(1:Nlambda_sun_mx)
      double precision F(1:Nlambda_sun_mx)
      double precision lambda_min
      double precision lambda_max
      double precision average_F
c     temp
      logical lambda_min_found
      logical lambda_max_found
      integer*8 i,imin,imax
      double precision lambda1,lambda2,F1,F2
      double precision S
c     label
      character*(Nchar_mx) label
      label='subroutine average_flux_over_interval'
c
      lambda_min_found=.false.
      do i=1,Nlambda-1
         if ((lambda_min.ge.lambda(i)).and.
     &        (lambda_min.le.lambda(i+1))) then
            lambda_min_found=.true.
            imin=i
            goto 111
         endif
      enddo                     ! i
 111  continue
      if (.not.lambda_min_found) then
         call error(label)
         write(*,*) 'lambda_min=',lambda_min
         write(*,*) 'could not be found in the wavelength array'
         stop
      endif
c     
      lambda_max_found=.false.
      do i=imin,Nlambda-1
         if ((lambda_max.ge.lambda(i)).and.
     &        (lambda_max.le.lambda(i+1))) then
            lambda_max_found=.true.
            imax=i
            goto 112
         endif
      enddo                     ! i
 112  continue
      if (.not.lambda_max_found) then
         call error(label)
         write(*,*) 'lambda_max=',lambda_max
         write(*,*) 'could not be found in the wavelength array'
         stop
      endif
      
      S=0.0D+0
      do i=imin,imax
c     
         if (i.eq.imin) then
            if (lambda(imin).eq.lambda_min) then
               lambda1=lambda(imin)
               F1=F(imin)
            else
               lambda1=lambda_min
               call linear_interpolation(
     &              lambda(imin),lambda(imin+1),lambda1,
     &              F(imin),F(imin+1),F1)
            endif
         else
            lambda1=lambda(i)
            F1=F(i)
         endif                  ! i=imin
c     
         if (i.eq.imax) then
            if (lambda(imax).eq.lambda_max) then
               lambda2=lambda(imax)
               F2=F(imax)
            else
               lambda2=lambda_max
               call linear_interpolation(
     &              lambda(imax),lambda(imax+1),lambda2,
     &              F(imax),F(imax+1),F2)
            endif
         else
            lambda2=lambda(i+1)
            F2=F(i+1)
         endif                  ! i=imax
c     
         S=S+(F1+F2)*(lambda2-lambda1) ! [W/m^2]
c     
      enddo                     ! i
      S=S/2.0D+0                ! [W/m^2]
      average_F=S/(lambda_max-lambda_min) ! [W/m^2/micrometer]

      return
      end
