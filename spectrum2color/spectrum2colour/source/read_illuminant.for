c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine read_illuminant(filename,Nlambda,lambda,spectrum)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read a standard illuminant data file
c     
c     Input:
c       + filemane: name of the file to read
c     
c     Output:
c       + Nlambda: number of lambda points
c       + lambda: values of the wavelength [micrometers]
c       + spectrum: values of the illuminant
c     
c     I/O
      character*(Nchar_mx) filename
      integer Nlambda
      double precision lambda(1:Nlambda_ill_mx)
      double precision spectrum(1:Nlambda_ill_mx)
c     temp
      integer i,ios,nl
c     label
      character*(Nchar_mx) label
      label='subroutine read_illuminant'

      open(11,file=trim(filename),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(filename)
         stop
      else
         call get_nlines(filename,nl)
         Nlambda=nl
         if (Nlambda.gt.Nlambda_ill_mx) then
            call error(label)
            write(*,*) 'Nlambda=',Nlambda
            write(*,*) '> Nlambda_ill_mx=',Nlambda_ill_mx
            stop
         endif
         do i=1,Nlambda
            read(11,*) lambda(i),spectrum(i)
            lambda(i)=lambda(i)*1.0D-3 ! [nm] -> [micrometers}
         enddo                  ! i
      endif
      close(11)
      
      return
      end


      
      subroutine identify_illuminant(Nlambda,lambda,dt,wvl,
     &     illuminant)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the value of the illuminant for a given wavelength
c     
c     Input:
c       + Nlambda: number of lambda points
c       + lambda: values of the wavelength [micrometers]
c       + dt: values of the illuminant
c       + wvl: wavelength the albedo has to be identified for
c     
c     Output:
c       + albedo: value of the albedo for wvl
c     
c     I/O
      integer Nlambda
      double precision lambda(1:Nlambda_ill_mx)
      double precision dt(1:Nlambda_ill_mx)
      double precision wvl
      double precision illuminant
c     temp
      logical int_found
      integer int,i
      double precision dt1,dt2
c     label
      character*(Nchar_mx) label
      label='subroutine identify_illuminant'

      if (wvl.lt.lambda(1)) then
         illuminant=0.0D+0
      else if (wvl.gt.lambda(Nlambda)) then
         illuminant=0.0D+0
      else
         int_found=.false.
         do i=1,Nlambda-1
            if ((wvl.ge.lambda(i)).and.(wvl.le.lambda(i+1))) then
               int_found=.true.
               int=i
               goto 111
            endif
         enddo                  ! i
 111     continue
         if (int_found) then
            dt1=dt(int)
            dt2=dt(int+1)
            illuminant=dt1+(dt2-dt1)
     &           *(wvl-lambda(int))
     &           /(lambda(int+1)-lambda(int))
         else
            call error(label)
            write(*,*) 'wvl=',wvl
            write(*,*) 'could not be identified in lambda range:'
            write(*,*) 'lambda(1)=',lambda(1)
            write(*,*) 'lambda(',Nlambda,')=',lambda(Nlambda)
            stop
         endif
      endif                     ! wvl

      return
      end
