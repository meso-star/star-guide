c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      double precision function Planck(T,nu1,nu2)
      implicit none
c
c     Purpose: to compute the mean value of the Planck intensity (W/m²/sr/cm¯¹)
c     over a given spectral interval
c
c     Inputs:
c       + T: temperature (K)
c       + nu1: wavenumber at the beginning of the spectral interval (cm¯¹)
c       + nu2: wavenumber at the end of the spectral interval (cm¯¹)
c
c     Output: Planck (W/m²/sr/cm¯¹)
c
c     I/O
      double precision T
      double precision nu1,nu2
c     temp
      double precision lambda_c,nu_c,B
      double precision lambda1,lambda2,dlambda,dnu
      double precision Blambda

      dnu=nu2-nu1
      call nu2lambda(nu1,lambda1)
      call nu2lambda(nu2,lambda2)
      dlambda=lambda1-lambda2
      nu_c=(nu1+nu2)/2.0D+0 ! cm¯¹
      call nu2lambda(nu_c,lambda_c)

      Planck=Blambda(T,lambda_c) ! W/m²/sr/µm
      Planck=Planck*dlambda/dnu ! W/m²/sr/cm¯¹

      return
      end



      double precision function Blambda(T,lambda)
      implicit none
c     
c     Planck intensity (W/m²/sr/µm) as function of wavelenght (µm) and temperature (K)
c     
c     Inputs:
c       + T: temperature in K
c       + lambda: wavelenght (µm)
c
c     Output: B (W/m²/sr/µm)
c
c     I/O
      double precision T,lambda
c     temp
      double precision B
      double precision c1,c2,in
      double precision hPl,kBz,c0,pi
      parameter(kBz=1.3806D-23) ! J/K (Boltzmann constant)
      parameter(hPl=6.62606896D-34) ! J.s (Planck constant)
      parameter(c0=2.99792458D+8) ! m/s

      pi=4.0D+0*datan(1.0D+0)

      c1=2*pi*hPl*c0**2 ! W.m²
      c2=hPl*c0/kBz*1.D+6 ! µm.K
      in=lambda*T
      if (T.eq.0) then
         Blambda=0.0D+0
      else
         Blambda=c1/(in**5*(dexp(c2/in)-1))*T**5*1.D+24/pi ! W/m²/sr/µm
      endif

      return
      end



      double precision function Bnu(T,nu)
      implicit none
c     
c     Planck intensity (W/m²/sr/cm¯¹) as function of wavenumber (cm¯¹) and temperature (K)
c     
c     Inputs:
c       + T: temperature in K
c       + nu: wavenumber (cm¯¹)
c
c     Output: B (W/m²/sr/cm¯¹)
c
c     I/O
      double precision T,nu
c     temp
      double precision B
      double precision Blambda,Bl,lambda,dlambda_dnu

      call nu2lambda(nu,lambda)
      Bl=Blambda(T,lambda) ! W/m²/sr/µm
      Bnu=Bl*dabs(dlambda_dnu(lambda)) ! W/m²/sr/cm¯¹
      
      return
      end



      double precision function F_wavnb(nu,T)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the fraction of energy between 0 and wavenumber "nu"
c
c     Inputs:
c       + nu: wavenumber [cm^{-1}]
c       + T: temperature [K]
c
c     Output:
c       + F: fraction of energy between 0 and nu
c
c     I/O
      double precision nu,T
c     temp
      double precision sigmaT4
      double precision nu_min
      parameter(nu_min=1.0D-5)
      double precision planck_int_wavnb

      F_wavnb=planck_int_wavnb(nu_min,nu,T)/sigmaT4(T)

      if (F_wavnb.gt.1.0D+0) then
         F_wavnb=1.0D+0
      endif

      return
      end



      double precision function planck_int_wavnb(nu_lo,nu_hi,T)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the Planck function, integrated between two
c     values of the wavenumber
c
c     Inputs:
c       + nu_lo: lower value of wavenumber [cm^{-1}]
c       + nu_hi: higer value of wavenumber [cm^{-1}]
c       + T: temperature [K]
c
c     Output:
c       + planck_int_wavnb: Planck function at temperature T, integrated between
c            nu_lo and nu_hi [W//m²/sr]
c
c     inputs
      double precision nu_lo,nu_hi,T
c     temp
      real PLKAVG
      
      planck_int_wavnb=PLKAVG(real(nu_lo),real(nu_hi),real(T)) ! W/m²/sr

      return
      end



      double precision function planck_int_wavlng(lambda_lo,lambda_hi,
     &     T)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the Planck function, integrated between two
c     values of the wavelength
c
c     Inputs:
c       + lambda_lo: lower value of wavelength [µm]
c       + lambda_hi: higer value of wavelength [µm]
c       + T: temperature [K]
c
c     Output:
c       + planck_int_wavlng: Planck function at temperature T, integrated between
c            lambda_lo and lambda_hi [W//m²/sr]
c
c     inputs
      double precision lambda_lo,lambda_hi,T
c     temp
      double precision planck_int_wavnb
      double precision nu_lo,nu_hi
      
      call l2nu(lambda_lo,nu_hi)
      call l2nu(lambda_hi,nu_lo)
      planck_int_wavlng=planck_int_wavnb(nu_lo,nu_hi,T) ! W/m²/sr

      return
      end



      subroutine nu2lambda(nu,lambda)
      implicit none
      include 'max.inc'
c
c     Purpose: to convert a wavenumber (in cm¯¹) to a wavelenght (in µm)
c
c     Input:
c       + nu: wavenumber (inverse centimeters)
c     
c     Output:
c       + lambda: wavelength (micrometers)
c
c     I/O
      double precision nu,lambda
      
      lambda=1.0D+4/nu          ! µm

      return
      end


      subroutine l2nu(lambda,nu)
      implicit none
      include 'max.inc'
c
c     Purpose: to converta wavelength (in µm)  to a wavenumber (in cm¯¹) 
c     
c     Input:
c       + lambda: wavelength (micrometers)
c     
c     Output:
c       + nu: wavenumber (inverse centimeters)
c
c     I/O
      double precision nu,lambda
      
      nu=1.0D+4/lambda          ! cm¯¹

      return
      end



      double precision function dlambda_dnu(lambda)
      implicit none
c
c     Purpose: to compute d(lambda)/d(nu) at a given value of lambda
c
c     Input:
c       + lambda: wavelength (micrometers)
c
c     Output:
c       + dlambda_dnu: d(lambda)/d(nu), micrometers per inverse centimeters
c
c     I/O
      double precision lambda,nu

      call l2nu(lambda,nu)
      dlambda_dnu=-1.0D+4/(nu**2.0D+0)

      return
      end



      double precision function dnu_dlambda(nu)
      implicit none
c
c     Purpose: to compute d(nu)/d(lambda) at a given value of nu
c
c     Input:
c       + nu: wavenumber (inverse centimeters)
c
c     Output:
c       + dnu_dlambda: d(nu)/d(lambda), inverse centimeters per micrometer
c
c     I/O
      double precision lambda,nu

      dnu_dlambda=-(nu**2.0D+0)/1.0D+4

      return
      end



      double precision function sigmaT4(T)
      implicit none
      include 'max.inc'
      include 'param.inc'
      include 'constants.inc'
c
c     Purpose: to compute the total energy emitted by a blackbody at temperature T
c
c     Input:
c       + T: temperature [K]
c
c     Output:
c       + sigmaT4 [W/m²/sr]
c
c     I/O
      double precision T
      
      sigmaT4=sigma*T**4.0D+0/pi ! W/m²/sr

      return
      end
