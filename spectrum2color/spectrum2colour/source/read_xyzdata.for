c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine read_xyzdata(infile,dim,Np,xyz)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read the "data/xyz.dat" input data file
c     
c     Input:
c       + infile: name of the file to read
c       + dim: dimension of colorspaces
c     
c     Output:
c       + Np: number of values for x, y and z functions
c       + xyz: data
c        -xyz(i,1): value of the wavelength [nm]
c        -xyz(i,2): value of the x function
c        -xyz(i,3): value of the y function
c        -xyz(i,4): value of the z function
c     
c     I/O
      character*(Nchar_mx) infile
      integer dim
      integer Np
      double precision xyz(1:Np_mx,1:Ndim_mx+1)
c     temp
      logical file_ex
      integer nl,i,j
      double precision dt(1:Np_mx,1:Ndim_mx+1)
      double precision sum(1:3)
c     label
      character*(Nchar_mx) label
      label='subroutine read_xyzdata'

      inquire(file=trim(infile), exist=file_ex)
      if (file_ex) then
         write(*,*) 'Reading file: ',trim(infile)
      else
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(infile)
         stop
      endif

      call get_nlines(infile,nl)
      Np=nl-1
      if (Np.gt.Np_mx) then
         call error(label)
         write(*,*) 'Np=',Np
         write(*,*) 'while Np_mx=',Np_mx
         stop
      endif
      
      open(11,file=trim(infile))
      read(11,*)
      do i=1,Np
         read(11,*) (dt(i,j),j=1,dim+1)
      enddo                     ! i
      close(11)
      
c     Wavelength space
      do i=1,Np
         do j=1,dim+1
            xyz(i,j)=dt(i,j)
         enddo                  ! j
      enddo                     ! i

      return
      end
