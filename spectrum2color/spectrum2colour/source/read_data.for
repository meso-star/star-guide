c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine read_data(filename,source,Tsource,material,
     &     matrix,cs,ws,tone_mapping,exposure_bias,whitescale,
     &     gamma_correction)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read the input data file
c     
c     Input:
c       + filename: name of the data file to read
c     
c     Output:
c       + source: (1) if HR Sun spectrum, (2) if Planck spectrum, (3) D65 Illuminant
c       + Tsource: temperature of Planck source
c       + material: name of the reflecting material
c       + matrix: (1) is XYZ -> RGB matrix must be computed, (2) if specified
c       + cs: colorspcae if matrix must be computed
c       + ws: working space if matrix is specified
c       + tone_mapping: T if tone mapping is enabled
c       + exposure_bias: exposure when tone mapping is enabled
c       + whitescale: white scale when tone mapping is enabled
c       + gamma_correction: T if gamma correction is enabled
c     
c     I/O
      character*(Nchar_mx) filename
      integer source
      double precision Tsource
      character*(Nchar_mx) material
      integer matrix
      character*(Nchar_mx) cs
      character*(Nchar_mx) ws
      logical tone_mapping
      double precision exposure_bias
      double precision whitescale
      logical gamma_correction
c     temp
      integer ios,i
c     label
      character*(Nchar_mx) label
      label='subroutine read_data'

      open(11,file=trim(filename),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(filename)
         stop
      else
         do i=1,3
            read(11,*)
         enddo                  ! i
         read(11,*) source
         read(11,*)
         read(11,*) Tsource
         read(11,*)
         read(11,*) material
         read(11,*)
         read(11,*) matrix
         read(11,*)
         read(11,*) cs
         read(11,*)
         read(11,*) ws
         read(11,*)
         read(11,*) tone_mapping
         read(11,*)
         read(11,*) exposure_bias
         read(11,*)
         read(11,*) whitescale
         read(11,*)
         read(11,*) gamma_correction
c     -------------------------------------------------------
c     Inconsistencies
         if ((source.lt.1).or.(source.gt.4)) then
            call error(label)
            write(*,*) 'Bad input argument:'
            write(*,*) 'source=',source
            stop
         endif
         if ((source.eq.2).and.(Tsource.lt.0.0D+0)) then
            call error(label)
            write(*,*) 'Tsource=',Tsource
            write(*,*) 'should be positive'
            stop
         endif
         if ((matrix.ne.1).and.(matrix.ne.2)) then
            call error(label)
            write(*,*) 'Bad input argument:'
            write(*,*) 'matrix=',matrix
            stop
         endif
         if (tone_mapping) then
            if (exposure_bias.le.0.0D+0) then
               call error(label)
               write(*,*) 'exposure_bias=',exposure_bias
               write(*,*) 'should be positive'
               stop
            endif
            if (whitescale.le.0.0D+0) then
               call error(label)
               write(*,*) 'whitescale=',whitescale
               write(*,*) 'should be positive'
               stop
            endif
         endif
c     -------------------------------------------------------
      endif
      close(11)

      return
      end
      
