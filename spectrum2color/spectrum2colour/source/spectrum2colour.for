c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      program spectrum2colour
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'param.inc'
      include 'constants.inc'
c     
c     Purpose: to test the conversion of a spectral signal into a colour
c     
c     Variables
      integer dim
      character*(Nchar_mx) filename
      integer source
      double precision Tsource
      character*(Nchar_mx) material
      integer matrix
      character*(Nchar_mx) cs
      character*(Nchar_mx) ws
      logical tone_mapping
      double precision exposure_bias
      double precision whitescale
      logical gamma_correction
c     
      integer Nlambda_ill
      double precision lambda_ill(1:Nlambda_ill_mx)
      double precision spectrum_ill(1:Nlambda_ill_mx)
      integer Nmaterial
      character*(Nchar_mx) material_name(1:Nmaterial_mx)
      integer Nlambda_mat
      double precision lambda_mat(1:Nlambda_mat_mx)
      double precision sdata(1:Nlambda_mat_mx,1:Nmaterial_mx)
      character*(Nchar_mx) infile
      integer Np
      double precision xyz(1:Np_mx,1:Ndim_mx+1)
      double precision M(1:Ndim_mx,1:Ndim_mx)
      double precision inv_M(1:Ndim_mx,1:Ndim_mx)
      double precision lambda_ref(1:Np_mx)
      double precision illuminant(1:Np_mx)
      double precision spectrum(1:Np_mx)
      double precision signal(1:Np_mx)
      double precision color_xyz(1:Ndim_mx)
      double precision color_rgb(1:Ndim_mx)
      double precision d2source
      double precision Rsource
      double precision omega_source
      integer Nlambda_sun
      double precision lambda_sun(1:Nlambda_sun_mx)
      double precision F_sun(1:Nlambda_sun_mx)
      double precision intF_sun,average_F
      double precision xyz_spectral_resolution
      double precision lambda,lambda_min,lambda_max
      double precision source_spectrum(1:Np_mx)
      double precision sum,sum_rgb,int_Yill
      double precision Xw,Yw,Zw
c     temp
      integer ios,i,j,ilambda,imaterial
      double precision material_albedo(1:Nlambda_mat_mx)
      double precision albedo_spectrum(1:Nlambda_mat_mx)
      double precision v1,v2
      character*(Nchar_mx) command
c     functions
      double precision Blambda
      double precision sigmaT4
c     label
      character*(Nchar_mx) label
      label='program spectrum2colour'

      dim=3
      filename='./data.in'
      call read_data(filename,source,Tsource,material,
     &     matrix,cs,ws,tone_mapping,exposure_bias,whitescale,
     &     gamma_correction)
      d2source=1.0D+0*AU        ! [km]
      Rsource=1.0D+0*Rsun       ! [km]

c     ------------------------------------------------------------------------------------------------------------------
c     XYZ data
      infile='./data/xyz.dat'
      call read_xyzdata(infile,dim,Np,xyz)
      do ilambda=1,Np
         lambda_ref(ilambda)=xyz(ilambda,1)*1.0D-3 ! [micrometers]
      enddo                     ! ilambda
      xyz_spectral_resolution=(lambda_ref(Np)-lambda_ref(1))/(Np-1) ! [micrometers]
      if (matrix.eq.1) then
         call xyz2rgb_matrix(dim,cs,M)
      else
         call specified_xyz2rgb_matrix(dim,ws,M)
      endif                     ! matrix
      call invert_matrix(dim,M,inv_M)
      Xw=inv_M(1,1)+inv_M(1,2)+inv_M(1,3)
      Yw=inv_M(2,1)+inv_M(2,2)+inv_M(2,3)
 2    Zw=inv_M(3,1)+inv_M(3,2)+inv_M(3,3)
c     Debug
      write(*,*) 'M XYZ -> RGB='
      do i=1,dim
         write(*,*) (M(i,j),j=1,dim)
      enddo                     ! i
c     Debug
c     Debug
c      write(*,*) 'M RGB -> XYZ='
c      do i=1,dim
c         write(*,*) (inv_M(i,j),j=1,dim)
c      enddo                     ! i
      write(*,*) 'Xw=',Xw
      write(*,*) 'Yw=',Yw
      write(*,*) 'Zw=',Zw
c     Debug
c     ------------------------------------------------------------------------------------------------------------------
c     
c     
c     ------------------------------------------------------------------------------------------------------------------
c     SOURCE
c     Source spectrum
      if (source.eq.1) then
         omega_source=pi*(Rsource/d2source)**2.0D+0 ! [sr] (6.79e-5 sr for Sun/Earth)
      else if (source.eq.2) then
         call read_HR_Sun_spectrum(Nlambda_sun,
     &        lambda_sun,F_sun,intF_sun) ! [micrometer] / [W/m^2/micrometer] / [W/m^2]
      else if (source.eq.3) then
         filename='./data/standard_illuminant_D65.dat'
         call read_illuminant(filename,
     &        Nlambda_ill,lambda_ill,spectrum_ill)
      else if (source.eq.4) then
         filename='./data/standard_illuminant_A.dat'
         call read_illuminant(filename,
     &        Nlambda_ill,lambda_ill,spectrum_ill)
      endif                     ! source
c
      if ((source.eq.1).or.(source.eq.2)) then
         open(21,file='illuminant.dat')
      endif
      do i=1,Np-1
         lambda_min=lambda_ref(i) ! [micrometer]
         lambda_max=lambda_ref(i+1) ! [micrometer]
         lambda=(lambda_min+lambda_max)/2.0D+0 !  [micrometer]
         if (source.eq.1) then
            lambda=(lambda_min+lambda_max)/2.0D+0 !  [micrometer]
            source_spectrum(i)=
     &           Blambda(Tsource,lambda)*omega_source/pi ! [W/m²/sr/µm]
            write(21,*) lambda*1.0D+3,source_spectrum(i)
         else if (source.eq.2) then
            call average_flux_over_interval(Nlambda_sun,
     &           lambda_sun,F_sun,
     &           lambda_min,lambda_max,average_F) ! [W/m²/µm]
            source_spectrum(i)=average_F/pi ! [W/m²/sr/µm]
            write(21,*) lambda*1.0D+3,source_spectrum(i)
         else if ((source.eq.3).or.(source.eq.4)) then
            call identify_illuminant(Nlambda_ill,lambda_ill,
     &           spectrum_ill,lambda_min,v1)
            call identify_illuminant(Nlambda_ill,lambda_ill,
     &           spectrum_ill,lambda_max,v2)
            source_spectrum(i)=(v1+v2)/2.0D+0
         endif                  ! source
      enddo                     ! i
      if ((source.eq.1).or.(source.eq.2)) then
         close(21)
      endif
c     Normalisation by the Y coordinate of the illuminant
      do i=1,Np-1
         signal(i)=source_spectrum(i)*(xyz(i,3)+xyz(i+1,3))/2.0D+0 ! [W/m^2/sr/micrometer]
      enddo                     ! i
      call integration_intervals(Np,lambda_ref,signal,int_Yill)
c     Debug
c      write(*,*) 'int_Yill=',int_Yill
c     Debug
      do i=1,Np-1
         source_spectrum(i)=source_spectrum(i)*3.0D+0/int_Yill ! [W/m²/sr/µm]
      enddo                     ! i
c     ------------------------------------------------------------------------------------------------------------------
c
c
c     ------------------------------------------------------------------------------------------------------------------
c     ALBEDO
c     read albedo database
      infile='./data/LUMA_SLUM_SW.csv'
      call read_material_spectral_data(infile,
     &     Nmaterial,material_name,Nlambda_mat,lambda_mat,sdata) ! lambda_mat: [micrometer]
c     extract material albedo signal
      call extract_albedo_signal(Nmaterial,Nlambda_mat,
     &     material_name,sdata,material,
     &     material_albedo)
c     average albedo over a 5 nm interval around each value of the reference wavelength
      filename='./data/albedo_'//trim(material)//'.dat'
      open(31,file=trim(filename))
      do i=1,Np-1
         lambda_min=lambda_ref(i) ! [micrometer]
         lambda_max=lambda_ref(i+1) ! [micrometer]
         lambda=(lambda_min+lambda_max)/2.0D+0 ! [micrometer]
         call average_albedo_over_interval(Nlambda_mat,
     &        lambda_mat,material_albedo,
     &        lambda_min,lambda_max,albedo_spectrum(i))
         write(31,*) lambda*1.0D+3,albedo_spectrum(i) ! lambda in nm
      enddo                     ! i
      close(31)
c     ------------------------------------------------------------------------------------------------------------------
c
c      
c     ------------------------------------------------------------------------------------------------------------------
c     SPECTRAL SIGNAL TO CONVERT AS A COLOR
      do i=1,Np-1
         spectrum(i)=source_spectrum(i)*albedo_spectrum(i) ! [W/m²/sr/µm]
      enddo                     ! i
c     ------------------------------------------------------------------------------------------------------------------
c
c      
c     ------------------------------------------------------------------------------------------------------------------
c     CONVERSION
c     convert spectral signal in XYZ color
      call spectrum2xyz(dim,Np,xyz,spectrum,color_xyz)
c     tonemapping
      if (tone_mapping) then
         call filmic_tonemapping(dim,exposure_bias,whitescale,
     &        color_xyz)
      endif
c     convert XYZ into RGB
      call xyz2rgb(dim,M,gamma_correction,color_xyz,color_rgb)
c     display results
      write(*,*) 'XYZ=',(color_xyz(i),i=1,dim)
      sum_rgb=0.0D+0
      do i=1,dim
         sum_rgb=sum_rgb+color_rgb(i)
      enddo                     ! i
      write(*,*) 'rgb=',(color_rgb(i)/sum_rgb*1.0D+2,i=1,dim)
      write(*,21) 'RGB= ',nint(color_rgb(1)*255),' ',
     &     nint(color_rgb(2)*255),' ',nint(color_rgb(3)*255)
c     ------------------------------------------------------------------------------------------------------------------

c     remove temporary files
      command='rm -f ./nlines*'
      call exec(command)
      
      end
