	integer Ndim_mx
	integer Np_mx
	integer Nmaterial_mx
	integer Nlambda_mat_mx
	integer Nlambda_ill_mx
	integer Nlambda_sun_mx
	integer Nchar_mx

	parameter(Ndim_mx=3)
	parameter(Np_mx=100)
        parameter(Nmaterial_mx=100)
        parameter(Nlambda_mat_mx=1000)
        parameter(Nlambda_ill_mx=1000)
	parameter(Nlambda_sun_mx=3500000)
	parameter(Nchar_mx=1024)
	