	double precision alphax(0:2)
	double precision alphay(0:1)
	double precision alphaz(0:1)
	double precision betax(0:2)
	double precision betay(0:1)
	double precision betaz(0:1)
	double precision gammax(0:2)
	double precision gammay(0:1)
	double precision gammaz(0:1)
	double precision deltax(0:2)
	double precision deltay(0:1)
	double precision deltaz(0:1)

	common /com_xyzdata1/ alphax
	common /com_xyzdata2/ alphay
	common /com_xyzdata3/ alphaz
	common /com_xyzdata4/ betax
	common /com_xyzdata5/ betay
	common /com_xyzdata6/ betaz
	common /com_xyzdata7/ gammax
	common /com_xyzdata8/ gammay
	common /com_xyzdata9/ gammaz
	common /com_xyzdata10/ deltax
	common /com_xyzdata11/ deltay
	common /com_xyzdata12/ deltaz
	