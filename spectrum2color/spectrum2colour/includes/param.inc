        double precision pi,kBz,c0,hPl,sigma
	parameter(pi=3.141592653589793238462643383279502884197169399D+0)
	parameter(kBz=1.3806D-23)    ! J/K     (Constante de Boltzmann)
	parameter(c0=2.9979D+8)      ! m/s     (c�l�rit� de la lumi�re dans le vide)
	parameter(hPl=6.6262D-34)    ! Js      (constante de Planck)
	parameter(sigma=5.6696D-8)   ! W/m2/K4 (constante de Stefan-Boltzman)
	