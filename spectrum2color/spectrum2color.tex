\documentclass[epsf,psfig,fancyheadings,12pt]{article}
%\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
%\usepackage[francais]{babel}
\usepackage[utf8]{inputenc}
\usepackage{fancyhdr}
\usepackage{amsmath}
\usepackage{graphics}
\usepackage{subfigure}
\usepackage[final]{epsfig}
\usepackage{mathrsfs}
%\usepackage{auto-pst-pdf}
%\usepackage{pstricks,pst-node,pst-text,pst-3d}
\usepackage{amssymb}
\usepackage{multirow}
\usepackage{rotating}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{animate}
\usepackage{bm}
\usepackage{array}
\usepackage{float}
\usepackage[toc,page]{appendix}
\makeatletter
% double spacing lines 
\renewcommand{\baselinestretch}{1}
\newcommand{\LyX}{L\kern-.1667em\lower.25em\hbox{Y}\kern-.125emX\spacefactor1000}
%pour faire reference toujours avec la meme norme
%lyx !
\newcommand{\eq}[1]{Eq.~\ref{eq:#1}}
\newcommand{\fig}[1]{Fig.~\ref{fig:#1}}
\newcommand{\tab}[1]{Tab.~\ref{tab:#1}}
\newcommand{\para}[1]{Sec.~\ref{para:#1}}
\newcommand{\ap}[1]{Appendix~\ref{ap:#1}}
\renewcommand{\appendixtocname}{Annexes} 
\renewcommand{\appendixpagename}{Annexes}
\newcommand{\ul}{\underline}
\newcommand\crule[3][black]{\textcolor[RGB]{#1}{\rule{#2}{#3}}}

%
\makeatother

%\fancyhf{}%
% Utiliser \fancyhead[L] pour mettre du texte dans la partie de gauche
% du header
%\fancyhead[L]{}%
%\fancyhead[R]{\thepage}%
%\renewcommand{\headrulewidth}{0pt} % ça c'est pour faire une ligne horizontale
%\headsep=25pt % ça c'est pour laisser de la place entre le header et le texte
%\pagestyle{fancy}%

% margins
\setlength{\topmargin}{-0.75in}
\setlength{\textheight}{9in}
\setlength{\oddsidemargin}{-.50in}
\setlength{\textwidth}{7.5in}



\begin{document}


\title{MODRADURB: a short and biased introduction to CIE colorimetry.}

\maketitle
\begin{center}
\author{\bf|Méso|Star>}\\
 (\url{http://www.meso-star.com}) \\
\end{center}
\vspace{1cm}

\tableofcontents

\newpage

\section{Introduction}

The purpose of the present document is to keep track of the work performed in the context of the MODRADURB project in the field of colorimetry. Since one of the purposes of the project is to produce images in the visible part of the spectrum of urban scenes, the question of generating the right color for every material used in the scene is a concern.

The question of producing colors from a given radiance spectral signal was already a question in the High-Tune project. However, the only colors that had to be rendered in this project were those of the sky, clouds and ground: the reflectivity of the ground is gray (a single value for the whole visible spectrum), the correct reproduction of colors was not a major issue. Moreover, its was necessary to introduce a tone mapper in the HTRDR software in order to deal with highly dynamic scenes: some parts of the image (edge of the clouds, sun if present in the image, reflections) being much more luminous than others.

In this document, the question is much more limited: how can we produce a credible color for a given (known) material illuminated by sunlight, given the reflection properties of that material ? The question was then slightly extended to any source of illumination.

\section{CIE colorimetry}

We are not interested in a description of all the work that ever contributed to the field of {\it colorimetry}. We will only work with some results of CIE colorimetry; CIE stands for {\it Commission Internationale de l'Eclairement}, a institution that was created in 1917. It defined the CIE 1931 color system, a standard model of human color vision that was internationaly adopted in order to reproduce colors in color prints, color displays, etc. The CIE 1931 standard is a Color Matching System: its purpose is not to define how humans perceive colors, but to numerically quantify a measured color, and accurately reproduce it. The human (subjective) perception of colors is much more complex than the linear algebra used in CIE colorimetry.

\subsection{Basics about colorspaces}

Some aspects of the human biology are nonetheless required in order to understand how the CIE established its color system. The human retina counts 3 types of color receptors (cones)\footnote{Some species can have a higher number of photoreceptors. The most complex ``eye'' known is attributed to the mantis shrimp. Depending on the particular species, their eye counts between 12 and 16 photoreceptors, can perceive ultraviolet radiation and light polarisation.}. Each type of cone cells is sensible to different wavelengths. This is the reason why they are commonly refered as red, green and blue receptors. However, each type of cones is sensible to all visible wavelengths: their response curves overlap, and only have different peaks in the ``red'', ``green'' and ``blue'' regions of the spectrum. This is the reason why different input spectral signals can result in the perception of similar colors (phenomenon that is known as {\it metamerism}).

In 1924, the CIE published a Luminous Efficiency Function $V(\lambda)$ (see figure \ref{fig:CIE1924_LEF}). This function gives the sensitivity of the average human eye to the various wavelengths in the visible spectrum (global information). This response curve only means that even though two monochromatic light sources can provide the same output radiance at various wavelengths, they will not be interpreted as equally bright. Similarly, if two light sources (at two different wavelengths) appear as equally bright, their output radiances are not equal. Using the CIE 1924 luminous efficiency function $V(\lambda)$, it is possible to define the total perceived brightness from the spectral signal of incoming radiative flux:

\begin{equation}
  \Phi=\int_{380}^{780} V(\lambda) \phi(\lambda)d\lambda
  \label{eq:global_brightness}
\end{equation}

\begin{figure}[!ht]
\centering
\includegraphics[width=0.50\textwidth,angle=0]{./figures/CIE1924_LEF.pdf}
\caption[]{The CIE 1924 Luminous Efficiency Function}
\label{fig:CIE1924_LEF}
\end{figure}

Then in 1931, the CIE produced the RGB color matching functions (see figure \ref{fig:CIE1931_RGBCMF}). These 3 functions $\bar{r}(\lambda)$, $\bar{g}(\lambda)$ and $\bar{b}(\lambda)$ provide an answer to the following problem: suppose we have three pure (=monochromatic) lasers, one red, one green and one blue that are focused at the same location on a (white) screen. If we select a given wavelength $\lambda$, we should be able to reproduce the color that the average human eye/brain associates with this wavelength by adjusting the output radiance of the 3 light sources. The CIE RGB color matching functions provide the exact amounts of red, green and blue lights that are needed, for every visible wavelength. They can consequently be used in order to compute the R, G and B components of a color, provided the spectral description $L(\lambda)$ of the radiance incoming on a human retina:

\begin{figure}[!ht]
\centering
\includegraphics[width=0.50\textwidth,angle=0]{./figures/CIE1931_RGBCMF.pdf}
\caption[]{The CIE 1931 RGB Color Matching Functions}
\label{fig:CIE1931_RGBCMF}
\end{figure}

\begin{equation}
  \begin{cases}
    R=\int_{380}^{780} \bar{r}(\lambda) L(\lambda))d\lambda \\[10pt]
    G=\int_{380}^{780} \bar{g}(\lambda) L(\lambda))d\lambda \\[10pt]
    B=\int_{380}^{780} \bar{b}(\lambda) L(\lambda)) d\lambda
  \end{cases}
  \label{eq:RGB_components}
\end{equation}

These 3 values (called tristimulus) can then be used in order to reproduce the corresponding color on a display.

The main issue with the $\bar{r}(\lambda)$, $\bar{g}(\lambda)$ and $\bar{b}(\lambda)$ functions is that they can take negative values. In other words, for some wavelengths (for instance 520 nm), the radiance of the red laser should be negative in order to reproduce the right color. Since this is not possible, it is just impossible to reproduce some colors using this approach.

The CIE finally decided to create a new color space: the XYZ colorspace. The corresponding $\bar{x}(\lambda)$, $\bar{y}(\lambda)$ and $\bar{z}(\lambda)$ color matching functions are a linear transformation of the previous $\bar{r}(\lambda)$, $\bar{g}(\lambda)$ and $\bar{b}(\lambda)$ functions (see figure \ref{fig:CIE1931_XYZCMF}), so that they can have interesting mathematical properties. The main ideas behind the XYZ colorspace are that the $\bar{g}(\lambda)$ function is the previously defined $V(\lambda)$ function, and that the corresponding color matching functions should all have positive values. One consequence is that the associated X, Y and Z primaries do not represent any real color (they are commonly refered as ``imaginary'' primaries).

\begin{figure}[!ht]
\centering
\includegraphics[width=0.50\textwidth,angle=0]{./figures/CIE1931_XYZCMF.pdf}
\caption[]{The CIE 1931 XYZ Color Matching Functions}
\label{fig:CIE1931_XYZCMF}
\end{figure}

In practice, the (X, Y, Z) tristimulus are computed from the spectral signal of radiance using $\bar{x}(\lambda)$, $\bar{y}(\lambda)$ and $\bar{z}(\lambda)$ color matching functions:

\begin{equation}
  \begin{cases}
    X=\int_{380}^{780} \bar{x}(\lambda) L(\lambda))d\lambda \\[10pt]
    Y=\int_{380}^{780} \bar{y}(\lambda) L(\lambda))d\lambda \\[10pt]
    Z=\int_{380}^{780} \bar{z}(\lambda) L(\lambda)) d\lambda
  \end{cases}
  \label{eq:XYZ_components_emission}
\end{equation}

This procedure is standard for computing the X, Y and Z components of the color corresponding to a given radiance spectrum {\bf in the case of radiation emission} by a given material. In the case of interest in this document, i.e. reflection of incoming light by a material, it is common to find the following modified version:

\begin{equation}
  \begin{cases}
    X=\frac{C}{N}\int_{380}^{780} \bar{x}(\lambda) \rho(\lambda) I_{0}(\lambda)d\lambda \\[10pt]
    Y=\frac{C}{N}\int_{380}^{780} \bar{y}(\lambda) \rho(\lambda) I_{0}(\lambda)d\lambda \\[10pt]
    Z=\frac{C}{N}\int_{380}^{780} \bar{z}(\lambda) \rho(\lambda) I_{0}(\lambda) d\lambda
  \end{cases}
  \label{eq:XYZ_components_reflection}
\end{equation}

with $\rho(\lambda)$ the spectral reflectivity (albedo) of the material, $I_{0}(\lambda)$ the spectral signal of incoming radiation (illuminant). From eq. \ref{eq:XYZ_components_reflection}, we can see the values of the integrals over the visible spectrum are all normalised by $N$:

\begin{equation}
  N=\int_{380}^{780} \bar{y}(\lambda) I_{0}(\lambda)d\lambda
  \label{eq:normalization_factor_N}
\end{equation}
which we recognize as the total brightness of the illuminant from eq. \ref{eq:global_brightness}. The X, Y and Z components are therefore normalized by the (spectrally integrated) brightness. The resulting color will therefore be independant of the total incoming flux. This is the reason why a scaling factor $C$ is necessary in order to account for the perception of the color that depends on the total flux of light that illuminates the material: to a human, a given material can appear both as black when it is in total darkness, and white when the illumination flux is very high. There is therefore no rule that would give us the value of $K$: it has to be adjusted for a given application. In the case of the present document, a value of $C=3$ was retained.

Equations sets \ref{eq:XYZ_components_emission} and \ref{eq:XYZ_components_reflection} are identical in the sense X, Y and Z are only the integrals of a given spectral signal. Equation set \ref{eq:XYZ_components_reflection} is more detailled: it introduces the concept of illuminant, and a scaling factor that is specific to the experiment.

Finally, a linear transformation is used in order to convert (XYZ) tristimulus into (RGB):

\begin{equation}
  \begin{bmatrix}
    R \\
    G \\
    B
  \end{bmatrix}
  =
  \begin{bmatrix}
    \mathbf{M}
  \end{bmatrix}
  \begin{bmatrix}
    X \\
    Y \\
    Z
  \end{bmatrix}
  \label{eq:XYZ2RGB}
\end{equation}

The transformation matrix $[{\bf M}]$ is specific for a given display; it may be provided, or computed according to the chosen illuminant and characteristics of the display (see appendix \ref{app:matrix}).

There are many colorspaces, and linear transformations are used to convert colors from one into the other. The standard procedure is now to work in the (display independant) CIE 1931 XYZ colorspace before converting the resulting color into any other particular colorspace.


\subsection{Illuminant}

Equations \ref{eq:XYZ_components_reflection} introduce the concept of {\it illuminant}: its power spectral distribution function is designated by $I_{0}(\lambda)$. It is only logical to think the spectral distribution of incoming light will influence the perceived color of the illuminated material. For instance, a material that is perceived as red under white light (it reflects only the highest wavelengths) will be perceived as black when illuminated by blue light (short wavelengths).

Equations \ref{eq:XYZ_components_emission} can be used in a general way in order to compute the values of the tristimulus associated to a given spectral signal $L(\lambda)$. In particular, when $L(\lambda)=B(\lambda,T)$ is the blackbody radiation distribution function, these equations may be used in order to compute the color associated with a blackbody at temperature $T$. Equations \ref{eq:XYZ_components_reflection} are more specific to the case of a material characterized by its reflectivity function $\rho(\lambda)$, illuminated by a spectral signal $I_{0}(\lambda)$. This signal can be of any nature: blackbody radiation (tungsten lightbulb), monochromatic (laser), spiked (fluorescent lamps), the Sun filtered through the atmosphere, etc.

\begin{figure}[!ht]
\centering
\includegraphics[width=0.50\textwidth,angle=0]{./figures/illuminants.pdf}
\caption[]{Relative spectral distribution of various illuminants}
\label{fig:illuminants}
\end{figure}

Figure \ref{fig:illuminants} shows various illuminants: standard illuminants A and D65 (defined by the CIE), the spectrum of a blackbody at 5773K, and the spectrum of sunlight. The illuminant A is a blackbody at 2856 K, and is intended at representing the spectral distribution of light provided by a tungsten-filament lightbulb. The illuminant D65 represents the spectral distribution of daylight, for a {\it color temperature} of 6500 K. The 5773 K blackbody (emission temperature of the Sun) is also represented, and the distribution of sunlight (taken from \cite{Kurucz:1995}, and averaged over 5-nm spectral intervals) matches closely the illuminant D65.

The influence of the illuminant on the reproduced color will be examined in part \ref{para:results}.

One interesting test to perform in order to check all the math involved in colorimetry has been understood, is to compute the $(X, Y, Z)$ coordinates corresponding to the reference illuminant used to establish a given transformation matrix $[{\bf M}]$. For instance, the $[{\bf M}]$ matrix given at the end of appendix \ref{app:matrix} (relation \ref{eq:MRec709}) has been established for the Rec709 colorsystem (or sRGB) and the D65 standard illuminant. Using relations \ref{eq:XYZ_components_reflection}, we can compute the $(X_{D65}, Y_{D65}, Z_{D65})$ tristimuli of the D65 illuminant (cf. fig. \ref{fig:illuminants}), using $\rho(\lambda)$=1. This is perfectly equivalent to computing $(X, Y, Z)$ using relations \ref{eq:XYZ_components_emission} with $L(\lambda)=I_{0,D65}(\lambda)$, and normalizing the results by the value of $Y$. We obtain the following values:

\begin{equation}
  \begin{cases}
    X_{D65}=0.9504\\
    Y_{D65}=1.000\\
    Z_{D65}=1.0883
  \end{cases}
  \label{eq:XYZ_D65}
\end{equation}

Using matrix $[{\bf M}]$ given by relation \ref{eq:MRec709} in eq. \ref{eq:XYZ2RGB}, the $(R_{D65}, G_{D65}, B_{D65})$ coordinates of the D65 illuminant are:

\begin{equation}
  \begin{cases}
    R_{D65}=1.0\\
    G_{D65}=1.0\\
    B_{D65}=1.0
  \end{cases}
  \label{eq:RGB_D65}
\end{equation}


\section{Reflectivity data}

In the next section, the color of various materials has been reproduced using the sRGB transformation matrix given in appendix \ref{app:matrix} (relation \ref{eq:MRec709}) using spectral reflectivity data provided by S. Kotthaus\footnote{\url{https://urban-meteorology-reading.github.io/SLUM}} (University of Reading).

\begin{figure}[H]
\centering
\includegraphics[width=0.50\textwidth,angle=0]{./figures/albedo.pdf}
\caption[]{Spectral reflectiviy of some materials}
\label{fig:albedo}
\end{figure}

Figure \ref{fig:albedo} shows the reflectivity signal for various materials, extracted from this database: sandstone (S001), asphalt roofing shingle (A007),  a cement brick (B001) and blue PVC (V004). Data is provided at a very fine spectral resolution, and for a wide variety of materials (74 in total).



\section{Post-processing}

Most documentation sources agree on the fact that a gamma-correction should be applied on the $(R, G, B)$ values produced after linear conversion from $(X, Y, Z)$ tristimuli. This correction takes into account the fact that the human perception of colors is not linear with brightness. Each author has its recommended ``recipe'' for this gamma-correction. Most of the time, it consists in applying the following transformation over each $u$ coordinate, $u \in (R, G, B)$:

\begin{equation}
  u=
  \begin{cases}
    \kappa u& \text{if } u< \epsilon\\
    (1+a)u^{\frac{1}{\gamma}}-a& \text{otherwise}
  \end{cases}
  \label{eq:gamma_correction}
\end{equation}

Most of the time, the values of the various parameters are given as: $\epsilon=3.1308~10^{-3}$, $\kappa=12.92$, $a=5.50~10^{-2}$ and $\gamma=2.4$, but this can change from source to source. More troublesome is the fact there in obviously an open war about the value of the $\gamma$ parameter: it is most of the time reported as varying between 2.2 and 2.4, but some authors claim it can be as low as 1.8. And no one seemed to ever agree on anything when it comes to Apple displays.

In section \ref{para:results}, the results that are shown have been obtained without any sort of gamma-correction. Whenever a gamma-correction was applied, the resulting color always felt wrong, but this could be a very personal thing.

Another topic is tone-mapping: a specific scaling can be applied on $(R, G, B)$ values for the various pixels of an image, to take into account the many orders of magnitude of brightness between very well lit zones and dark areas of the image (High Dynamic Range images). The human eye/brain has no problem for constructing a coherent image in such cases, but this is very different for cameras. Specific algorithms are implemented whenever a image has to be produced, either in digital photography or image rendering. Since no image had to be produced in the context of this document, this subject was not of interest.


\section{Results}
\label{para:results}

This section presents the results obtained over 18 different materials. The RGB color code is computed from relation \ref{eq:XYZ2RGB}, using the sRGB transformation matrix given by eq. \ref{eq:MRec709}. The $(X, Y, Z)$ tristimuli have been computed using relations \ref{eq:XYZ_components_reflection} with a scaling coefficient $C=3$. For each material, three colors have been generated, using three different illuminants:
\begin{itemize}
\item a 5773 K blackbody (the emission temperature of the Sun)
\item the high-resolution spectrum of the Sun (\cite{Kurucz:1995}), measured outside the atmosphere, and averaged over each 5-nm spectral interval the $\bar{x}(\lambda)$, $\bar{y}(\lambda)$ and $\bar{z}(\lambda)$ color matching functions are provided for
\item the CIE standard illuminant D65.
\end{itemize}

When comparing the colors obtained for each type of illuminant with the picture of the material (provided with the reflectivity database), remember the photographs may have been taken with various global levels of brightness, and the colors have been computed for a arbitrary value of the $C$ scaling coefficient that gave the best results for most materials. Materials have been selected mainly among non-composite ones, so that it is relevant to attach a single color to the material.

\begin{table}[H]
  \centering \begin{tabular}{|>{\centering\arraybackslash}m{2cm}||>{\centering\arraybackslash}m{6cm}|>{\centering\arraybackslash}m{3cm}|>{\centering\arraybackslash}m{3cm}|>{\centering\arraybackslash}m{3cm}|}\hline
\textbf{Material} & Picture & 5773K & TOA Sun & D65\\ \hline
S001 & \includegraphics[width=6cm]{./figures/S001.pdf} & \colorbox[rgb]{1,0.70,0.44}{\makebox(80,110){\textcolor{black}{255 180 112}}} & \colorbox[rgb]{1,0.72,0.46}{\makebox(80,110){\textcolor{black}{255 183 117}}} & \colorbox[rgb]{1,0.79,0.53}{\makebox(80,110){\textcolor{black}{255 202 136}}}\\ \hline
S002 & \includegraphics[width=6cm]{./figures/S002.pdf} & \colorbox[rgb]{0.71,0.57,0.39}{\makebox(80,110){\textcolor{black}{183 145 99}}} & \colorbox[rgb]{0.70,0.57,0.40}{\makebox(80,110){\textcolor{black}{179 146 102}}} & \colorbox[rgb]{0.65,0.58,0.43}{\makebox(80,110){\textcolor{black}{165 149 110}}}\\ \hline
S003 & \includegraphics[width=6cm]{./figures/S003.pdf} & \colorbox[rgb]{0.863,0.571,0.301}{\makebox(80,110){\textcolor{black}{220 146 77}}} & \colorbox[rgb]{0.849,0.573,0.312}{\makebox(80,110){\textcolor{black}{216 146  79}}} & \colorbox[rgb]{0.788,0.583,0.335}{\makebox(80,110){\textcolor{black}{201 149  86}}}\\ \hline
S004 & \includegraphics[width=6cm]{./figures/S004.pdf} & \colorbox[rgb]{1.000,0.731,0.486}{\makebox(80,110){\textcolor{black}{255 186 124}}} & \colorbox[rgb]{1.000,0.748,0.510}{\makebox(80,110){\textcolor{black}{255 191 130}}} & \colorbox[rgb]{1.000,0.824,0.592}{\makebox(80,110){\textcolor{black}{255 210 151}}}\\ \hline
  \end{tabular}
  \caption{Color computed for various materials, using high-resolution reflectivity data and three illuminants. Column 1: designation of the material in the original database; column 2: picture of the target material; column 3: RGB color code computed using a 5773K blackbody as illuminant; column 4: RGB color computed using a high-resolution spectrum of sunlight outside the atmosphere as illuminant; column 5: RGB color computed using the CIE D65 standard illuminant.}
  \label{materials_colors_001}
\end{table}

\begin{table}[H]
  \centering \begin{tabular}{|>{\centering\arraybackslash}m{2cm}||>{\centering\arraybackslash}m{6cm}|>{\centering\arraybackslash}m{3cm}|>{\centering\arraybackslash}m{3cm}|>{\centering\arraybackslash}m{3cm}|}\hline
\textbf{Material} & Picture & 5773K & TOA Sun & D65\\ \hline
A007 & \includegraphics[width=6cm]{./figures/A007.pdf} & \colorbox[rgb]{0.234,0.196,0.178}{\makebox(80,110){\textcolor{white}{60  50  45}}} & \colorbox[rgb]{0.230,0.197,0.183}{\makebox(80,110){\textcolor{white}{59  50  47}}} & \colorbox[rgb]{0.211,0.201,0.195}{\makebox(80,110){\textcolor{white}{54  51  50}}}\\ \hline
C001 & \includegraphics[width=6cm]{./figures/C001.pdf} & \colorbox[rgb]{1.000,0.671,0.392}{\makebox(80,110){\textcolor{black}{255 171 100}}} & \colorbox[rgb]{1.000,0.685,0.411}{\makebox(80,110){\textcolor{black}{255 175 105}}} & \colorbox[rgb]{0.981,0.737,0.467}{\makebox(80,110){\textcolor{black}{250 188 119}}}\\ \hline
C002 & \includegraphics[width=6cm]{./figures/C002.pdf} & \colorbox[rgb]{0.745,0.528,0.355}{\makebox(80,110){\textcolor{black}{190 135  91}}} & \colorbox[rgb]{0.732,0.530,0.366}{\makebox(80,110){\textcolor{black}{187 135  93}}} & \colorbox[rgb]{0.678,0.540,0.393}{\makebox(80,110){\textcolor{black}{173 138 100}}}\\ \hline
B001 & \includegraphics[width=6cm]{./figures/B001.pdf} & \colorbox[rgb]{1.000,0.623,0.276}{\makebox(80,110){\textcolor{black}{255 159  70}}} & \colorbox[rgb]{1.000,0.636,0.291}{\makebox(80,110){\textcolor{black}{255 162  74}}} & \colorbox[rgb]{1.000,0.693,0.337}{\makebox(80,110){\textcolor{black}{255 177  86}}}\\ \hline
B002 & \includegraphics[width=6cm]{./figures/B002.pdf} & \colorbox[rgb]{0.402,0.323,0.257}{\makebox(80,110){\textcolor{black}{103  82  66}}} & \colorbox[rgb]{0.395,0.324,0.264}{\makebox(80,110){\textcolor{black}{101  83  67}}} & \colorbox[rgb]{0.364,0.331,0.283}{\makebox(80,110){\textcolor{black}{93  84  72}}}\\ \hline
  \end{tabular}
  \caption{Color computed for various materials, using high-resolution reflectivity data and three illuminants. Column 1: designation of the material in the original database; column 2: picture of the target material; column 3: RGB color code computed using a 5773K blackbody as illuminant; column 4: RGB color computed using a high-resolution spectrum of sunlight outside the atmosphere as illuminant; column 5: RGB color computed using the CIE D65 standard illuminant.}
  \label{materials_colors_002}
\end{table}

\begin{table}[H]
  \centering \begin{tabular}{|>{\centering\arraybackslash}m{2cm}||>{\centering\arraybackslash}m{6cm}|>{\centering\arraybackslash}m{3cm}|>{\centering\arraybackslash}m{3cm}|>{\centering\arraybackslash}m{3cm}|}\hline
\textbf{Material} & Picture & 5773K & TOA Sun & D65\\ \hline
B005 & \includegraphics[width=6cm]{./figures/B005.pdf} & \colorbox[rgb]{0.680,0.348,0.252}{\makebox(80,110){\textcolor{black}{173  89  64}}} & \colorbox[rgb]{0.668,0.350,0.259}{\makebox(80,110){\textcolor{black}{170  89  66}}} & \colorbox[rgb]{0.624,0.356,0.278}{\makebox(80,110){\textcolor{black}{159  91  71}}}\\ \hline
B009 & \includegraphics[width=6cm]{./figures/B009.pdf} & \colorbox[rgb]{0.891,0.485,0.264}{\makebox(80,110){\textcolor{black}{227 124  67}}} & \colorbox[rgb]{0.875,0.488,0.273}{\makebox(80,110){\textcolor{black}{223 124  70}}} & \colorbox[rgb]{0.816,0.496,0.294}{\makebox(80,110){\textcolor{black}{208 126  75}}}\\ \hline
L003 & \includegraphics[width=6cm]{./figures/L003.pdf} & \colorbox[rgb]{0.232,0.174,0.141}{\makebox(80,110){\textcolor{white}{59  44  36}}} & \colorbox[rgb]{0.227,0.174,0.144}{\makebox(80,110){\textcolor{white}{58  44  37}}} & \colorbox[rgb]{0.210,0.178,0.155}{\makebox(80,110){\textcolor{white}{53  45  39}}}\\ \hline
R001 & \includegraphics[width=6cm]{./figures/R001.pdf} & \colorbox[rgb]{1.000,0.377,0.225}{\makebox(80,110){\textcolor{black}{255  96  57}}} & \colorbox[rgb]{1.000,0.386,0.236}{\makebox(80,110){\textcolor{black}{255  98  60}}} & \colorbox[rgb]{0.977,0.407,0.265}{\makebox(80,110){\textcolor{black}{249 104  68}}}\\ \hline
R002 & \includegraphics[width=6cm]{./figures/R002.pdf} & \colorbox[rgb]{0.373,0.235,0.163}{\makebox(80,110){\textcolor{yellow}{95  60  42}}} & \colorbox[rgb]{0.366,0.236,0.168}{\makebox(80,110){\textcolor{yellow}{93  60  43}}} & \colorbox[rgb]{0.340,0.241,0.180}{\makebox(80,110){\textcolor{yellow}{87  61  46}}}\\ \hline
  \end{tabular}
  \caption{Color computed for various materials, using high-resolution reflectivity data and three illuminants. Column 1: designation of the material in the original database; column 2: picture of the target material; column 3: RGB color code computed using a 5773K blackbody as illuminant; column 4: RGB color computed using a high-resolution spectrum of sunlight outside the atmosphere as illuminant; column 5: RGB color computed using the CIE D65 standard illuminant.}
  \label{materials_colors_003}
\end{table}

\begin{table}[H]
  \centering \begin{tabular}{|>{\centering\arraybackslash}m{2cm}||>{\centering\arraybackslash}m{6cm}|>{\centering\arraybackslash}m{3cm}|>{\centering\arraybackslash}m{3cm}|>{\centering\arraybackslash}m{3cm}|}\hline
\textbf{Material} & Picture & 5773K & TOA Sun & D65\\ \hline
V001 & \includegraphics[width=6cm]{./figures/V001.pdf} & \colorbox[rgb]{0.280,0.265,0.273}{\makebox(80,110){\textcolor{yellow}{71  68  70}}} & \colorbox[rgb]{0.274,0.266,0.279}{\makebox(80,110){\textcolor{yellow}{70  68  71}}} & \colorbox[rgb]{0.250,0.271,0.298}{\makebox(80,110){\textcolor{yellow}{64  69  76}}}\\ \hline
V004 & \includegraphics[width=6cm]{./figures/V004.pdf} & \colorbox[rgb]{0.064,0.298,0.555}{\makebox(80,110){\textcolor{black}{16  76 141}}} & \colorbox[rgb]{0.058,0.300,0.569}{\makebox(80,110){\textcolor{black}{15  76 145}}} & \colorbox[rgb]{0.040,0.309,0.605}{\makebox(80,110){\textcolor{black}{10  79 154}}}\\ \hline
V005 & \includegraphics[width=6cm]{./figures/V005.pdf} & \colorbox[rgb]{0.829,0.244,0.127}{\makebox(80,110){\textcolor{black}{211  62  32}}} & \colorbox[rgb]{0.814,0.246,0.132}{\makebox(80,110){\textcolor{black}{208  63  34}}} & \colorbox[rgb]{0.766,0.250,0.143}{\makebox(80,110){\textcolor{black}{195  64  36}}}\\ \hline
Z002 & \includegraphics[width=6cm]{./figures/Z002.pdf} & \colorbox[rgb]{0.805,0.702,0.635}{\makebox(80,110){\textcolor{black}{205 179 162}}} & \colorbox[rgb]{0.789,0.705,0.651}{\makebox(80,110){\textcolor{black}{201 180 166}}} & \colorbox[rgb]{0.724,0.719,0.696}{\makebox(80,110){\textcolor{black}{185 183 177}}}\\ \hline
  \end{tabular}
  \caption{Color computed for various materials, using high-resolution reflectivity data and three illuminants. Column 1: designation of the material in the original database; column 2: picture of the target material; column 3: RGB color code computed using a 5773K blackbody as illuminant; column 4: RGB color computed using a high-resolution spectrum of sunlight outside the atmosphere as illuminant; column 5: RGB color computed using the CIE D65 standard illuminant.}
  \label{materials_colors_004}
\end{table}

\subsection{Discussion}

With the notable exception of the V001 material (lead grey PVC roofing sheet), all reproduced colors are credible, in the sense we can match computed colors with the {\it feeling} of the color we mentally associate to the target material illuminated by sunlight. Of course, computed colors do not take into account many aspects that can influence this feeling: inhomogeneities of the material, surface roughness, reflections, etc.

Shown to as impartial as possible people (children), it looks as if the prefered illuminant is most of the time the standard illuminant D65, and in approximately one third of cases, the two other illuminants have been selected as providing the most accurate color. This experiment needs to be reproduced whenever possible. Please feel free to comment on the accuracy (or lack thereof) of the results, or additional numerical experiments of interest (\href{mailto:vincent.eymet@meso-star.com}{vincent.eymet@meso-star.com}).

\newpage
\appendix
\section{Production of the transformation matrix}
\label{app:matrix}

The production of the transformation matrix from the CIE XYZ colorspace to any other colorspace can easily be found, such as in \cite{Glassner:1994}. However, each author uses its own notations and conventions that are not always well documented. This sections tries of provide a coherent description of what could be found in many sources of documentation.

In order to compute the transformation matrix, some physical characteristics have to be known about the target display. These characteristics are the chromatic coordinates of the red, green and blue colors, which has no meaning in the absolute. These chromatic coordinates are physically measured for the colors that the manufacturer of the display thinks to be the closest from red, green and blue (what the display can achieve). Being chromatic coordinates, they are defined in the $xyY$ colorspace, using the following transformations:

\begin{equation}
  \begin{cases}
    x=\frac{X}{X+Y+Z}\\[10pt]
    y=\frac{Y}{X+Y+Z}\\[10pt]
    Y=Y
  \end{cases}
  \label{eq:XYZ2xyY}
\end{equation}

In the $xyY$ colorspace, the coordinate $z$ of $Z$ is: $z=1-(x+y)$: it can be computed knowing the values of $x$ and $y$, so only the values of $x$ and $y$ are required to locate a given color. These are called the chromaticities values. The $Y$ tristimulus has also to be known (the $XYZ$ colorspace was designed to that the $Y$ coordinate is a measure of the brightess of a color) in order to perform the opposite conversion:


\begin{equation}
  \begin{cases}
    X=x\frac{Y}{y}\\[10pt]
    Y=Y\\[10pt]
    Z=(1-x-y)\frac{Y}{y}
  \end{cases}
  \label{eq:xyY2XYZ}
\end{equation}

Suppose the $x$ and $y$ chromaticities of red, green and blue are known for a given display:

\begin{itemize}
\item ($x_{r}$, $y_{r}$) for red. 
\item ($x_{g}$, $y_{g}$) for green. 
\item ($x_{b}$, $y_{b}$) for blue.
\end{itemize}

$x_{r}$, $x_{g}$ and $x_{b}$ are the coefficients by which the values of $R$, $G$ and $B$ are multiplied in order to obtain the value of $X$ for a given $[R, G, B]$ color:

\begin{equation}
  X=Rx_{r}+Gx_{g}+Bx_{b}
\end{equation}

From the chromaticies of red, green and blue, we can therefore build a first $[{\bf K}]$ matrix intended at converting $[R, G, B]$ values into $[X, Y, Z]$ values:

\begin{equation}
  \begin{bmatrix}
    \mathbf{K}
  \end{bmatrix}
  =
  \begin{bmatrix}
    x_{r}&x_{g}&x_{b}\\[10pt]
    y_{r}&y_{g}&y_{b}\\[10pt]
    z_{r}&z_{g}&z_{b}
  \end{bmatrix}
  \label{eq:matrixK}
\end{equation}

Unfortunately, this matrix $[{\bf K}]$ (or rather its opposite) is not what we are looking for, since we want to work with normalized values: $R$, $G$ and $B$ values have to be numerically specified in the $[0,1]$ interval, and the same has to be true for $Y$ (brightness of the display, normalized by its maximum value). Using the raw $[{\bf K}]$ matrix, or instead the $[{\bf K}]^{-1}$ matrix in order to convert from $(X, Y, Z)$ to $(R, G, B)$, does not ensure that the values of $R$, $G$ and $B$ will end in the $[0,1]$ range, even when $Y \in [0,1]$. Let us call $[{\bf M}]$ the $(X, Y, Z)$ to $(R, G, B)$ transformation matrix:

\begin{equation}
  \begin{bmatrix}
    R\\
    G\\
    B
  \end{bmatrix}
  =
  \begin{bmatrix}
    \mathbf{M}
  \end{bmatrix}
  \begin{bmatrix}
    X\\
    Y\\
    Z
  \end{bmatrix}
  \label{eq:RGB2XYZ}
\end{equation}

with all $Y$, $R$, $G$ and $B$ coordinates in the $[0,1]$ range.

Matrix $[{\bf M}]$ can be obtained from matrix $[{\bf K}]^{-1}$ using a simple normalization: all we have to do is to scale the red, green and blue chromaticities by 3 (unknown at this stage) scaling factors $\alpha$, $\beta$ and $\gamma$ in order to obtain the $[{\bf M}]$ matrix: let us call $[{\bf G}]$ the diagonal matrix built from these 3 factors:

\begin{equation}
  \begin{bmatrix}
    \mathbf{G}
  \end{bmatrix}
  =
  \begin{bmatrix}
    \alpha&0&0\\
    0&\beta&0\\
    0&0&\gamma
  \end{bmatrix}
  \label{eq:matrixG}
\end{equation}


\begin{equation}
  \begin{bmatrix}
    \mathbf{M}
  \end{bmatrix}
  =
  \begin{bmatrix}
    \mathbf{G}
  \end{bmatrix}
  \begin{bmatrix}
    \mathbf{K}
  \end{bmatrix}^{-1}
  \label{eq:matrixM}
\end{equation}

In order to obtain the values of scaling coefficients $\alpha$, $\beta$ and $\gamma$, we need some additional information. This is provided by chromaticity coordinates of the white signal (the brightest and ``whitest'' color the display is capable of): let note then $x_{w}$, $y_{w}$ and $z_{w}=1-(x_{w}+y_{w})$. The corresponding $(X_{w}, Y_{w}, Z_{w})$ coordinates are obtained using transformation relations \ref{eq:xyY2XYZ}: $X_{w}=x_{w}\frac{Y_{w}}{y_{w}}$, $Y_{w}=y_{w}\frac{Y_{w}}{y_{w}}$ and $Z_{w}=z_{w}\frac{Y_{w}}{y_{w}}$; since we still want $Y_{w} \in [0,1]$, and the white color is the brightest of what the display can do {\it for the standard illuminant of choice}, we have $Y_{w}=1$. Finally, the vector of coordinates for the white, in the $XYZ$ colorspace, is:

\begin{equation}
  \begin{bmatrix}
    \mathbf{W}
  \end{bmatrix}
  =
  \begin{bmatrix}
    \frac{x_{w}}{y_{w}}\\[5pt]
    1\\[5pt]
    \frac{1-(x_{w}+y_{w})}{y_{w}}
  \end{bmatrix}  
  \label{eq:vectorW}
\end{equation}
    
The corresponding coordinates of white in the $RGB$ colorspace are:

\begin{equation}
  \begin{bmatrix}
    \mathbf{F}
  \end{bmatrix}
  =
  \begin{bmatrix}
    1\\[5pt]
    1\\[5pt]
    1
  \end{bmatrix}  
  \label{eq:vectorF}
\end{equation}

Using relation \ref{eq:XYZ2RGB}:

\begin{equation}
  \begin{bmatrix}
    \mathbf{M}
  \end{bmatrix}
  \begin{bmatrix}
    \mathbf{W}
  \end{bmatrix}=
  \begin{bmatrix}
    \mathbf{F}
  \end{bmatrix}
  \label{eq:MWF}
\end{equation}

Replacing $[{\bf M}]$ from eq. \ref{eq:matrixM}:

\begin{equation}
  \begin{bmatrix}
    \mathbf{G}
  \end{bmatrix}
  \begin{bmatrix}
    \mathbf{K}
  \end{bmatrix}^{-1}
  \begin{bmatrix}
    \mathbf{W}
  \end{bmatrix}=
  \begin{bmatrix}
    \mathbf{F}
  \end{bmatrix}
  \label{eq:GinvKWF}
\end{equation}

Multiplying both sides by $[{\bf G}]^{-1}$:

\begin{equation}
  \begin{bmatrix}
    \mathbf{G}
  \end{bmatrix}^{-1}
  \begin{bmatrix}
    \mathbf{F}
  \end{bmatrix}=
  \begin{bmatrix}
    \mathbf{K}
  \end{bmatrix}^{-1}
  \begin{bmatrix}
    \mathbf{W}
  \end{bmatrix}
  \label{eq:invGFinvKW}
\end{equation}

Matrix $[{\bf G}]$ being diagonal, its inverse is also diagonal: 
\begin{equation}
  \begin{bmatrix}
    \mathbf{G}
  \end{bmatrix}^{-1}
  =
  \begin{bmatrix}
    \frac{1}{\alpha}&0&0\\
    0&\frac{1}{\beta}&0\\
    0&0&\frac{1}{\gamma}
  \end{bmatrix}
  \label{eq:matrix_invG}
\end{equation}


Since $[{\bf G}]^{-1}[{\bf F}]$ is the vector $[1/\alpha, 1/\beta, 1/\gamma]$ containing the unknowns, we finally end up with:

\begin{equation}
  \begin{bmatrix}
    1/\alpha\\
    1/\beta\\
    1/\gamma
  \end{bmatrix}
  =
  \begin{bmatrix}
    \mathbf{K}
  \end{bmatrix}^{-1}
  \begin{bmatrix}
    \mathbf{W}
  \end{bmatrix}
  \label{eq:VinvKW}
\end{equation}

Once these coefficients have been computed, they can be used in order to normalize matrix $[{\bf M}]$ (eq. \ref{eq:matrixM}):

\begin{equation}
  \begin{bmatrix}
    \mathbf{K}
  \end{bmatrix}^{-1}
  =
  \begin{bmatrix}
    y_{g}z_{b}-y_{b}z_{g}&x_{b}z_{g}-x_{g}z_{b}&x_{g}y_{b}-x_{b}y_{g}\\
    y_{b}z_{r}-y_{r}z_{b}&x_{r}z_{b}-x_{b}z_{r}&x_{b}y_{r}-x_{r}y_{b}\\
    y_{r}z_{g}-y_{g}z_{r}&x_{g}z_{r}-x_{r}z_{g}&x_{r}y_{g}-x_{g}y_{r}
  \end{bmatrix}
  \label{eq:matrix_invK}
\end{equation}

\begin{equation}
  \begin{bmatrix}
    1/\alpha\\
    1/\beta\\
    1/\gamma
  \end{bmatrix}
  =
  \begin{bmatrix}
    \frac{(y_{g}z_{b}-y_{b}z_{g})x_{w}+(x_{b}z_{g}-x_{g}z_{b})y_{w}+(x_{g}y_{b}-x_{b}y_{g})z_{w}}{y_{w}}\\
    \frac{(y_{b}z_{r}-y_{r}z_{b})x_{x}+(x_{r}z_{b}-x_{b}z_{r})y_{w}+(x_{b}y_{r}-x_{r}y_{b})z_{w}}{y_{w}}\\
    \frac{(y_{r}z_{g}-y_{g}z_{r})x_{x}+(x_{g}z_{r}-x_{r}z_{g})y_{w}+(x_{r}y_{g}-x_{g}y_{r})z_{w}}{y_{w}}
  \end{bmatrix}  
  \label{eq:scaling_factors}
\end{equation}


The XYZ to RGB transformation matrix $[{\bf M}]$ is therefore given by:

\begin{equation}
  \begin{bmatrix}
    \mathbf{M}
  \end{bmatrix}
  =
  \begin{bmatrix}
    \alpha(y_{g}z_{b}-y_{b}z_{g})&\alpha(x_{b}z_{g}-x_{g}z_{b})&\alpha(x_{g}y_{b}-x_{b}y_{g})\\
    \beta(y_{b}z_{r}-y_{r}z_{b})&\beta(x_{r}z_{b}-x_{b}z_{r})&\beta(x_{b}y_{r}-x_{r}y_{b})\\
    \gamma(y_{r}z_{g}-y_{g}z_{r})&\gamma(x_{g}z_{r}-x_{r}z_{g})&\gamma(x_{r}y_{g}-x_{g}y_{r})
  \end{bmatrix}
  \label{eq:final_matrixM}
\end{equation}

For instance, we have for the Rec709 (or sRGB) colorsystem:
\begin{itemize}
\item $x_{r}=0.64$, $y_{r}=0.33$, and $z_{r}=1-(x_{r}+y_{r})=0.03$
\item $x_{g}=0.30$, $y_{g}=0.60$, and $z_{g}=1-(x_{g}+y_{g})=0.10$
\item $x_{b}=0.15$, $y_{b}=0.06$, and $z_{b}=1-(x_{b}+y_{b})=0.79$
\item $x_{w}=0.3127$, $y_{w}=3290$ and $z_{w}=1-(x_{w}+y_{w})=0.3583$
\end{itemize}

Using these reference values, the XYZ to RGB transformation matrix is:

\begin{equation}
  \begin{bmatrix}
    \mathbf{M}
  \end{bmatrix}
  =
  \begin{bmatrix}
    3.2409&-1.5374&-0.4986\\
    -0.9692&1.8760&0.041555\\
    0.055630&-0.2040&1.0570
  \end{bmatrix}
  \label{eq:MRec709}
\end{equation}






\bibliographystyle{unsrt}
\bibliography{biblio}

\end{document}
