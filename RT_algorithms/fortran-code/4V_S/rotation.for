c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c      
      subroutine rotation(dim,u,alpha,axe,v)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute coordinates of vector "v", the transform of vector "u"
c     by a rotation of angle "alpha" around an axe
c
c     Input:
c       + dim: dimension of vectors
c       + u: (ux,uy,uz) coordinates of vector "u"
c       + alpha: rotation angle
c       + axe: (ax,ay,az) coordinates of the elementary vector definig the direction of the rotation axe
c     
c     Output:
c       + v: (vx,vy,vz) coordinates of vector "v"
c
c     I/O
      integer dim
      double precision u(1:Ndim_mx)
      double precision alpha
      double precision axe(1:Ndim_mx)
      double precision v(1:Ndim_mx)
c     temp
      double precision scalar
      double precision naxe(1:Ndim_mx)
      double precision M(1:Ndim_mx,1:Ndim_mx)
      double precision I3(1:Ndim_mx,1:Ndim_mx)
      double precision Maxe1(1:Ndim_mx,1:Ndim_mx)
      double precision Maxe2(1:Ndim_mx,1:Ndim_mx)
      double precision Mt1(1:Ndim_mx,1:Ndim_mx)
      double precision Mt2(1:Ndim_mx,1:Ndim_mx)
      double precision Mt3(1:Ndim_mx,1:Ndim_mx)
      double precision Mt4(1:Ndim_mx,1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine rotation'

c     checking inputs are understood
      call normalize_vector(dim,axe,naxe)
      call axe_matrix(naxe,Maxe1,Maxe2)
c     computation of rotation matrix M
      call identity_matrix(dim,I3)
      scalar=dcos(alpha)
      call matrix_scalar(dim,I3,scalar,Mt1)
      scalar=1.0D+0-dcos(alpha)
      call matrix_scalar(dim,Maxe1,scalar,Mt2)
      scalar=dsin(alpha)
      call matrix_scalar(dim,Maxe2,scalar,Mt3)
      call add_matrix(dim,Mt1,Mt2,Mt4)
      call add_matrix(dim,Mt4,Mt3,M)
c     computation of vector "v" coordinates
      call matrix_vector(dim,M,u,v)

      return
      end



      subroutine axe_matrix(axe,Maxe1,Maxe2)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute axe matrices "Maxe1" and "Maxe2"
c
c     Input:
c       + axe: coordinates of the rotation axe
c
c     Output:
c       + Maxe1, Maxe2: matrices used to compute the 3D rotation matrix
c
c     I/O
      double precision axe(1:Ndim_mx)
      double precision Maxe1(1:Ndim_mx,1:Ndim_mx)
      double precision Maxe2(1:Ndim_mx,1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine axe_matrix'

      Maxe1(1,1)=axe(1)**2.0D+0
      Maxe1(1,2)=axe(1)*axe(2)
      Maxe1(1,3)=axe(1)*axe(3)
      Maxe1(2,1)=axe(1)*axe(2)
      Maxe1(2,2)=axe(2)**2.0D+0
      Maxe1(2,3)=axe(2)*axe(3)
      Maxe1(3,1)=axe(1)*axe(3)
      Maxe1(3,2)=axe(2)*axe(3)
      Maxe1(3,3)=axe(3)**2.0D+0

      Maxe2(1,1)=0.0D+0
      Maxe2(1,2)=-axe(3)
      Maxe2(1,3)=axe(2)
      Maxe2(2,1)=axe(3)
      Maxe2(2,2)=0.0D+0
      Maxe2(2,3)=-axe(1)
      Maxe2(3,1)=-axe(2)
      Maxe2(3,2)=axe(1)
      Maxe2(3,3)=0.0D+0

      return
      end



      subroutine vector_j1(dim,u,j1)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute vector "j1", the vector used for rotations around axe "u"
c
c     Input:
c       + dim: dimension of vectors
c       + u: rotation axe
c
c     Output:
c       + j1: vector j1
c
c     I/O
      integer dim
      double precision u(1:Ndim_mx)
      double precision j1(1:Ndim_mx)
c     temp
      double precision us(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine vector_j1'

c     get "theta" and "phi", the angles that define direction "u"
      call cartesian2spherical(u,us)
c     FIRST ROTATION of vector "u", by an angle "alpha1", around axe "j1" -> vector "t"
      j1(1)=-dsin(us(3))
      j1(2)=dcos(us(3))
      j1(3)=0.0D+0

      return
      end
