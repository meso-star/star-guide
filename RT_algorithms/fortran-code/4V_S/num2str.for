c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine num2str(num,str,err_code)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to convert an integer to a character string
c
c     Input:
c       + num: integer
c     
c     Output:
c       + str: character string
c       + err_code: false means conversion is OK; true means "num" was not recognized.
c
c     I/O
      integer num
      character*(Nchar_mx) str
      logical err_code
c     temp
      integer absnum,sign
      character*(Nchar_mx) f
c     label
      character*(Nchar_mx) label
      label='subroutine num2str'

      if (num.lt.0) then
         sign=-1
      else
         sign=1
      endif
      absnum=abs(num)
      
      err_code=.false.
      if ((absnum.ge.0).and.(absnum.lt.10)) then
         write(str,101) absnum
      else if ((absnum.ge.10).and.(absnum.lt.100)) then
         write(str,102) absnum
      else if ((absnum.ge.100).and.(absnum.lt.1000)) then
         write(str,103) absnum
      else if ((absnum.ge.1000).and.(absnum.lt.10000)) then
         write(str,104) absnum
      else if ((absnum.ge.10000).and.(absnum.lt.100000)) then
         write(str,105) absnum
      else if ((absnum.ge.100000).and.(absnum.lt.1000000)) then
         write(str,106) absnum
      else if ((absnum.ge.1000000).and.(absnum.lt.10000000)) then
         write(str,107) absnum
      else if ((absnum.ge.10000000).and.(absnum.lt.100000000)) then
         write(str,108) absnum
      else if ((absnum.ge.100000000).and.(absnum.lt.1000000000)) then
         write(str,109) absnum
      else
         err_code=.true.
         goto 666
      endif

      if (sign.eq.-1) then
         str="-"//trim(str)
      endif
      
 666  continue
      return
      end
