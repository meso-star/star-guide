c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine read_data(datafile,dim,center,radius,ks,gHG,Nevent)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read the specified input data file
c     
c     Input:
c       + datafile: file to read
c       + dim: dimension of space
c     
c     Output:
c       + center of the sphere [m, m, m]
c       + radius: radius of the sphere [m]
c       + ks: scattering coefficient [inv. m]
c       + gHG: Henyey-Greenstein phase function asymetry parameter
c       + Nevent: number of statistical realizations
c     
c     I/O
      character*(Nchar_mx) datafile
      integer dim
      double precision center(1:Ndim_mx)
      double precision radius
      double precision ks
      double precision gHG
      integer Nevent
c     temp
      integer i,ios
c     label
      character*(Nchar_mx) label
      label='subroutine read_data'

      open(11,file=trim(datafile),iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(datafile)
         stop
      else
         write(*,*) 'Reading file: ',trim(datafile)
         do i=1,3
            read(11,*)
         enddo                  ! i
         do i=1,dim
            read(11,*) center(i)
         enddo
         read(11,*)
         read(11,*) radius
         read(11,*)
         read(11,*) ks
         read(11,*)
         read(11,*) gHG
         read(11,*)
         read(11,*) Nevent
      endif                     ! ios.ne.0
      close(11)
      
      if (radius.le.0.0D+0) then
         call error(label)
         write(*,*) 'radius=',radius
         write(*,*) 'should be positive'
         stop
      endif
      if (ks.le.0.0D+0) then
         call error(label)
         write(*,*) 'ks=',ks
         write(*,*) 'should be positive'
         stop
      endif
      if ((gHG.lt.-1.0D+0).or.(gHG.gt.1.0D+0)) then
         call error(label)
         write(*,*) 'gHG=',gHG
         write(*,*) 'should be in the [-1,1] range'
         stop
      endif
      if (Nevent.le.0.0D+0) then
         call error(label)
         write(*,*) 'Nevent=',Nevent
         write(*,*) 'should be positive'
         stop
      endif
      write(*,*) 'No inconsistency detected'
      
      return
      end
