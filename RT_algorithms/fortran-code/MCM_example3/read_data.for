c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine read_data(data_file,alpha,beta,f10,f20,xmin,xmax,Nevent)
      implicit none
      include 'max.inc'
c
c     Purpose: to read the "data.in" input file
c
c     Input:
c       + data_file: name of the data file to read
c
c     Output:
c       + alpha: value of parameter alpha
c       + beta: value of parameter beta
c       + f10: value of f1(x=0)
c       + f20: value of f2(x=0)
c       + xmin: minimum value of x
c       + xmax: maximum value of x
c       + Nevent: number of MC realizations
c
c     I/O
      character*(Nchar_mx) data_file
      double precision alpha,beta
      double precision f10,f20
      double precision xmin,xmax
      integer Nevent
c     temp
      integer i,ios
c     label
      character*(Nchar_mx) label
      label='subroutine read_data'

      open(10,file=trim(data_file),status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'File could not be found:'
         write(*,*) trim(data_file)
         stop
      else
         do i=1,4
            read(10,*)
         enddo                  ! i
         read(10,*) alpha
         read(10,*)
         read(10,*) beta
         read(10,*)
         read(10,*) f10
         read(10,*)
         read(10,*) f20
         read(10,*)
         read(10,*) xmin
         read(10,*)
         read(10,*) xmax
         read(10,*)
         read(10,*)
         read(10,*) Nevent
         if (xmax.lt.xmin) then
            call error(label)
            write(*,*) 'xmax=',xmax
            write(*,*) 'should be > xmin=',xmin
            stop
         endif
         if (Nevent.le.0) then
            call error(label)
            write(*,*) 'Nevent=',Nevent
            write(*,*) 'should be greater than 1'
            stop
         endif
      endif
      close(10)

      return
      end
