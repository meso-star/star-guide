c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      program main
      implicit none
      include 'max.inc'
c     
c     Purpose: to evaluate the (discrete) sum of first integers using the Monte-Carlo method
c     
c     Variables
      integer Nterms,iterm
      integer Niter,iter
      double precision alpha
      double precision result
      integer Nevent,event
      integer iseed(4),n
      double precision, dimension(:), allocatable :: r
      integer DeAllocateStatus
      integer i
      double precision weight
      double precision sum,sum2
      double precision mean,variance,std_dev
      double precision time0,time1,delta_t0,delta_t1
      character*(Nchar_mx) time_file
c     label
      character*(Nchar_mx) label
      label='program main'

c     --------------------------------------------------------
c     INPUT DATA

c     Number of terms in the discrete sum on the first iteration
      Nterms=100
c     Number of iterations (Nterms x alpha on each iteration)
      Niter=26
c     value of alpha (Nterms x alpha on eah iteration)
      alpha=1.50D+0
c     Number of statistical events
      Nevent=10000
c     log file
      time_file='./CPU_time.txt'
      
c     END OF INPUT DATA
c     --------------------------------------------------------

      write(*,*) 'Computation...'
      iseed(1)=0
      iseed(2)=1
      iseed(3)=2
      iseed(4)=3
      n=1
      allocate(r(n))
      open(11,file=trim(time_file))
c     Iterative loop
      do iter=1,Niter
c     Analytical result:
         call cpu_time(time0)
         result=0.0D+0
         do iterm=1,Nterms
            result=result+dble(iterm)
         enddo                  ! iterm
         call cpu_time(time1)
         delta_t0=time1-time0
      write(*,*) 'Theoretical result for Nterms=',Nterms,' :',result
c     Monte-Carlo Method:
         call cpu_time(time0)
         sum=0.0D+0             ! sum of weights: initialization
         sum2=0.0D+0            ! sum of weights squares: initialization
         do event=1,Nevent
            call dlaruv(iseed,n,r) ! generate a uniform random number 'r(1)' over the [0,1[ range
            i=int(r(1)*Nterms+1)
c     Debug
            if ((i.lt.1).or.(i.gt.Nterms)) then
               call error(label)
               write(*,*) 'i=',i
               stop
            endif
c     Debug
            weight=dble(i)*dble(Nterms)
            sum=sum+weight
            sum2=sum2+weight**2.0D+0
         enddo                  ! event
         call statistics(Nevent,sum,sum2,mean,variance,std_dev)
         call cpu_time(time1)
         delta_t1=time1-time0
         write(*,*) 'MC result:',mean,' +/-',std_dev
         write(11,*) Nterms,delta_t0,delta_t1

         Nterms=int(Nterms*alpha)
      enddo ! iter
      close(11)
      write(*,*) 'File was generated: ',trim(time_file)
      
      deallocate(r,stat=DeAllocateStatus)
      
      end
