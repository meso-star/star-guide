c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      program main
      implicit none
      include 'max.inc'
c     
c     Purpose: to test the LAPACK random number generator
c     
c     Variables
      integer n,iseed(4)
      double precision, dimension(:), allocatable :: x
      integer DeAllocateStatus
      integer Nevent,event
      double precision weight
      double precision sum
      double precision sum2
      double precision mean
      double precision variance
      double precision std_dev
c     label
      character*(Nchar_mx) label
      label='program main'

      iseed(1)=0
      iseed(2)=1
      iseed(3)=2
      iseed(4)=3
      n=1
      allocate(x(n))

      sum=0.0D+0
      sum2=0.0D+0
      Nevent=10000
      do event=1,Nevent
         call dlaruv(iseed,n,x)
         weight=x(1)
         sum=sum+weight
         sum2=sum2+weight**2.0D+0
      enddo                     ! event
      call statistics(Nevent,sum,sum2,mean,variance,std_dev)
      write(*,*) 'mean=',mean,' +:-',std_dev
      
      deallocate(x,stat=DeAllocateStatus)
      
      end
