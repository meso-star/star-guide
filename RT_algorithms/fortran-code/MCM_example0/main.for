c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      program main
      implicit none
      include 'max.inc'
c     
c     Purpose: to evaluate a discrete sum using the Monte-Carlo method
c     
c     Variables
      integer Nterms,iterm
      double precision p(1:Nterms_mx)
      double precision f(1:Nterms_mx)
      double precision c(0:Nterms_mx)
      double precision result
      integer iseed(4),n
      double precision, dimension(:), allocatable :: r
      integer DeAllocateStatus
      integer Nevent,event
      logical ifound
      integer i
      double precision weight
      double precision sum,sum2
      double precision mean,variance,std_dev
c     label
      character*(Nchar_mx) label
      label='program main'

c     --------------------------------------------------------
c     INPUT DATA

c     Number of terms in the discrete sum p(i)f(i)
      Nterms=10
c     Values of the probability set; the sum should be equal to 1
      p(1)=0.12D+0
      p(2)=0.08D+0
      p(3)=0.06D+0
      p(4)=0.14D+0
      p(5)=0.10D+0
      p(6)=0.10D+0
      p(7)=0.04D+0
      p(8)=0.16D+0
      p(9)=0.02D+0
      p(10)=0.18D+0
c     Values of the function
      f(1)=3.0D+0
      f(2)=10.0D+0
      f(3)=8.0D+0
      f(4)=5.0D+0
      f(5)=6.0D+0
      f(6)=4.0D+0
      f(7)=7.0D+0
      f(8)=2.0D+0
      f(9)=9.0D+0
      f(10)=1.0D+0
c     Number of statistical events
      Nevent=1000000
      
c     END OF INPUT DATA
c     --------------------------------------------------------

      if (Nterms.gt.Nterms_mx) then
         call error(label)
         write(*,*) 'Nterms=',Nterms
         write(*,*) 'should be lower or equal to:',Nterms_mx
         stop
      endif
      do iterm=1,Nterms
         if (p(iterm).lt.0.0D+0) then
            call error(label)
            write(*,*) 'p(',iterm,')=',p(iterm)
            write(*,*) 'should be positive or null'
            stop
         endif
      enddo                     ! iterm

      c(0)=0.0D+0
      do iterm=1,Nterms
         c(iterm)=c(iterm-1)+p(iterm)
      enddo                     ! iterm
      if (c(Nterms).ne.1.0D+0) then
         do iterm=1,Nterms
            c(iterm)=c(iterm)/c(Nterms)
         enddo                  ! iterm
      endif                     ! c(Nterms).ne.1

c     Analytical result:
      result=0.0D+0
      do iterm=1,Nterms
         result=result+p(iterm)*f(iterm)
      enddo                     ! iterm
      write(*,*) 'Theoretical result:',result
      
c     Monte-Carlo Method:
      iseed(1)=0
      iseed(2)=1
      iseed(3)=2
      iseed(4)=3
      n=1
      allocate(r(n))
      sum=0.0D+0                ! sum of weights: initialization
      sum2=0.0D+0               ! sum of weights squares: initialization
      do event=1,Nevent
         call dlaruv(iseed,n,r) ! generate a uniform random number 'r(1)' over the [0,1[ range
c     identify the associated interval
         ifound=.false.
         do iterm=1,Nterms
            if ((r(1).ge.c(iterm-1)).and.(r(1).le.c(iterm))) then
               ifound=.true.
               i=iterm
               goto 111
            endif
         enddo                  ! iterm
 111     continue
         if (.not.ifound) then
            call error(label)
            write(*,*) 'Interval could not be located'
            write(*,*) 'generated random number r=',r(1)
            write(*,*) 'cumulative probability set:'
            do iterm=0,Nterms
               write(*,*) 'c(',iterm,')=',c(iterm)
            enddo               ! iterm
            stop
         endif
         weight=f(i)
         sum=sum+weight
         sum2=sum2+weight**2.0D+0
      enddo                     ! event
      call statistics(Nevent,sum,sum2,mean,variance,std_dev)
      write(*,*) 'mean=',mean,' +/-',std_dev

      deallocate(r,stat=DeAllocateStatus)
      
      end
