c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine statistics(Nevent,sum,sum2,mean,variance,std_dev)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the variance and standard deviation of a N-sample
c
c     Input:
c       + Nevent: number of statistical events
c       + sum: sum of weights
c       + sum2: sum of weights squares
c
c     Output:
c       + mean: average value of the N-sample
c       + variance: variance of the N-sample
c       + std_dev: standard deviation of the N-sample
c
c     I/O
      integer Nevent
      double precision sum
      double precision sum2
      double precision mean
      double precision variance
      double precision std_dev
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine statistics'
      
      mean=sum/dble(Nevent)
      variance=dabs(sum2/dble(Nevent)-(sum/dble(Nevent))**2)
      std_dev=dsqrt(variance/dble(Nevent))
      
      return
      end
