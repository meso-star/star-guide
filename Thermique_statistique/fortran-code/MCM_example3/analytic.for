c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine analytic(f10,f20,alpha,beta,x,f1,f2)
      implicit none
      include 'max.inc'
c     
c     Purpose: to solve functions {f1(x), f2(x)} for the following system:
c     
c     d[f1(x)]/d[x]=-alpha.f1(x)+beta.f2(x)
c     d[f2(x)]/d[x]=+beta.f1(x)-alpha.f2(x)
c     
c     and initial conditions:
c     
c     f1(x=0)=f10
c     f2(x=0)=f20
c     
c     Input:
c       + f10: value of f1(x=0)
c       + f20: value of f2(x=0)
c       + alpha: value of parameter alpha
c       + beta: value of parameter beta
c       + x: value of x
c     
c     Output:
c       + f1: value of f1(x)
c       + f2: value of f2(x)
c     
c     I/O
      double precision f10
      double precision f20
      double precision alpha
      double precision beta
      double precision x
      double precision f1
      double precision f2
c     temp
      double precision c1,c2
c     label
      character*(Nchar_mx) label
      label='subroutine analytic'

      c1=(f10-f20)/2.0D+0
      c2=(f10+f20)/2.0D+0

      f1=c1*dexp(-(alpha+beta)*x)+c2*dexp(-(alpha-beta)*x)
      f2=-c1*dexp(-(alpha+beta)*x)+c2*dexp(-(alpha-beta)*x)
      
      return
      end
