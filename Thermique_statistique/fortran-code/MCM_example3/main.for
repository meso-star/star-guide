c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      program main
      implicit none
      include 'max.inc'
c     
c     Purpose: to solve a linear system A.X=B using the Monte-Carlo method
c     
c     Variables
      integer Nevent,event
      integer iseed(4),n
      double precision, dimension(:), allocatable :: r
      integer DeAllocateStatus
      double precision weight
      double precision sum,sum2
      double precision mean,variance,std_dev
      character*(Nchar_mx) data_file
      double precision f10,f20
      double precision alpha,beta
      double precision xmin,xmax,x
      integer Npos,ipos
      double precision f1,f2
      double precision sigma,pdf_sigma
      integer idx,idx0,idx1
      logical keep_running
      double precision c,x0
      double precision m1,m2,s1,s2
      character*(Nchar_mx) resfile
c     label
      character*(Nchar_mx) label
      label='program main'

      write(*,*) 'Reading data...'
      data_file='./data.in'
      call read_data(data_file,alpha,beta,f10,f20,xmin,xmax,Nevent)

      write(*,*) 'Analytical computation...'
      resfile='./analytical_functions.txt'
      open(11,file=trim(resfile))
      Npos=100
      do ipos=1,Npos
         x=xmin+(xmax-xmin)/dble(Npos-1)*dble(ipos-1)
         call analytic(f10,f20,alpha,beta,x,f1,f2)
         write(11,*) x,f1,f2
      enddo                     ! ipos
      close(11)

      
      write(*,*) 'MC computation...'
      resfile='./MC_functions.txt'
      open(12,file=trim(resfile))
      iseed(1)=0
      iseed(2)=1
      iseed(3)=2
      iseed(4)=3
      n=1
      allocate(r(n))
      Npos=10
      do ipos=1,Npos
         x=xmin+(xmax-xmin)/dble(Npos-1)*dble(ipos-1)
         do idx=1,2
c     idx: index of the function to solve
            sum=0.0D+0
            sum2=0.0D+0
            do event=1,Nevent
               x0=0.0D+0
               c=1.0D+0
               idx0=idx
               keep_running=.true.
               do while (keep_running)
                  call dlaruv(iseed,n,r) ! generate a uniform random number 'r(1)' over the [0,1[ range
                  call sample_length_unlimited(r(1),alpha,sigma,pdf_sigma)
                  x0=x0+sigma
                  if (x0.ge.x) then
                     if (idx0.eq.1) then
                        weight=f10
                     else if (idx0.eq.2) then
                        weight=f20
                     else
                        call error(label)
                        write(*,*) 'idx0=',idx0
                        stop
                     endif      ! idx0
                     keep_running=.false.
                  else
                     c=c*beta/alpha
                     if (idx0.eq.1) then
                        idx1=2
                     else
                        idx1=1
                     endif      ! idx0
                     idx0=idx1
                  endif         ! x0 > x
               enddo            ! while (keep_running)
               weight=weight*c
               sum=sum+weight
               sum2=sum2+weight**2.0D+0
            enddo               ! event
            call statistics(Nevent,sum,sum2,mean,variance,std_dev)
            if (idx.eq.1) then
               m1=mean
               s1=std_dev
            else
               m2=mean
               s2=std_dev
            endif
         enddo                  ! idx
         write(12,*) x,m1,s1,m2,s2
      enddo                     ! ipos
      close(12)
      write(*,*) 'Results file: ',trim(resfile)
      deallocate(r,stat=DeAllocateStatus)
         
      end
