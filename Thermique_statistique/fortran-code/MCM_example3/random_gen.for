c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine sample_length_unlimited(r,k,length,pdf_l)
      implicit none
      include 'max.inc'
c
c     Purpose: to generate a length in the [0,+infty) range according to:
c     pdf(length)=k*exp(-k*length)
c
c     Input:
c       + r: uniform random number in [0,1[
c       + k: optical coefficient (absorption, scattering or total extinction) (m^-1)
c
c     Output:
c       + length: random length chosen over the [0,+infty) range according to pdf(length)
c       + pdf_l: value of pdf(length)
c
c     I/O
      double precision r
      double precision k
      double precision length
      double precision pdf_l
c     temp
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine sample_length_unlimited'

      if (k.le.0.0D+0) then
         call error(label)
         write(*,*) 'Bad input k=',k
         stop
      endif

      length=-1.0D+0/k*dlog(r)
      pdf_l=k*dexp(-k*length)

      return
      end
