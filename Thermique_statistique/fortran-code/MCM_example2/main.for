c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      program main
      implicit none
      include 'max.inc'
c     
c     Purpose: to solve a linear system A.X=B using the Monte-Carlo method
c     
c     Variables
      integer Nevent,event
      integer iseed(4),n
      double precision, dimension(:), allocatable :: r
      integer DeAllocateStatus
      integer i,j
      double precision weight
      double precision sum,sum2
      double precision mean,variance,std_dev
      character*(Nchar_mx) data_file
      integer dim
      double precision, dimension(:,:), allocatable :: A
      double precision, dimension(:), allocatable :: B
      double precision, dimension(:,:), allocatable :: inv_A
      double precision, dimension(:), allocatable :: X
      double precision, dimension(:,:), allocatable :: alpha
      double precision, dimension(:), allocatable :: beta
      double precision, dimension(:,:), allocatable :: cdf
      integer idx,i0,line_index
      logical over,i0_found
c     label
      character*(Nchar_mx) label
      label='program main'

      write(*,*) 'Reading data...'
      data_file='./data.in'
      call read_data(data_file,dim,Nevent)
      allocate(A(dim,dim))
      allocate(B(dim))
      allocate(inv_A(dim,dim))
      allocate(X(dim))
      call read_matrices(dim,A,B)
c     Solving using a classical method
      write(*,*) 'Solving using matrix inversion...'
      call inverse_matrix(dim,.true.,A,inv_A)
      call dgemm('n','n',dim,1,dim,1.0D+0,inv_A,
     &     dim,B,dim,0.0D+0,X,dim)
      do i=1,dim
         write(*,*) 'X[',i,']=',X(i)
      enddo                     ! i

c     Solving using a MC method
      write(*,*) 'Solving using MC...'
      iseed(1)=0
      iseed(2)=1
      iseed(3)=2
      iseed(4)=3
      n=1
      allocate(r(n))
      allocate(alpha(dim,dim))
      allocate(beta(dim))
c     initialization
      do i=1,dim
         do j=1,dim
            if (i.eq.j) then
               alpha(i,j)=A(i,j)+1.0D+0
            else
               alpha(i,j)=A(i,j)
            endif
         enddo                  ! j
         beta(i)=-B(i)
      enddo                     ! i
      deallocate(A,stat=DeAllocateStatus)
      deallocate(B,stat=DeAllocateStatus)
      deallocate(inv_A,stat=DeAllocateStatus)
      deallocate(X,stat=DeAllocateStatus)
c     precompute cumulative for each line
      allocate(cdf(1:dim,0:dim+1))
      do i=1,dim
         cdf(i,0)=0.0D+0
         do j=1,dim
            if ((alpha(i,j).lt.0.0D+0).or.
     &           (alpha(i,j).gt.1.0D+0)) then
               call error(label)
               write(*,*) 'alpha(',i,',',j,')=',alpha(i,j)
               write(*,*) 'should be in the [0,1] range'
               stop
            endif
            cdf(i,j)=cdf(i,j-1)+alpha(i,j)
         enddo                  ! j
         if ((beta(i).lt.0.0D+0).or.(beta(i).gt.1.0D+0)) then
            call error(label)
            write(*,*) 'beta(',i,')=',beta(i)
            write(*,*) 'should be in the [0,1] range'
            stop
         endif
         cdf(i,dim+1)=cdf(i,dim)+beta(i)
         if ((cdf(i,dim+1).lt.0.0D+0).or.(cdf(i,dim+1).gt.1.0D+0)) then
            call error(label)
            write(*,*) 'cdf(',i,',',dim+1,')=',cdf(i,dim+1)
            write(*,*) 'should be in the [0,1] range'
            stop
         endif
      enddo                     ! i

c     MCM
      do line_index=1,dim
         sum=0.0D+0
         sum2=0.0D+0
         do event=1,Nevent
            idx=line_index
            over=.false.
            do while (.not.over)
               call dlaruv(iseed,n,r) ! generate a uniform random number 'r(1)' over the [0,1[ range
               i0_found=.false.
               do i=1,dim+1
                  if ((r(1).ge.cdf(idx,i-1)).and.(r(1).le.cdf(idx,i))) then
                     i0_found=.true.
                     i0=i
                     goto 111
                  endif
               enddo            ! i
 111           continue
               if (.not.i0_found) then
                  weight=0.0D+0
                  over=.true.
               else
                  if (i0.eq.dim+1) then
                     weight=1.0D+0
                     over=.true.
                  else
                     idx=i0
                  endif         ! i=dim+1
               endif            ! i0_found
            enddo               ! while (.not.over)
            sum=sum+weight
            sum2=sum2+weight**2.0D+0
         enddo                  ! event
c     
         call statistics(Nevent,sum,sum2,mean,variance,std_dev)
         write(*,*) 'X[',line_index,']=',mean,' +/-',std_dev
      enddo                     ! line_index
      
      deallocate(alpha,stat=DeAllocateStatus)
      deallocate(beta,stat=DeAllocateStatus)
      deallocate(cdf,stat=DeAllocateStatus)
      deallocate(r,stat=DeAllocateStatus)
      
      end
