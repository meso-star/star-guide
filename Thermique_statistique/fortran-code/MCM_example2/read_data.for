c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine read_data(data_file,dim,Nevent)
      implicit none
      include 'max.inc'
c
c     Purpose: to read the "data.in" input file
c
c     Input:
c       + data_file: name of the data file to read
c
c     Output:
c       + dim: dimension of matrix A and vector B
c       + Nevent: number of MC realizations
c
c     I/O
      character*(Nchar_mx) data_file
      integer dim
      integer Nevent
c     temp
      integer i,j,ios,dimA,dimB
      character*(Nchar_mx) nlines_file
      character*(Nchar_mx) file
c     label
      character*(Nchar_mx) label
      label='subroutine read_data'

      open(10,file=trim(data_file),status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'File could not be found:'
         write(*,*) trim(data_file)
         stop
      else
         do i=1,3
            read(10,*)
         enddo ! i
         read(10,*) Nevent
         if (Nevent.le.0) then
            call error(label)
            write(*,*) 'Nevent=',Nevent
            write(*,*) 'should be greater than 1'
            stop
         endif
      endif
      close(10)

      nlines_file='./nlines'
      
      file='./matrix_A.in'
      call get_nlines(file,nlines_file,dimA)
      file='./vector_B.in'
      call get_nlines(file,nlines_file,dimB)

      if (dimA.eq.dimB) then
         dim=dimA
      else
         call error(label)
         write(*,*) 'size of matrix A=',dimA
         write(*,*) 'size of vector B=',dimB
         write(*,*) 'should be identical'
         stop
      endif

      if (dim.le.0) then
         call error(label)
         write(*,*) 'size of A and B=',dim
         write(*,*) 'should be positive'
         stop
      endif

      return
      end

