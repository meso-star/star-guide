c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine read_matrices(n,A,B)
      implicit none
      include 'max.inc'
c
c     Purpose: to read the "data.in" input file
c
c     Input:
c       + n: dimension of matrix A and vector B
c
c     Output:
c       + A: matrix A
c       + B: vector B
c
c     I/O
      integer n
      double precision, dimension(n,n), intent(out) :: A
      double precision, dimension(n), intent(out) :: B
c     temp
      character*(Nchar_mx) data_file
      integer i,j,ios
c     label
      character*(Nchar_mx) label
      label='subroutine read_matrices'

      data_file='./matrix_A.in'
      open(11,file=trim(data_file),status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'File could not be found:'
         write(*,*) trim(data_file)
         stop
      else
         do i=1,n
            read(11,*) (A(i,j),j=1,n)
         enddo                  ! i
      endif
      close(11)
      
      data_file='./vector_B.in'
      open(12,file=trim(data_file),status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'File could not be found:'
         write(*,*) trim(data_file)
         stop
      else
         do i=1,n
            read(12,*) B(i)
         enddo                  ! i
      endif
      close(12)

      return
      end

