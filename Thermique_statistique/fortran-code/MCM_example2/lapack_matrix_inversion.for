c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      subroutine inverse_matrix(dim,check_inversion,A,Ainv)
      implicit none
      include 'max.inc'
c     
c     Purpose: to invert a matrix
c     Warning: uses LAPACK; needs to be compiled with LD_FLAGS= -llapack -lblas 
c     
c     Input:
c       + dim: dimension of the matrix
c       + check_inversion: T if inversion result has to be displayed
c       + A: (n*n) square matrix
c     
c     Output:
c       + Ainv: (n*n) square matrix, invere of matrix A
c     
c     I/O
      integer dim
      logical check_inversion
      double precision, dimension(dim,dim), intent(in) :: A
      double precision, dimension(dim,dim), intent(out) :: Ainv
c     temp
      double precision, allocatable, dimension(:) :: work
      integer :: i,j, lwork
      integer info,lda,m
      integer, allocatable, dimension(:) :: ipiv
      integer DeAllocateStatus
      external DGETRF
      external DGETRI
      integer nl,nc
c     label
      integer strlen
      character*(Nchar_mx) label
      label='subroutine inverse_matrix'

      lda=dim
      lwork=dim*dim
      allocate(work(lwork))
      allocate(ipiv(dim))
c
c     DGETRF computes an LU factorization of a general M-by-N matrix A
c     using partial pivoting with row interchanges.
      m=dim
      lda=dim

c  Store A in Ainv to prevent it from being overwritten by LAPACK      
      Ainv = A
      call dgetrf(m,dim,Ainv,lda,ipiv,info)

c  DGETRI computes the inverse of a matrix using the LU factorization
c  computed by DGETRF.
      call dgetri(dim,Ainv,dim,ipiv,work,lwork,info)

      if (check_inversion) then
         if (info.NE.0) THEN
            stop 'Matrix inversion failed!'
         else
            print '(" Inverse Successful ")'
         endif
      endif

      deallocate(ipiv, stat = DeAllocateStatus)
      deallocate(work, stat = DeAllocateStatus)

      return
      end
      
