c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c     
      subroutine sample_direction_normal(dim,normal,r1,r2,u,pdf_u)
      implicit none
      include 'max.inc'
c
c     Purpose: to sample a direction "u" around a normal "n"
c     according to pdf(u)=|u.n|/pi
c
c     Input:
c       + dim: space dimension
c       + normal: normal direction
c       + r1, r2: uniform random numbers over [0,1]
c
c     Output:
c       + u: direction
c       + pdf_u: corresponding pdf
c       
c     I/O
      integer dim
      double precision normal(1:Ndim_mx)
      double precision r1,r2
      double precision u(1:Ndim_mx)
      double precision pdf_u
c     temp
      integer i
      double precision j1(1:Ndim_mx)
      double precision theta,phi
      double precision pdf_theta,pdf_phi
      double precision v(1:Ndim_mx)
      double precision product_normalj1
      double precision product_un
      double precision normals(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine sample_direction_normal'

      call vector_j1(dim,normal,j1)
      call sample_theta_cos(r1,theta,pdf_theta)
      call sample_phi(r2,phi,pdf_phi)

c     First rotation: vector "normal" is rotated by "theta" around "j1" -> vector "v"
      call rotation(dim,normal,theta,j1,v)
      call scalar_product(dim,normal,j1,product_normalj1)
      if (dabs(product_normalj1).gt.1.0D-8) then
         call error(label)
         write(*,*) 'normal=',(normal(i),i=1,dim)
         write(*,*) 'j1=',(j1(i),i=1,dim)
         write(*,*) '|normal.j1|=',product_normalj1
         write(*,*) 'should be null'
         stop
      endif
c     Second rotation: vector "v" is rotated by "phi" around "normal" -> vector "u"
      call rotation(dim,v,phi,normal,u)
c     compute pdf_u(u)
      pdf_u=pdf_theta*pdf_phi
c     double check
      call scalar_product(dim,normal,u,product_un)
      if (dabs(product_un-dcos(theta)).gt.1.0D-8) then
         call error(label)
         write(*,*) 'cos(',theta,')=',dcos(theta)
         write(*,*) '|u.n|=',product_un
         write(*,*) 'normal=',(normal(i),i=1,dim)
         write(*,*) 'j1=',(j1(i),i=1,dim)
         write(*,*) '|normal.j1|=',product_normalj1
         stop
      endif

      return
      end


      
      subroutine sample_theta_cos(r,theta,pdf_theta)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c
c     Purpose: to sample the zenital angle theta [0,pi/2]
c     according to pdf(w)=u.n/pi
c
c     Input:
c       + r: uniform random number over [0,1]
c     
c     Output:
c       + theta: zenital angle [0,pi/2] between the normal and a direction
c       + pdf_theta: probability density function corresponding to the chosen angle
c
c     I/O
      double precision r
      double precision theta
      double precision pdf_theta
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine sample_theta_cos'

      theta=0.50D+0*dacos(1.0D+0-2.0D+0*r)
      pdf_theta=2.0D+0*dcos(theta)
c     Tests
      if ((theta.lt.0.0D+0).or.(theta.gt.pi/2.0D+0)) then
         call error(label)
         write(*,*) 'theta=',theta
         write(*,*) 'is not between 0 and pi/2'
         write(*,*) 'r=',r
         stop
      endif
c     Tests

      return
      end



      subroutine sample_phi(r,phi,pdf_phi)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to sample the azimutal angle phi
c     uniformally in the [0,2*pi] range
c     
c     Input:
c       + r: uniform random number over [0,1]
c     
c     Output:
c       + phi: azimutal angle in [0,2*pi] range
c       + pdf_phi: associated probability density function
c
c     I/O
      double precision r
      double precision phi
      double precision pdf_phi
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine sample_phi'

      phi=2.0D+0*pi*r
      pdf_phi=1.0D+0/(2.0D+0*pi)

      return
      end


      
      subroutine sample_direction_sphere(dim,normal,r1,r2,u,pdf_u)
      implicit none
      include 'max.inc'
c
c     Purpose: to sample a direction "u" isotropically over 4pi sr
c
c     Input:
c       + dim: space dimension
c       + normal: normal to the surface
c       + r1, r2: uniform random numbers over [0,1]
c
c     Output:
c       + u: direction
c       + pdf_u: corresponding pdf
c       
c     I/O
      integer dim
      double precision normal(1:Ndim_mx)
      double precision r1,r2
      double precision u(1:Ndim_mx)
      double precision pdf_u
c     temp
      double precision theta,phi
      double precision pdf_theta,pdf_phi
c     label
      character*(Nchar_mx) label
      label='subroutine sample_direction_sphere'

      
      theta=dacos(1.0D+0-2.0D+0*r1)
      pdf_theta=0.5D+0
      call sample_phi(r2,phi,pdf_phi)
c     direction
      u(1)=dsin(theta)*dcos(phi)
      u(2)=dsin(theta)*dsin(phi)
      u(3)=dcos(theta)
c     compute pdf_u(u)
      pdf_u=pdf_theta*pdf_phi
      
      return
      end


      
      subroutine sample_length_unlimited(r,k,length,pdf_l)
      implicit none
      include 'max.inc'
c
c     Purpose: to generate a length in the [0,+infty) range according to:
c     pdf(length)=k*exp(-k*length)
c
c     Input:
c       + r: uniform random number in [0,1[
c       + k: optical coefficient (absorption, scattering or total extinction) (m^-1)
c
c     Output:
c       + length: random length chosen over the [0,+infty) range according to pdf(length)
c       + pdf_l: value of pdf(length)
c
c     I/O
      double precision r
      double precision k
      double precision length
      double precision pdf_l
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine sample_length_unlimited'

      if (k.le.0.0D+0) then
         call error(label)
         write(*,*) 'Bad input k=',k
         stop
      endif

      length=-1.0D+0/k*dlog(r)
      pdf_l=k*dexp(-k*length)

      return
      end


      
      subroutine sample_scattering_angle(generate_angle,r,
     &     pf,Na,user_phase,g_HG,
     &     theta,phase_function)
      implicit none
      include 'constants.inc'
      include 'max.inc'
c
c     Purpose: to return a scattering angle theta [0,pi]
c
c     Input:
c       + generate_angle: if true, the this routine will generate a new scattering
c         angle ("theta"). If false, this routine will take "theta" as an input, and
c         only compute the corresponding pdf ("phase_function").
c       + r: uniform random number in [0,1[
c       + pf: option for choosing the phase function that must be used
c         - pf=0: use the Rayleigh phase function
c         - pf=1: use the user-defined phase function
c         - pf=2: use the Henyey-Greenstein phase function
c       + Na: number of angles the phase function has been discretized into
c       + user_phase: array that contains descrete values of the user-defined phase-function
c         - user_phase(i,1): value of the ith cos(angle) (first value must be -1.0D+0, last value must be 1.0D+0)
c         - user_phase(i,2): value of the ith user-defined phase function for phase(i,1)
c       + g_HG: Henyey-Greenstein phase function asymetry parameter
c       + theta: if "generate_angle" is false, theta is an input
c     
c     Output:
c       + theta: scattering angle [0,pi] (zenital angle) if "generate_theta" is true
c       + phase_function: value of the phase function for the selected angle
c
c     I/O
      logical generate_angle
      double precision r
      integer pf
      integer Na
      double precision user_phase(1:Nangle_mx,1:2)
      double precision g_HG
      double precision theta
      double precision phase_function
c     temp
      integer i,angle,j
      double precision u,S,mu
      double precision cumul(1:Nangle_mx)
      double precision sum
      logical interval_found
      integer interval
c     label
      character*(Nchar_mx) label
      label='subroutine sample_scattering_angle'

      if (generate_angle) then
         if (pf.eq.0) then      ! use the Rayleigh phase-function
            u=4.0D+0*r-2.0D+0
            S=(u+dsqrt(1.0D+0+u**2))**(1.0D+0/3.0D+0)
            mu=S-1.0D+0/S
         else if (pf.eq.1) then ! use the user-defined phase function contained in "user_phase"
c     Tests
            if (Na.le.0) then
               call error(label)
               write(*,*) 'Bad value for input argument Na:'
               write(*,*) 'Na=',Na
               stop
            endif
            if (user_phase(1,1).ne.-1.0D+0) then
               call error(label)
               write(*,*) 'Bad value for input argument phase:'
               write(*,*) 'user_phase(1,1)=',user_phase(1,1)
               write(*,*) 'should be 1.0D+0'
               stop
            endif
            if (user_phase(Na,1).ne.1.0D+0) then
               call error(label)
               write(*,*) 'Bad value for input argument user_phase:'
               write(*,*) 'user_phase(',Na,',1)=',user_phase(Na,1)
               write(*,*) 'should be 0.0D+0'
               stop
            endif
            if (Na.ge.2) then
               do i=2,Na
                  if (user_phase(i,1).lt.user_phase(i-1,1)) then
                     call error(label)
                     write(*,*) 'user_phase(',i-1,',1)=',user_phase(i-1,1)
                     write(*,*) ' > user_phase(',i,',1)=',user_phase(i,1)
                     stop
                  endif
               enddo
            else
               call error(label)
               write(*,*) 'Na=',Na,' is too low'
               stop
            endif
            do i=1,Na
               if (user_phase(i,2).lt.0.0D+0) then
                  call error(label)
                  write(*,*) 'user_phase(',i,',2)=',user_phase(i,2)
                  write(*,*) 'is negative'
                  stop
               endif
            enddo
c     Tests
c     computation of the cumulative of the user-defined phase function
            cumul(1)=0.0D+0
            do i=2,Na
               cumul(i)=cumul(i-1)
     &              +2.0D+0*pi*
     &              (user_phase(i,2)+user_phase(i-1,2))
     &              *(user_phase(i,1)-user_phase(i-1,1))/2.0D+0
            enddo
            if (cumul(Na).le.0.0D+0) then
               call error(label)
               write(*,*) 'cumul(',Na,')=',cumul(Na)
               stop
            endif
c     normalization
            sum=cumul(Na)
            if (sum.ne.1.0D+0) then
               do i=1,Na
                  cumul(i)=cumul(i)/sum
               enddo
            endif
c     inversion of the user-defined phase function
            interval_found=.false.
            do i=2,Na
               if ((r.gt.cumul(i-1)).and.(r.lt.cumul(i))) then
                  interval=i
                  interval_found=.true.
                  goto 123
               endif
            enddo
 123        continue
c     Tests
            if (.not.interval_found) then
               call error(label)
               write(*,*) 'No angular interval could be found for R=',R
               write(*,*) 'and phase function cumulative:'
               do i=1,Na
                  write(*,*) 'mu(',i,')=',user_phase(i,1),
     &                 ' cumul(',i,')=',cumul(i)
               enddo
               stop
            endif
c     Tests
            mu=user_phase(interval-1,1)
     &           +(user_phase(interval,1)-user_phase(interval-1,1))
     &           /(cumul(interval)-cumul(interval-1))
     &           *(r-cumul(interval-1))
         else if (pf.eq.2) then ! use the Henyey-Greenstein phase-function
            if ((g_HG.lt.-1.0D+0).or.(g_HG.gt.1.0D+0)) then
               call error(label)
               write(*,*) 'Bad value for input argument g_HG=',g_HG
               stop
            endif
            mu=2.D+0*r*(1.D+0+g_HG)**2*(g_HG*(r-1.D+0)+1.D+0)/
     &           (1.D+0-g_HG*(1.D+0-2.D+0*r))**2-1.D+0
         endif                  ! pf
c     when generating the scattering angle is required, then compute "theta" from "mu"
         theta=dacos(mu)
      else
c     otherwise we just want to know the value of "phase_function" for the provided
c     value of "theta" -> "mu" has to be recomputed for the provided "theta"
         mu=dcos(theta)
      endif
      call scattering_phase_function(pf,Na,user_phase,g_HG,mu,
     &     phase_function)

      return
      end
