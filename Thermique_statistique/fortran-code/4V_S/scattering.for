c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c     
      subroutine scattering_phase_function(pf,Na,user_phase,g_HG,
     &     mu,phase_function)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the value of the phase function for a given scattering angle
c
c     Input:
c       + pf: option for choosing the phase function that must be used
c         - pf=0: use the Rayleigh phase function
c         - pf=1: use the user-defined phase function
c         - pf=2: use the Henyey-Greenstein phase function
c       + Na: number of angles the phase function has been discretized into
c       + user_phase: array that contains descrete values of the user-defined phase-function
c         - user_phase(i,1): value of the ith cos(angle) (first value must be -1.0D+0, last value must be 1.0D+0)
c         - user_phase(i,2): value of the ith user-defined phase function for phase(i,1)
c       + g_HG: Henyey-Greenstein phase function asymetry parameter
c       + mu: cosinus of the scattering angle
c
c     Output:
c       + phase_function: value of the phase function for 'mu'
c
c     I/O
      integer pf
      integer Na
      double precision user_phase(1:Nangle_mx,1:2)
      double precision g_HG
      double precision mu
      double precision phase_function
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine scattering_phase_function'

      if (pf.eq.0) then         ! use the Rayleigh phase-function
         call Rayleigh_phase_function(mu,phase_function)
      else if (pf.eq.1) then    ! use the user-defined phase function contained in "phase"
         call User_phase_function(mu,Na,user_phase,phase_function)
      else if (pf.eq.2) then    ! use the Henyey-Greenstein phase-function
         call HenyeyGreenstein_phase_function(mu,g_HG,phase_function)
      endif

      return
      end



      subroutine Rayleigh_phase_function(mu,phase_function)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the value of the Rayleigh phase function
c     for a given angle
c
c     Input:
c       + mu: cos(alpha) [-1;1]
c
c     Output:
c       + phase_function: value of the phase function for alpha
c
c     I/O
      double precision mu
      double precision phase_function
c     label
      character*(Nchar_mx) label
      label='subroutine Rayleigh_phase_function'

      phase_function=3.0D+0/8.0D+0*(1.0D+0+mu**2.0D+0)

      return
      end



      subroutine User_phase_function(mu,Na,user_phase,phase_function)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the value of the User-defined phase function
c     for a given angle
c
c     Input:
c       + mu: cos(alpha) [-1;1]
c       + Na: number of angles the user phase-function has been defined for
c       + user_phase: array defining the user phase-function:
c         - user_phase(angle,1): value of cos(theta)
c         - user_phase(angle,2): value of the user phase-function for that theta
c
c     Output:
c       + phase_function: value of the phase function for alpha

c     I/O
      double precision mu
      integer Na
      double precision user_phase(1:Nangle_mx,1:2)
      double precision phase_function
c     temp
      integer Np,ip
      double precision x(1:Np_mx)
      double precision f(1:Np_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine User_phase_function'

c     spline interpolation for "mu"
      Np=Na
      do ip=1,Np
         x(ip)=user_phase(ip,1)
         f(ip)=user_phase(ip,2)
      enddo ! ip
      call interpolation(Np,x,f,mu,phase_function)

      return
      end



      subroutine HenyeyGreenstein_phase_function(mu,g_HG,phase_function)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c
c     Purpose: to compute the value of the Henyey-Greenstein phase function
c     for a given angle
c
c     Input:
c       + mu: cos(alpha) [-1;1]
c       + g_HG: Henyey-Greenstein phase function asymetry parameter (-1;1)
c
c     Output:
c       + phase_function: value of the phase function for alpha
c
c     I/O
      double precision mu,g_HG
      double precision phase_function
c     label
      character*(Nchar_mx) label
      label='subroutine HenyeyGreenstein_phase_function'

      if ((g_HG.eq.1.0D+0).and.(mu.eq.1.0D+0)) then
         phase_function=1.0D+0/(4.0D+0*pi)
      else
         phase_function=1.0D+0/(4.0D+0*pi)
     &        *(1.0D+0-g_HG**2.0D+0)
     &        /(1.0D+0+g_HG**2.0D+0-2.0D+0*g_HG*mu)**1.5D+0
      endif

      return
      end



      subroutine direction_after_scattering(dim,u,theta,phi,v)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c
c     Purpose: to compute vector "v", based on initial direction "u" and scattering angle "alpha".
c
c     Input:
c       + dim: dimension of vectors
c       + u: initial direction
c       + theta: scattering angle [0,pi] (zenital angle)
c       + phi: azimutal angle {0,2pi]
c
c     Output:
c       + v: final direction
c     
c     I/O
      integer dim
      double precision u(1:Ndim_mx)
      double precision theta
      double precision phi
      double precision v(1:Ndim_mx)
c     temp
      integer i
      double precision j1(1:Ndim_mx)
      double precision t(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine direction_after_scattering'

c     Tests
      if ((theta.lt.0.0D+0).or.(theta.gt.pi)) then
         call error(label)
         write(*,*) 'theta=',theta
         write(*,*) 'is not between 0 and pi'
         stop
      endif
      if ((phi.lt.0.0D+0).or.(phi.gt.2.0D+0*pi)) then
         call error(label)
         write(*,*) 'phi=',phi
         write(*,*) 'is not between 0 and 2pi'
         stop
      endif
c     Tests
c     FIRST ROTATION of vector "u", by an angle "theta" aroung vector "j1" -> vector "t"
      call vector_j1(dim,u,j1)
      call rotation(dim,u,theta,j1,t)
c     SECOND ROTATION of vector "t", by an angle "alpha2" (chosen uniformally between 0 and 2pi), around vector "u" -> vector "v"
      call rotation(dim,t,phi,u,v)

      return
      end
