c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c
      program main
      implicit none
      include 'max.inc'
c     
c     Purpose: this program computes the average of optical paths lengths
c     within a sphere; emission positions are sampled uniformly on the
c     inner surface, and emission directions are sampled according to
c     |u.n|/pi ; scattering is taken into account: scattering path lengths "l"
c     are sampled according to ks.exp(-ks.l) over [0,+infty) and directions
c     of propagation after scattering are sampled according to the
c     Henyey-Greenstein phase function. Optical paths are stopped whenever
c     they hit the inner surface of the sphere again.
c     
c     Input data can be specified in the "data.in" file: position and size
c     of the sphere, values of the scattering coefficient and the 
c     Henyey-Greenstein asymetry parameter, and the number of statistical
c     realizations.
c     
c     Results are:
c       + <L> the average path length of optical paths
c       + 4V/S with V the volume of the sphere and S its surface
c     
c     Variables
      integer dim,n,iseed(4),j
      double precision, dimension(:), allocatable :: r
      integer DeAllocateStatus
      integer Nevent,event
      double precision center(1:Ndim_mx)
      double precision radius
      double precision ks
      double precision gHG
      double precision weight
      double precision sum
      double precision sum2
      double precision mean
      double precision variance
      double precision std_dev
      double precision normal(1:Ndim_mx)
      double precision u(1:Ndim_mx),pdf_u
      double precision P0(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision Pint(1:Ndim_mx),d2int
      double precision u0(1:Ndim_mx),pdf_u0
      double precision u1(1:Ndim_mx)
      double precision v(1:Ndim_mx),d
      logical debug,keep_flying
      double precision path_length
      double precision pdf_l
      integer Na
      double precision user_phase(1:Nangle_mx,1:2)
      double precision theta,phi,phase_function,pdf_phi
      character*(Nchar_mx) datafile
c     status
      integer len,ndone
      logical cerr
      double precision fdone,fdone0
      integer ifdone,pifdone
      character*(Nchar_mx) str
      character*(Nchar_mx) fmt,fmt0
c     label
      character*(Nchar_mx) label
      label='program main'

      debug=.false.
      
c     Dimension of space
      dim=3
c     Reading user input data
      datafile='./data.in'
      call read_data(datafile,dim,center,radius,ks,gHG,Nevent)
c     Initialization
      iseed(1)=0
      iseed(2)=1
      iseed(3)=2
      iseed(4)=3
      n=2                       ! generating "n" random numbers each time
      allocate(r(n))
c     Print initial status
      len=6
      call num2str(len,str,cerr)
      if (.not.cerr) then
         fmt0='(a,i'//trim(str)//',a)'
         fmt='(i'//trim(str)//',a)'
      endif
      fdone0=0
      ndone=0
      pifdone=0
      write(*,trim(fmt0),advance='no') 'Done:   ',floor(fdone0),' %'

c     Monte-Carlo
      sum=0.0D+0
      sum2=0.0D+0
      do event=1,Nevent
c     sample emission position "P0" on the sphere
         call dlaruv(iseed,n,r)
         call sample_direction_sphere(dim,normal,r(1),r(2),u,pdf_u)
         call scalar_vector(dim,radius,u,v)
         call add_vectors(dim,center,v,P0) ! "P0" is the emission position on the sphere
c     sample emission direction "u0"
         call normal_on_sphere(dim,center,radius,P0,normal)
         call dlaruv(iseed,n,r)
         call sample_direction_normal(dim,normal,r(1),r(2),u0,pdf_u0)
c     Initialization of the optical path
         weight=0.0D+0
         keep_flying=.true.
         do while (keep_flying)
c     intersection between (P0,u0) and the sphere
            call line_sphere_forward_intersect(debug,dim,
     &           P0,u0,center,radius,Pint,d2int)
c     sample the path length
            call dlaruv(iseed,n,r)
            call sample_length_unlimited(r(1),ks,path_length,pdf_l)
            if (path_length.lt.d2int) then
c     get scattering position P1
               call scalar_vector(dim,path_length,u0,v)
               call add_vectors(dim,P0,v,P1)
c     sample new propagation direction after scattering
               call dlaruv(iseed,n,r)
               call sample_scattering_angle(.true.,r(1),
     &              2,1,user_phase,gHG,
     &              theta,phase_function)
               call sample_phi(r(2),phi,pdf_phi)
               call direction_after_scattering(dim,u0,theta,phi,u1)
c     travelled distance
               d=path_length
c     loop using new origin and propagation direction
               call copy_vector(dim,P1,P0)
               call copy_vector(dim,u1,u0)
            else
c     travelled distance
               d=d2int
c     stop the realization
               keep_flying=.false.
            endif               ! path_length > d2int
c     update the weight of the realization
            weight=weight+d
         enddo                  ! while (keep_flying)
c     print status
         ndone=ndone+1
         fdone=dble(ndone)/dble(Nevent)*1.0D+2
         ifdone=floor(fdone)
         if (ifdone.gt.pifdone) then
            do j=1,len+2
               write(*,"(a)",advance='no') "\b"
            enddo               ! j
            write(*,trim(fmt),advance='no') floor(fdone),' %'
            pifdone=ifdone
         endif                  ! ifdone > pifdone
c     increment sums
         sum=sum+weight
         sum2=sum2+weight**2.0D+0
      enddo                     ! event
      write(*,*)
c     compute average and standard deviation
      call statistics(Nevent,sum,sum2,mean,variance,std_dev)
c     and finally print results
      write(*,*) '<L>=',mean,' +:-',std_dev
      write(*,*) '4V/S=',4.0D+0*radius/3.0D+0
c     release allocated memory
      deallocate(r,stat=DeAllocateStatus)
      
      end
