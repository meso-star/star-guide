c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c     
      subroutine interpolation(Np,x,f,x0,f0)
      implicit none
      include 'max.inc'
c
c     Purpose: to perform an interpolation of function "f" at position"x0"
c     among Np couples of [x(i),f(x(i))] values
c
c     Input:
c       + Np: number of x/f(x) values
c       + x: array of "x" values
c       + f: array of "f(x)" values
c       + x0: value of x for which function f has to be interpolated
c
c     Output:
c       + f0: value of f(x0)
c
c     I/O
      integer Np
      double precision x(1:Np_mx)
      double precision f(1:Np_mx)
      double precision x0,f0
c     temp
      integer Nacc
      double precision xacc(1:Nacc_mx,1:2)
      integer index_acc(1:Nacc_mx,1:2)
      double precision z(1:Np_mx)
      integer valid
c     Debug
      integer i,j
c     Debug
      character*(Nchar_mx) label
      label='subroutine interpolation'

c     Inconsistencies
      do i=2,Np
         if (x(i).lt.x(i-1)) then
            call error(label)
            write(*,*) 'x(',i,')=',x(i)
            write(*,*) 'is lower than x(',i-1,')=',x(i-1)
            write(*,*) 'x and f arrays must be sorted'
            write(*,*) 'by ascending values of x'
            do j=1,Np
               write(*,*) 'x(',j,')=',x(j),' f(',j,')=',f(j)
            enddo ! j
            stop
         endif
      enddo ! i
c     Inconsistencies

      if (Np.ge.4) then
c     cubic spline interpolation when enough reference points are available
         call acceleration(Np,x,Nacc,xacc,index_acc)
         call cubic_spline(Np,x,f,z,valid)
         call spline_interpolation(Np,x,f,z,
     &        Nacc,xacc,index_acc,x0,f0)
      else if (Np.ge.2) then
c     linear interpolation if only two or three points
         if (Np.eq.2) then
            call linear_interpolation(x(1),x(2),f(1),f(2),x0,f0)
         else if (Np.eq.3) then
            if (x0.le.x(2)) then
               call linear_interpolation(x(1),x(2),f(1),f(2),x0,f0)
            else if (x0.gt.x(2)) then
               call linear_interpolation(x(2),x(3),f(2),f(3),x0,f0)
            else
               call error(label)
               write(*,*) 'could not determine which one is bigger:'
               write(*,*) 'x0=',x0
               write(*,*) 'x(2)=',x(2)
               stop
            endif
         else
            call error(label)
            write(*,*) 'Np was found < 4 and >= 2'
            write(*,*) 'however Np is different from 2 and 3'
            write(*,*) 'Np=',Np
            stop
         endif ! Np=2 or 3
      else if (Np.eq.1) then
c     linear interpolation between zero and the only available point
         call linear_interpolation(0.0D+0,x(1),0.0D+0,f(1),x0,f0)
      else
         call error(label)
         write(*,*) 'Interpolation is impossible with:'
         write(*,*) 'Np=',Np
         stop
      endif

      return
      end



      subroutine linear_interpolation(x1,x2,f1,f2,x0,f0)
      implicit none
      include 'max.inc'
c
c     Purpose: to perform a linear interpolation between two reference values
c
c     Input:
c       + x1, x2: values of x for the two reference points
c       + f1, f2: f1=f(x1) and f2=f(x2), known values of f at points x1 and x2
c       + x0: value of x where interpolation has to be performed
c     
c     Output:
c       + f0: value of f interpolated at x=x0
c
c     I/O
      double precision x1,x2
      double precision f1,f2
      double precision x0,f0
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine linear_interpolation'

      if ((x1.eq.x2).and.(f1.ne.f2)) then
         call error(label)
         write(*,*) 'x1=',x1
         write(*,*) 'x2=',x2
         write(*,*) 'have identical values'
         write(*,*) 'while:'
         write(*,*) 'f1=',f1
         write(*,*) 'f2=',f2
         write(*,*) 'are different'
         stop
      else
         f0=f1+(f2-f1)/(x2-x1)*(x0-x1)
      endif

      return
      end

