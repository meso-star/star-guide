c     Copyright (C)
c     2021 Mésotar
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c     
      subroutine spline_interpolation(Np,x,f,z,Nacc,xacc,index_acc,x0,f0)
      implicit none
      include 'max.inc'
c
c     Purpose: to interpolate a function "f" from discrete x/f(x) values
c     using a cubic spline interpolation
c
c     Input:
c       + Np: number of x/f(x) values
c       + x: array of "x" values
c       + f: array of "f(x)" values
c       + z: array of z values
c       + Nacc: number of points in array 'xacc'
c       + xacc & index_acc: acceleration array
c       + x0: value of x for which function f has to be interpolated
c
c     Output:
c       + f0: value of f(x0)
c
c     I/O
      integer Np
      double precision x(1:Np_mx)
      double precision f(1:Np_mx)
      double precision z(1:Np_mx)
      integer Nacc
      double precision xacc(1:Nacc_mx,1:2)
      integer index_acc(1:Nacc_mx,1:2)
      double precision x0
      double precision f0
c     temp
      double precision x1,x2,f1,f2,z1,z2
c     label
      character*(Nchar_mx) label
      label='subroutine spline_interpolation'

c     Indentify parameters for the quadratic function
      call cubic_spline_parameters(Np,x,f,z,Nacc,xacc,index_acc,x0,
     &     x1,x2,f1,f2,z1,z2)
      
c     interpolation
      f0=(z1*(x2-x0)**3.0D+0+z2*(x0-x1)**3.0D+0)/(6.0D+0*(x2-x1))
     &     +(f2/(x2-x1)-(x2-x1)/6.0D+0*z2)*(x0-x1)
     &     +(f1/(x2-x1)-(x2-x1)/6.0D+0*z1)*(x2-x0)

      return
      end



      subroutine spline_integration(Np,x,f,z,Nacc,xacc,index_acc,a,b,A0)
      implicit none
      include 'max.inc'
c
c     Purpose: to integrate a function "f" from discrete x/f(x) values
c     using a cubic spline interpolation
c
c     Input:
c       + Np: number of x/f(x) values
c       + x: array of "x" values
c       + f: array of "f(x)" values
c       + z: array of z values
c       + Nacc: number of points in array 'xacc'
c       + xacc & index_acc: acceleration array
c       + a: lower integration boundary
c       + b: higher integration boundary
c
c     Output:
c       + A0: value of int(f(x),x=a,b)
c
c     I/O
      integer Np
      double precision x(1:Np_mx)
      double precision f(1:Np_mx)
      double precision z(1:Np_mx)
      integer Nacc
      double precision xacc(1:Nacc_mx,1:2)
      integer index_acc(1:Nacc_mx,1:2)
      double precision a,b
      double precision A0
c     temp
      double precision x1,x2,f1,f2,z1,z2
      integer ia,ib,i
      double precision int
c     label
      character*(Nchar_mx) label
      label='subroutine spline_interpolation'

c     Indentify intervals
      call identify_index(Np,x,f,z,Nacc,xacc,index_acc,a,ia)
      call identify_index(Np,x,f,z,Nacc,xacc,index_acc,b,ib)
      if (ib.lt.ia) then
         A0=0.0D+0
c     interval "ib"
         call spline_parameters(x,f,z,ib,x1,x2,f1,f2,z1,z2)
         call int_S(x1,x2,f1,f2,z1,z2,b,x2,int)
         A0=A0+int
c     intervals from "ib+1" to "ia-1"
         if (ia.gt.ib+1) then
            do i=ib+1,ia-1
               call spline_parameters(x,f,z,i,x1,x2,f1,f2,z1,z2)
               call int_S(x1,x2,f1,f2,z1,z2,x1,x2,int)
               A0=A0+int
            enddo ! i
         endif
c     interval "ia"
         call spline_parameters(x,f,z,ia,x1,x2,f1,f2,z1,z2)
         call int_S(x1,x2,f1,f2,z1,z2,x1,a,int)
         A0=A0+int

      else if (ib.eq.ia) then
         call spline_parameters(x,f,z,ia,x1,x2,f1,f2,z1,z2)
         call int_S(x1,x2,f1,f2,z1,z2,a,b,A0)

      else if (ib.gt.ia) then
         A0=0.0D+0
c     interval "ia"
         call spline_parameters(x,f,z,ia,x1,x2,f1,f2,z1,z2)
         call int_S(x1,x2,f1,f2,z1,z2,a,x2,int)
         A0=A0+int
c     intervals from "ia+1" to "ib-1"
         if (ib.gt.ia+1) then
            do i=ia+1,ib-1
               call spline_parameters(x,f,z,i,x1,x2,f1,f2,z1,z2)
               call int_S(x1,x2,f1,f2,z1,z2,x1,x2,int)
               A0=A0+int
            enddo ! i
         endif
c     interval "ib"
         call spline_parameters(x,f,z,ib,x1,x2,f1,f2,z1,z2)
         call int_S(x1,x2,f1,f2,z1,z2,x1,b,int)
         A0=A0+int

      else
         call error(label)
         write(*,*) 'a=',a,' ia=',ia
         write(*,*) 'b=',b,' ib=',ib
         stop
      endif

      return
      end


      
      subroutine int_S(x1,x2,f1,f2,z1,z2,a,b,int)
      implicit none
      include 'max.inc'
c
c     Purpose: to perform the integration of the quadratic function over an [a,b] interval
c     
c     Input:
c       + x1,x2: values of x for the interval
c       + f1,f2: values of f for x1 and x2
c       + z1 and z2: coefficients z for the [x1,x2] interval
c       + a: lower integration boundary
c       + b: higher integration boundary
c
c     Output:
c       + A: value of int(f(x),x=a,b)
c       
c     I/O
      double precision x1,x2,f1,f2,z1,z2
      double precision a,b
      double precision int
c     temp
      double precision coefficients(1:4)
c     label
      character*(Nchar_mx) label
      label='subroutine int_S'

      call deg4_polynom_coefficients(x1,x2,f1,f2,z1,z2,
     &     coefficients)
      int=coefficients(1)*(b**4.0D+0-a**4.0D+0)
     &     +coefficients(2)*(b**3.0D+0-a**3.0D+0)
     &     +coefficients(3)*(b**2.0D+0-a**2.0D+0)
     &     +coefficients(4)*(b**1.0D+0-a**1.0D+0)
      if (a.gt.b) then
c     invert result when lower integration limit < higher limit
         int=-int
      endif
c     Debug
      if (int.lt.0.0D+0) then
         int=dabs(int)
      endif
      
      return
      end


      
      subroutine deg4_polynom_coefficients(x1,x2,f1,f2,z1,z2,coefficients)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the coefficient of the 4th-degree polynom
c     used as the integral of the cubic spline, for a given interval
c
c     Input:
c       + x1,x2: values of x for the interval
c       + f1,f2: values of f for x1 and x2
c       + z1 and z2: coefficients z for the [x1,x2] interval
c
c     Output:
c       + coefficients: the 4 coefficients of the polynom,
c         i.e. coefficients a, b, c and d from: a*x^4+b*x^4+c*x^2+d*x
c
c     I/O
      double precision x1,x2,f1,f2,z1,z2
      double precision coefficients(1:4)
c     label
      character*(Nchar_mx) label
      label='subroutine deg4_polynom_coefficients'

      coefficients(1)=(z2-z1)/(24.0D+0*(x2-x1))
      coefficients(2)=(z1*x2-z2*x1)/(6.0D+0*(x2-x1))
      coefficients(3)=((z2*x1**2.0D+0-z1*x2**2.0D+0)
     &     /(2.0D+0*(x2-x1))
     &     +(f2-f1)/(x2-x1)
     &     +(x2-x1)*(z1-z2)/6.0D+0)/2.0D+0
      coefficients(4)=(z1*x2**3.0D+0-z2*x1**3.0D+0)
     &     /(6.0D+0*(x2-x1))
     &     +(f1*x2-f2*x1)/(x2-x1)
     &     +(x2-x1)*(x1*z2-x2*z1)/6.0D+0
      
      return
      end



      subroutine cubic_spline_parameters(Np,x,f,z,
     &     Nacc,xacc,index_acc,x0,
     &     x1,x2,f1,f2,z1,z2)
      implicit none
      include 'max.inc'
c
c     Purpose: to use acceleration arrays and return coefficients (x,f,z) for the
c     quadratic function.
c
c     Input:
c       + Np: number of x/f(x) values
c       + x: array of "x" values
c       + f: array of "f(x)" values
c       + z: array of z values
c       + Nacc: number of points in array 'xacc'
c       + xacc & index_acc: acceleration array
c       + x0: value of x for which function f has to be interpolated
c
c     Output:
c       + x1,x2: values of x around "x0"
c       + f1,f2: values of f for x1 and x2
c       + z1 and z2: coefficients z for the [x1,x2] interval
c
c     I/O
      integer Np
      double precision x(1:Np_mx)
      double precision f(1:Np_mx)
      double precision z(1:Np_mx)
      integer Nacc
      double precision xacc(1:Nacc_mx,1:2)
      integer index_acc(1:Nacc_mx,1:2)
      double precision x0
      double precision x1,x2,f1,f2,z1,z2
c     temp
      integer index
c     label
      character*(Nchar_mx) label
      label='subroutine cubic_spline_parameters'

      call identify_index(Np,x,f,z,Nacc,xacc,index_acc,x0,
     &     index)
      call spline_parameters(x,f,z,index,
     &     x1,x2,f1,f2,z1,z2)

      return
      end



      subroutine spline_parameters(x,f,z,index,x1,x2,f1,f2,z1,z2)
      implicit none
      include 'max.inc'
c
c     Purpose: to return quadratic function parameters for a given interval
c
c     Input:
c       + x: array of "x" values
c       + f: array of "f(x)" values
c       + z: array of z values
c       + index: index of the interval
c
c     Output:
c       + x1,x2: values of x for the interval
c       + f1,f2: values of f for the interval
c       + z1 and z2: coefficients z for the interval
c
c     I/O
      double precision x(1:Np_mx)
      double precision f(1:Np_mx)
      double precision z(1:Np_mx)
      integer index
      double precision x1,x2,f1,f2,z1,z2
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine spline_parameters'

      x1=x(index)
      x2=x(index+1)
      f1=f(index)
      f2=f(index+1)
      z1=z(index)
      z2=z(index+1)

      return
      end
      


      subroutine identify_index(Np,x,f,z,Nacc,xacc,index_acc,x0,index)
      implicit none
      include 'max.inc'
c
c     Purpose: identify the  interval to use
c
c     Input:
c       + Np: number of x/f(x) values
c       + x: array of "x" values
c       + f: array of "f(x)" values
c       + z: array of z values
c       + Nacc: number of points in array 'xacc'
c       + xacc & index_acc: acceleration array
c       + x0: value of x for which function f has to be interpolated
c
c     Output:
c       + index: index of the interval where "x0" is located
c
c     I/O
      integer Np
      double precision x(1:Np_mx)
      double precision f(1:Np_mx)
      double precision z(1:Np_mx)
      integer Nacc
      double precision xacc(1:Nacc_mx,1:2)
      integer index_acc(1:Nacc_mx,1:2)
      double precision x0
      integer index
c     temp
      integer ifound,i,j,i1,i2
c     label
      character*(Nchar_mx) label
      label='subroutine identify_index'

c     acceleration
      ifound=0
      do i=1,Nacc
         if ((x0.ge.xacc(i,1)).and.(x0.le.xacc(i,2))) then
            ifound=1
            i1=index_acc(i,1)
            i2=index_acc(i,2)
            goto 111
         endif
      enddo ! i
 111  continue
c     ----------------------------------------
c     extrapolation instead of interpolation
      if (ifound.eq.0) then
         if (x0.lt.xacc(1,1)) then
            ifound=1
            i1=index_acc(1,1)
            i2=index_acc(1,2)
         else if (x0.gt.xacc(Nacc,2)) then
            ifound=1
            i1=index_acc(Nacc,1)
            i2=index_acc(Nacc,2)
         else
            call error(label)
            write(*,*) 'x0=',x0
            write(*,*) 'could not be located'
            stop
         endif
      endif
c     ----------------------------------------
      if (ifound.eq.0) then
         call error(label)
         write(*,*) 'acceleration interval could not be found:'
         write(*,*) 'x0=',x0
         write(*,*) 'xacc(1)=',xacc(1,1),xacc(1,2)
         write(*,*) 'xacc(',Nacc,')=',xacc(Nacc,1),xacc(Nacc,2)
         stop
      endif

c     identification of index
      ifound=0
      do i=i1+1,i2
         if ((x0.ge.x(i-1)).and.(x0.le.x(i))) then
            ifound=1
            index=i-1
            goto 123
         endif
      enddo
 123  continue
c     ----------------------------------------
c     extrapolation instead of interpolation
      if (ifound.eq.0) then
         if (x0.lt.xacc(1,1)) then
            ifound=1
            index=1
         else if (x0.gt.xacc(Nacc,2)) then
            ifound=1
            index=Np-1
         endif
      endif
c     ----------------------------------------
      if (ifound.eq.0) then
         call error(label)
         write(*,*) 'Interval could not be found for:'
         write(*,*) 'x0=',x0
         write(*,*) 'xacc(1,1)=',xacc(1,1)
         write(*,*) 'xacc(',Nacc,',2)=',xacc(Nacc,2)
         write(*,*) 'i1=',i1
         write(*,*) 'i2=',i2
         do i=1,Nacc
            write(*,*) 'index_acc(',i,')=',(index_acc(i,j),j=1,2)
         enddo ! i
         stop
      endif

      return
      end



      subroutine cubic_spline(Np,x,f,z,valid)
      implicit none
      include 'max.inc'
c
c     Purpose: to find the Np values of "z" coefficients that are needed to compute
c     the Np-1 cubic spline functions.
c
c     Input:
c       + Np: number of x/f points: HAS TO BE AT LEAST 4
c       + x: array of "x" values
c       + f: array of "f(x)" values
c
c     Output:
c       + z: Np values of z coefficients
c       + valid: 1 if solution found; 0 otherwise.
c
c     I/O
      integer Np
      double precision x(1:Np_mx)
      double precision f(1:Np_mx)
      double precision z(1:Np_mx)
      integer valid
c     temp
      integer i
      double precision alpha(1:Np_mx)
      double precision beta(1:Np_mx)
      double precision gamma(1:Np_mx)
      double precision delta(1:Np_mx)
      double precision a(1:Np_mx)
      double precision b(1:Np_mx)
      double precision c(1:Np_mx)
      double precision d(1:Np_mx)
      double precision sol(1:Np_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine cubic_spline'

c     tests
      if (Np.lt.4) then
         call error(label)
         write(*,*) 'Bad input parameter:'
         write(*,*) 'Np=',Np
         write(*,*) 'must be at least equal to 4:'
         write(*,*) 'Np-1 z coefficients have to be evaluated'
         write(*,*) 'size of tridiagonal systems is Np-2'
         write(*,*) 'and must be equal to 2 at least'
         stop
      endif
      if (Np.gt.Np_mx) then
         call error(label)
         write(*,*) 'Bad input parameter:'
         write(*,*) 'Np=',Np
         write(*,*) 'exceeds Np_mx=',Np_mx
         stop
      endif

c     computing the Np-1 values of z coefficients
      z(1)=0.0D+0
      z(Np)=0.0D+0
      do i=2,Np-1
         if (i.eq.2) then
            alpha(i)=0.0D+0
         else
            alpha(i)=x(i)-x(i-1)
         endif
         beta(i)=2.0D+0*(x(i+1)-x(i-1))
         if (i.lt.Np-1) then
            gamma(i)=x(i+1)-x(i)
         else
            gamma(i)=0.0D+0
         endif
         delta(i)=6.0D+0*((f(i+1)-f(i))/(x(i+1)-x(i))
     &        -(f(i)-f(i-1))/(x(i)-x(i-1)))
      enddo
      do i=1,Np-2
         a(i)=alpha(i+1)
         b(i)=beta(i+1)
         c(i)=gamma(i+1)
         d(i)=delta(i+1)
      enddo
      call tridiag(Np-2,a,b,c,d,sol,valid)
c     Debug
c      write(*,*) 'valid=',valid
c     Debug
      if (valid.eq.0) then
         call error(label)
         write(*,*) 'solution NOT found'
         stop
      endif
      do i=2,Np-1
         z(i)=sol(i-1)
      enddo

      return
      end



      subroutine tridiag(n,alpha,beta,gamma,y,x,valid)
      implicit none
      include 'max.inc'
c
c     Purpose: to solve a tridiagonal linear system
c
c     Input:
c       + n: dimension of the system
c       + alpha: array of below-diagonal terms , with alpha(1)=0
c       + beta: array of diagonal terms
c       + gamma: array of above-diagonal terms, with gamma(n)=0
c       + y: array of y values
c
c     Ouput:
c       + x: array of x values
c       + valid: 0 if solution is not valid; 1 if solution is valid
c
c     I/O
      integer n
      double precision alpha(1:Np_mx)
      double precision beta(1:Np_mx)
      double precision gamma(1:Np_mx)
      double precision y(1:Np_mx)
      double precision x(1:Np_mx)
      integer valid
c     temp
      integer i
      double precision delta(1:Np_mx)
      double precision yp(1:Np_mx)
      double precision yv(1:Np_mx)
      double precision epsilon
      character*(Nchar_mx) label
c     label
      parameter(epsilon=1.0D-6)
      label='subroutine tridiag'

c     tests
      if (n.lt.2) then
         call error(label)
         write(*,*) 'n=',n
         stop
      endif
      if (alpha(1).ne.0.0D+0) then
         call error(label)
         write(*,*) 'alpha(1)=',alpha(1)
         stop
      endif
      if (gamma(n).ne.0.0D+0) then
         call error(label)
         write(*,*) 'gamma(n)=',gamma(n)
         stop
      endif
c     tests

      if (beta(1).eq.0.0D+0) then
         call error(label)
         write(*,*) 'beta(1)=',beta(1)
         stop
      else
         delta(1)=gamma(1)/beta(1)
         yp(1)=y(1)/beta(1)
      endif
      do i=2,n
         if (beta(i)-delta(i-1)*alpha(i).eq.0.0D+0) then
            call error(label)
            write(*,*) 'beta(',i,')=',beta(i)
            write(*,*) 'delta(',i-1,')*alpha(',i,')=',
     &           delta(i-1)*alpha(i)
            stop
         else
            delta(i)=gamma(i)/(beta(i)-delta(i-1)*alpha(i))
            yp(i)=(y(i)-yp(i-1)*alpha(i))/(beta(i)-delta(i-1)*alpha(i))
         endif
      enddo ! i

      x(n)=yp(n)
      do i=n-1,1,-1
         x(i)=yp(i)-x(i+1)*delta(i)
      enddo

c     Tests
      yv(1)=beta(1)*x(1)+gamma(1)*x(2)
      do i=2,n-1
         yv(i)=alpha(i)*x(i-1)+beta(i)*x(i)+gamma(i)*x(i+1)
      enddo
      yv(n)=alpha(n)*x(n-1)+beta(n)*x(n)
      valid=1
      do i=1,n
         if (dabs(yv(i)-y(i)).gt.epsilon) then
            valid=0
            goto 123
         endif
      enddo
c     Tests
 123  continue
      return
      end

      

      subroutine acceleration(Np,x,Nacc,xacc,index_acc)
      implicit none
      include 'max.inc'
c     
c     Purpose: produce acceleration array 'xacc'
c
c     Input:
c       + Np: number of points in array 'x'
c       + x: array of values for x
c
c     Output:
c       + Nacc: number of points in array 'xacc'
c       + xacc & index_acc: acceleration array
c
c     I/O
      integer Np
      double precision x(1:Np_mx)
      integer Nacc
      double precision xacc(1:Nacc_mx,1:2)
      integer index_acc(1:Nacc_mx,1:2)
c     temp
      integer rf
      parameter(rf=100)
      integer i,j,Nr
c     label
      character*(Nchar_mx) label
      label='subroutine acceleration'

      Nacc=int(Np/rf)+1
c     Debug
      if (Nacc.gt.Nacc_mx) then
         call error(label)
         write(*,*) 'Nacc=',Nacc
         write(*,*) 'while Nacc_mx=',Nacc_mx
         stop
      endif
c     Debug

      Nr=Np-(Nacc-1)*rf
      index_acc(1,1)=1
      do i=1,Nacc-1
         index_acc(i,2)=i*rf
         index_acc(i+1,1)=index_acc(i,2)
         xacc(i,1)=x(index_acc(i,1))
         xacc(i,2)=x(index_acc(i,2))
      enddo                     ! i
      index_acc(Nacc,2)=Np
      xacc(Nacc,1)=x(index_acc(Nacc,1))
      xacc(Nacc,2)=x(index_acc(Nacc,2))

      return
      end
