\documentclass[epsf,psfig,fancyheadings,12pt]{article}
%\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
%\usepackage[francais]{babel}
\usepackage[utf8]{inputenc}
\usepackage{fancyhdr}
\usepackage{amsmath}
\usepackage{graphics}
\usepackage{subfigure}
\usepackage[final]{epsfig}
\usepackage{mathrsfs}
\usepackage{auto-pst-pdf}
\usepackage{pstricks,pst-node,pst-text,pst-3d}
\usepackage{amssymb}
\usepackage{multirow}
\usepackage{rotating}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{nicefrac}
\usepackage{xfrac}
\usepackage{animate}
\usepackage[toc,page]{appendix}
\makeatletter
% double spacing lines 
\renewcommand{\baselinestretch}{1}
\newcommand{\LyX}{L\kern-.1667em\lower.25em\hbox{Y}\kern-.125emX\spacefactor1000}
%pour faire reference toujours avec la meme norme
%lyx !
\newcommand{\eq}[1]{Eq.~\ref{eq:#1}}
\newcommand{\fig}[1]{Fig.~\ref{fig:#1}}
\newcommand{\tab}[1]{Tab.~\ref{tab:#1}}
\newcommand{\para}[1]{Sec.~\ref{para:#1}}
\newcommand{\ap}[1]{Appendix~\ref{ap:#1}}
\renewcommand{\appendixtocname}{Annexes} 
\renewcommand{\appendixpagename}{Annexes}
\newcommand{\ul}{\underline}
%
\makeatother

%\fancyhf{}%
% Utiliser \fancyhead[L] pour mettre du texte dans la partie de gauche
% du header
%\fancyhead[L]{}%
%\fancyhead[R]{\thepage}%
%\renewcommand{\headrulewidth}{0pt} % ça c'est pour faire une ligne horizontale
%\headsep=25pt % ça c'est pour laisser de la place entre le header et le texte
%\pagestyle{fancy}%

% margins
\setlength{\topmargin}{-0.75in}
\setlength{\textheight}{9in}
\setlength{\oddsidemargin}{-.50in}
\setlength{\textwidth}{7.5in}



\begin{document}


\title{Pour Benjamin Boissière\\ Transfert radiatif}

\maketitle
\begin{center}
\author{\bf|Méso|Star>}\\
 (\url{http://www.meso-star.com}) \\
\end{center}
\vspace{1cm}

\tableofcontents

\newpage

%============================================================================================================
\section{Corps noir et luminance}

Le concept de base, en rayonnement, est le corps noir. C'est la notion à partir de laquelle tout
l'édifice est construit, elle est donc vraiment essentielle.

Un corps noir peut être défini comme un corps théorique (amas de matière, solide, liquide ou gazeux)
qui absorbe tout rayonnement incident. Le corollaire est qu'aucun corps réel ne va émettre autant
d'énergie radiative, à température identique, qu'un corps noir. On cite classiquement comme
exemple de corps noir: le Soleil. Sur la base du ``fait bien connu'' qu'un quanta d'énergie
émis au coeur d'une étoile peut mettre des centaines de milliers voire des millions d'années
pour être émis en surface, résultat du fait que le plasma des étoiles est un milieu
extrêmement diffusant. La réalité est plus complexe, et dépend grandement de la longueur d'onde.
Un autre bon exemple de corps noir est un volume d'eau: dans l'infrarouge thermique terrestre
du moins ($\lambda$ > 4 $\mu$m) l'eau liquide présente un si grand nombre de transitions
de vibration-rotation qu'un photon incident est forcément absorbé dans les premiers micromètres
après la surface, et ce quelle que soit la longueur d'onde IR considérée.

Le second concept essentiel est la luminance. Il s'agit du flux d'énergie radiative, directionnel:
nombre de photons traversant, par unité de temps, une surface virtuelle perpendiculaire à la
direction de propagation de ce rayonnement, par unité d'angle solide et par unité de longueur
d'onde; l'unité est le $W/m_{\perp}^{2}/sr/\mu m$; j'insiste lourdement sur le ``par mètre carré de
surface perpendiculaire'' noté $m_{\perp}^{2}$: la luminance étant une grandeur directionnelle,
elle mesure un flux d'énergie radiative se propageant dans une direction donnée. Et ce flux
surfacique est donc nécessairement mesuré par unité de surface perpendiculaire à cette direction
de propagation. Ce petit détail est à l'origine du ou des $|\vec{u}.\vec{n}|$=cos($\theta$) que l'on retrouve dès
qu'on va réaliser des intégrales directionnelles (facteurs de forme, etc); pour donner une
image, c'est la grandeur mesurée par les photographes lorsqu'ils effectuent des mesures à l'aide
de leur ``cellule'' en diverses positions et dans diverses directions, afin de quantifier la
luminosité de la pièce.

La luminance d'un corps noir, à une longueur d'onde $\lambda$ donnée, dépend de la température
$T$ du corps émetteur. Elle est notée $L_{\lambda}^{0}(T)$, et on sait bien que:

\begin{equation}
  \int_{0}^{+\infty} L_{\lambda}^{0}(T)d\lambda = \frac{\sigma}{\pi}T^{4}
\end{equation}

avec $\sigma$=5.67 $10^{-8}$ $W.m^{-2}.K^{-4}$ la constante de Stefan-Boltzmann

Si on veut calculer l'émittance $M^{0}_{\lambda}(T)$ d'un corps noir (flux émis par $m^{2}$), on a

\begin{equation}
  \begin{split}
    M^{0}_{\lambda}(T)&=\int_{2\pi} |\vec{u}.\vec{n}| L_{\lambda}^{0}(T) d\vec{u}\\
    &= L_{\lambda}^{0}(T)\int_{0}^{2\pi}d\phi \int_{0}^{\frac{pi}{2}} cos(\theta)d\theta\\
    &=\pi L_{\lambda}^{0}(T)
  \end{split}
\end{equation}
avec $\theta$ l'angle zénithal et $\phi$ l'angle azimuthal (projection dans un repère à
coordonnées sphériques, dont l'axe $z$ est orienté par la normale $\vec{n}$ à la surface).

On vérifie donc:
\begin{equation}
  \int_{0}^{+\infty} M_{\lambda}^{0}(T)d\lambda = \sigma T^{4}
\end{equation}

%============================================================================================================
\section{Propriétés de surface}

On se propose de noter:
\begin{itemize}
\item $\rho_{\lambda}$ la réflectivité d'un corps; il s'agit de la probabilité (0-1) qu'un photon incident
  à la surface de ce corps soit réfléchi;
\item $\alpha_{\lambda}$ l'absorptivité d'un corps; il s'agit de la probabilité (0-1) qu'un photon incident
  à la surface de ce corps soit absorbé;
\item $\epsilon_{\lambda}$ l'émissivité d'un corps; il s'agit du ratio entre la puissance radiative émise
  par le corps en question, à une température $T$ donnée, et la puissance radiative émise par un corps
  noir à la même température:
  \begin{equation}
    \epsilon_{\lambda}=\frac{L_{\lambda}(T)}{L_{\lambda}^{0}(T)}
  \end{equation}
  On peut également voir cette émissivité comme une probabilité: le corps noir émettant au maximum,
  un quanta d'énergie arrivant en surface a une probabilité de 1 d'être émis; pour un corps réel, qui
  émet nécessairement moins que le corps noir à la même température, l'émissivité est une mesure de la
  probabilité que ce même quanta d'énergie soit émis.
\end{itemize}

Si on veut généraliser aux corps semi-transparents, il faut également définir la transmissivité $\mathcal{T}_{\lambda}$
du corps. Tout rayonnement incident étant nécessairement soit absorbé, soit réfléchi, soit transmis, on
a l'égalité suivante:

\begin{equation}
  \alpha_{\lambda}+\rho_{\lambda}+\mathcal{T}_{\lambda}=1
\end{equation}

Si on se limite aux corps opaques, $\mathcal{T}_{\lambda}$=0 et la relation précédente se limite à:
\begin{equation}
  \alpha_{\lambda}+\rho_{\lambda}=1
\end{equation}

Tous ces paramètres sont des propriétés intrinsèques de la matière. Elles peuvent dépendre de la
température du corps, de la direction, de façon générale de la longueur d'onde. Mais \emph{en aucun
cas des conditions auxquelles ce corps est soumis}.

\subsection{Loi de Kirchhoff}

Il s'agit de montrer la relation entre l'absorptivité $\alpha_{\lambda}$ et l'émissivité $\epsilon_{\lambda}$.
Etant donné qu'il s'agit de propriétés de la matière, il suffit de démontrer cette relation dans
un cas particulier. Elle restera vraie dans le cas général.

En guise de cas particulier, prenons un solide convexe (forme rouge dans la figure \ref{fig:cavity})
à la température $T$, situé dans un solide concave (cavité), noir, rayonnant à la température
$T_{r}$.
\begin{figure}[!h]
\centering
\includegraphics[width=0.40\textwidth,angle=0]{./figures/cavite.png}
\caption[]{Cavité noire rayonnant à température $T_{r}$, contenant un objet opaque réléchissant ($\rho_{\lambda}$), absorbant ($\alpha_{\lambda}$) et émettant ($\epsilon_{\lambda}$) à la température $T$}
\label{fig:cavity}
\end{figure}

La luminance monochromatique émise par le solide rouge est par définition:

\begin{equation}
  L_{\lambda}(T)=\epsilon_{\lambda}L_{\lambda}^{0}(T)
\end{equation}

Le flux radiatif monochromatique émis par ce solide est donc:
\begin{equation}
  \Phi_{\lambda,emis}(T)=\pi S \epsilon_{\lambda}L_{\lambda}^{0}(T)
\end{equation}

La luminance monochromatique émise par la surface interne de la cavité est $L_{\lambda}^{0}(T_{r})$; le
flux monochromatique absorbé par le solide rouge est donc:
\begin{equation}
  \Phi_{\lambda,absorbe}=\pi S \alpha_{\lambda}L_{\lambda}^{0}(T_{r})
\end{equation}
Ce flux absorbé ne dépend que de l'environnement (luminance émise par la cavité) et de l'absorptivité
du matériau (probabilité qu'un photon incident soit bien absorbé).

Partons initialement d'une situation où $T \ne T_{r}$; si seul l'échange radiatif à une
longueur d'onde est possible, la température $T$ va évoluer jusqu'à devenir égale à $T_{r}$:
si initialement $T$<$T_{r}$, le corps rouge va absorber plus d'énergie qu'il n'en émet, jusqu'à
atteindre l'équilibre. Si au contraire on a initialement $T$>$T_{r}$, le corps va émettre plus
d'énergie qu'il n'en absorbe, mais encore une fois il va atteindre l'équilibre pour $T$=$T_{r}$.

L'équilibre est caractérisé par:
\begin{equation}
  \Phi_{\lambda,em}(T_{r})=\Phi_{\lambda,abs}
\end{equation}

On obtient immédiatement:
\begin{equation}
  \epsilon_{\lambda}=\alpha_{\lambda}
\end{equation}

On peut réaliser la même égalité à l'équilibre pour n'importe quel intervalle spectral $\Delta \lambda$=[$\lambda_{min}$,$\lambda_{max}$]:
\begin{itemize}
\item Le flux émis par la cavité 2 en direction du corps central 1 est:
  \begin{equation}
    \Phi_{emis,2\rightarrow 1}=\int_{\lambda_{min}}^{\lambda_{max}} S_{2}F_{21} \pi L_{\lambda}^{0}(T_{r}) d\lambda
  \end{equation}
\item Le flux reçu par le corps central 1 est le même; étant donné la réciprocité des
  trajectoires optiques on a $S_{1}F_{12}=S_{2}F_{21}$ et comme $F_{12}$=1 dans ce cas, on peut
  le reformuler en:
  \begin{equation}
    \Phi_{incident,1}=\int_{\lambda_{min}}^{\lambda_{max}} S_{1} \pi L_{\lambda}^{0}(T_{r}) d\lambda
  \end{equation}
\item A chaque longueur d'onde, le matériau présente une probabilité $\alpha_{\lambda}$ d'absorption;
  le flux absorbé par 1 est donc:
  \begin{equation}
    \Phi_{absorbe,1}=\int_{\lambda_{min}}^{\lambda_{max}} \alpha_{\lambda} \pi  S_{1} L_{\lambda}^{0}(T_{r}) d\lambda
  \end{equation}
\item Le flux émis par 1, à la température $T$, est:
  \begin{equation}
    \Phi_{emis,1}=\int_{\lambda_{min}}^{\lambda_{max}} \epsilon_{\lambda} \pi  S_{1} L_{\lambda}^{0}(T) d\lambda
  \end{equation}
\end{itemize}

Lorsque l'équilibre thermique est atteint, la température $T$ prend la valeur de $T_{r}$, et le
flux net sur la surface de 1 est nul (flux absorbé = flux émis):
\begin{equation}
  \int_{\lambda_{min}}^{\lambda_{max}} \alpha_{\lambda} \pi S_{1} L_{\lambda}^{0}(T_{r}) d\lambda=\int_{\lambda_{min}}^{\lambda_{max}} \epsilon_{\lambda} \pi S_{1} L_{\lambda}^{0}(T_{r}) d\lambda
\end{equation}
Ces deux flux peuvent être reformulés en utilisant $\alpha_{\Delta \lambda}$ et $\epsilon_{\Delta \lambda}$, respectivement
l'absorptivité et l'émissivité moyenne sur l'intervalle de longueur d'onde $\Delta \lambda$, et la fraction
de corps noir $F(\lambda,T)=\int_{0}^{\lambda} L_{\lambda}^{0}(T)$; on arrive à:
\begin{equation}
  \bigl(\alpha_{\Delta \lambda}-\epsilon_{\Delta \lambda}\bigr) S_{1} \sigma T_{r}^{4} \Bigl(F(\lambda_{max},T_{r})-F(\lambda_{min},T_{r})\Bigr)=0
\end{equation}
On obtient donc bien $\alpha_{\Delta \lambda}$=$\epsilon_{\Delta \lambda}$

Le même résultat est atteint pour une intégration sur tout le spectre: $\lambda_{min} \rightarrow 0$ et
$\lambda_{max} \rightarrow +\infty$; on arrive à $\alpha$=$\epsilon$, égalité entre l'absorptivité
et l'émissivité moyennes définies sur tout le spectre.


Une fois l'égalité entre l'émissivité et l'absorptivité (monochromatique ou intégrée sur un intervalle
spectral arbitraire) prouvée, cette propriété reste vraie dans toutes les situations. En particulier,
cela ne dépend en aucune manière des conditions auxquelles sont soumises le corps étudié.


%============================================================================================================
\section{Bilans radiatifs}

La situation de la figure \ref{fig:cavity} est représentative, d'un point de vue radiatif, des procédés
de production d'Arcelor-Mittal: un objet d'intérêt (1), qui a un facteur de forme $F_{12}$=1 avec son
environnement (2). L'environnement n'envoie qu'une partie de son rayonnement vers l'objet ($F_{21}$<1)
et le reste est échangé avec l'environnement lui-même ($F_{22}$>0).

Que se passe t-il lorsque les propriétés de surface $\epsilon_{\lambda}$ et $\rho_{\lambda}=1-\epsilon_{\lambda}$
dépendent fortement de la longueur d'onde ?

On peut toujours écrire, pour un intervalle spectral [$\lambda_{min}$,$\lambda_{max}$], éventuellement
dégénéré sur le spectre entier ($\lambda_{min}\rightarrow$0 et $\lambda_{max} \rightarrow +\infty$):
\begin{itemize}
\item la densité surfacique de flux émis par 1 à une température $T$ donnée, en une position $\vec{x}$:
  \begin{equation}
    \phi_{emis,1}(\vec{x})=\pi \int_{\lambda_{min}}^{\lambda_{max}} \epsilon_{\nu}(\vec{x}) L_{\lambda}^{0}(T) d\lambda
  \end{equation}
\item la densité surfacique de flux reçu par 1 en une position $\vec{x}$
  \begin{equation}
    \phi_{recu,1}(\vec{x})=\int_{2\pi^{-}(\vec{n})}|\vec{u}.\vec{n}|d\vec{u} \int_{\lambda_{min}}^{\lambda_{max}} L_{\lambda}(\vec{x},\vec{u}) d\lambda
  \end{equation}
  Etant donné que 2 est un corps noir émettant à la température $T_{r}$, on connait la luminance incidente sur 1: $L_{\lambda}(\vec{x},\vec{u})=L_{\lambda}^{0}(T_{r})$
  pour toutes les positions $\vec{x}$ et les directions $\vec{u}$, d'où:
  \begin{equation}
    \begin{split}
      \phi_{recu,1}(\vec{x})&=\pi \int_{\lambda_{min}}^{\lambda_{max}} L_{\lambda}^{0}(T_{r}) d\lambda\\
      &=\sigma T_{r}^{4}\Bigl[F(\lambda_{max},T_{r})-F(\lambda_{min},T_{r})\Bigr]
    \end{split}
  \end{equation}
\item la densité surfacique de flux réfléchi par 1 en une position $\vec{x}$
  \begin{equation}
    \begin{split}
      \phi_{reflechi,1}(\vec{x})&=\int_{2\pi^{-}(\vec{n})}÷|\vec{u}.\vec{n}|d\vec{u} \int_{\lambda_{min}}^{\lambda_{max}} \rho_{\nu}(\vec{x}) L_{\lambda}(\vec{x},\vec{u}) d\lambda\\
      &=\int_{2\pi^{-}(\vec{n})}÷|\vec{u}.\vec{n}|d\vec{u} \int_{\lambda_{min}}^{\lambda_{max}} \rho_{\nu}(\vec{x}) L_{\lambda}^{0}(T_{r}) d\lambda\\
      &=\pi \int_{\lambda_{min}}^{\lambda_{max}} \rho_{\nu}(\vec{x}) L_{\lambda}^{0}(T_{r}) d\lambda
    \end{split}
  \end{equation}
\item la densité surfacique de flux absorbé par 1 en une position $\vec{x}$
  \begin{equation}
    \begin{split}
      \phi_{absorbe,1}(\vec{x})&=\int_{2\pi^{-}(\vec{n})}÷|\vec{u}.\vec{n}|d\vec{u} \int_{\lambda_{min}}^{\lambda_{max}} \epsilon_{\lambda} L_{\lambda}(\vec{x},\vec{u}) d\lambda\\
      &=\int_{2\pi^{-}(\vec{n})}÷|\vec{u}.\vec{n}|d\vec{u} \int_{\lambda_{min}}^{\lambda_{max}} \epsilon_{\lambda} L_{\lambda}^{0}(T_{r}) d\lambda\\
      &=\pi \int_{\lambda_{min}}^{\lambda_{max}} \epsilon_{\lambda} L_{\lambda}^{0}(T_{r}) d\lambda
    \end{split}
  \end{equation}
\item la radiosité de 1, somme des flux totaux émis et réfléchi par 1:
  \begin{equation}
    J_{1}=\int_{S_{1}}\phi_{emis,1}(\vec{x})+\phi_{reflechi,1}(\vec{x})dS(\vec{x})
  \end{equation}
\item le bilan radiatif en une position donnée (densité de flux absorbé - densité de flux émis):
  \begin{equation}
    \begin{split}
      \phi_{net,1}(\vec{x})&=\phi_{absorbe,1}(\vec{x})-\phi_{emis,1}(\vec{x})\\
      &=\pi \int_{\lambda_{min}}^{\lambda_{max}} \epsilon_{\nu}(\vec{x}) \Bigl[L_{\lambda}^{0}(T_{r})-L_{\lambda}^{0}(T)\Bigr] d\lambda
    \end{split}
  \end{equation}
  Si par exemple $T$<$T_{r}$ cette quantité est toujours positive, quel que soit le spectre d'émisivité. Et y compris
  si l'émissivité de l'objet 1 évolue avec la température de ce dernier, par exemple: la valeur de cette densité
  de flux net sera seulement plus faible lorsque $\epsilon_{\lambda}$ est plus faible autour du maximum (spectral)
  de la fonction de Planck à $T_{r}$. On peut par exemple imaginer une situation où cette émissivité est initialement
  importante autour du pic de $L_{\lambda}^{0}(T_{r})$: la densité de flux net est importante, et l'objet se
  réchauffe vite. Admettons que son émissivité diminue (la réflectivité augmente) lorsque sa température
  augmente: dans ce cas là, non seulement $L_{\lambda}^{0}(T_{r})-L_{\lambda}^{0}(T)$ diminue lorsque $T$
  augmente, mais si en plus $\epsilon_{\nu}$ diminue dans la zone spectrale du maximum de $L_{\lambda}^{0}(T_{r})$,
  la densité de flux net diminue d'autant plus: plus l'objet chauffe, et moins il chauffe vite.
\end{itemize}

Evidemment la situation où 2 est un corps noir est certainement trop simpliste: dans le cas général
on préférera modéliser 2 par un corps réfléchissant (ou plutôt un ensemble de corps réfléchissants,
chacun munis de leurs signatures de réflectivité / émissivité).




%============================================================================================================
\bibliographystyle{unsrt}
\bibliography{biblio}

\end{document}
